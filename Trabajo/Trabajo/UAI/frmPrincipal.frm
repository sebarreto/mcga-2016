VERSION 5.00
Begin VB.Form frmPrincipal 
   Caption         =   "Generador de Back Up"
   ClientHeight    =   2010
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   3645
   LinkTopic       =   "Form1"
   ScaleHeight     =   2010
   ScaleWidth      =   3645
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame2 
      Height          =   1695
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3375
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   10000
         Left            =   2640
         Top             =   960
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "Aceptar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   960
         TabIndex        =   2
         Top             =   840
         Width           =   1215
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Realizar Back Up al iniciar el Sistema"
         Height          =   375
         Left            =   240
         TabIndex        =   1
         Top             =   240
         Width           =   3015
      End
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Path, PathDestino As String

Private Sub Realizar_BackUp()
  Path = "E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  PathDestino = "E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BACK UP\dataserv.mdb"
  FileCopy Path, PathDestino
End Sub

Private Sub cmdAceptar_Click()
  If Check1.Value = vbChecked Then
    Open rutaIni For Output As #1
      Print #1, ";TECSERV"
      Print #1, ""
      Print #1, "[BACKUP]"
      Print #1, ";DETERMINA SI REALIZA EL BACK UP O NO"
      Print #1, "OPT=1"
    Close #1
  Else
    Open rutaIni For Output As #1
      Print #1, ";TECSERV"
      Print #1, ""
      Print #1, "[BACKUP]"
      Print #1, ";DETERMINA SI REALIZA EL BACK UP O NO"
      Print #1, "OPT=0"
    Close #1
  End If
End Sub

Private Sub Form_Load()
'LEER INI
Timer1.Enabled = False
rutaIni = "E:\UAI\TECServicios-singleton\TECServicios\TECSERV\tecserv.ini"
ResultValue = fileExists(rutaIni)
  If ResultValue = False Then
    Open rutaIni For Output As #1
      Print #1, ";TECSERV"
      Print #1, ""
      Print #1, "[BACKUP]"
      Print #1, ";DETERMINA SI REALIZA EL BACK UP O NO"
      Print #1, "1"
    Close #1
  End If
    TITULO = "BACKUP"
    campo = "OPT"
    VARIABLE = LeeINI(TITULO, campo, rutaIni)
    If VARIABLE = "1" Then
      Check1.Value = vbChecked
      Realizar_BackUp
      Timer1.Enabled = True
    Else
      Check1.Value = vbUnchecked
      Timer1.Enabled = True
    End If
End Sub


Private Sub Timer1_Timer()
End
End Sub
