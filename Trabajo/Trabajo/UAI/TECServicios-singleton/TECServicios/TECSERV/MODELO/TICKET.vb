Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class TICKET
    Private Conexion As New sqlConnection()
    Private oDataAdapter As sqlDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New sqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As sqlCommand
        oSComando = New sqlCommand("SELECT * FROM T_Tickets ORDER BY Id_Ticket ", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As sqlCommand
        oIComando = New sqlCommand("INSERT INTO T_Tickets (Id_Cliente,Id_Operador,Id_Tecnico,Falla,Nconforme,Cobranza,Monto,Fecha,Hora) VALUES (@Id_Cliente,@Id_Operador,@Id_Tecnico,@Falla,@Nconforme,@Cobranza,@Monto,@Fecha,@Hora)", Conexion)
        oIComando.Parameters.Add("@Id_Cliente", SqlDbType.Int, 200, "Id_Cliente")
        'oIComando.Parameters.Add("@NomCliente", sqlDbType.VarChar, 50, "NomCliente")
        oIComando.Parameters.Add("@Id_Operador", SqlDbType.Int, 20, "Id_Operador")
        oIComando.Parameters.Add("@Id_Tecnico", SqlDbType.Int, 20, "Id_Tecnico")
        oIComando.Parameters.Add("@Falla", SqlDbType.VarChar, 200, "Falla")
        oIComando.Parameters.Add("@Nconforme", SqlDbType.Int, 10, "Nconforme")
        oIComando.Parameters.Add("@Cobranza", SqlDbType.VarChar, 1, "Cobranza")
        oIComando.Parameters.Add("@Monto", SqlDbType.Int, 10, "Monto")
        oIComando.Parameters.Add("@Fecha", SqlDbType.Date, 20, "Fecha")
        oIComando.Parameters.Add("@Hora", SqlDbType.VarChar, 20, "Hora")

        oIComando.Parameters.Add("@Id_Ticket", SqlDbType.Int, 10, "Id_Ticket")
        oDataAdapter.InsertCommand = oIComando

        'DELETE COMMAND
        Dim oDComando As sqlCommand
        oDComando = New SqlCommand("DELETE T_Tickets WHERE Id_Ticket = @Id_Ticket", Conexion)
        oDComando.Parameters.Add("@Id_Ticket", SqlDbType.Int, 1, "Id_Ticket")
        oDataAdapter.DeleteCommand = oDComando

        'UPDATE COMMAND
        Dim oUComando As sqlCommand
        oUComando = New sqlCommand("UPDATE T_Tickets SET Id_Cliente=@Id_Cliente, Id_Operador=@Id_Operador, Id_Tecnico=@Id_Tecnico, Falla=@Falla, Nconforme=@Nconforme, Cobranza=@Cobranza, Monto=@Monto,Fecha= @Fecha , Hora = @Hora  WHERE Id_Ticket = @Id_Ticket ", Conexion)
        oUComando.Parameters.Add("@Id_Cliente", SqlDbType.Int, 200, "Id_Cliente")
        'oUComando.Parameters.Add("@NomCliente", sqlDbType.VarChar, 50, "NomCliente")
        oUComando.Parameters.Add("@Id_Operador", SqlDbType.Int, 20, "Id_Operador")
        oUComando.Parameters.Add("@Id_Tecnico", SqlDbType.Int, 20, "Id_Tecnico")
        oUComando.Parameters.Add("@Falla", SqlDbType.VarChar, 200, "Falla")
        oUComando.Parameters.Add("@Nconforme", SqlDbType.Int, 6, "Nconforme")
        oUComando.Parameters.Add("@Cobranza", SqlDbType.VarChar, 1, "Cobranza")
        oUComando.Parameters.Add("@Monto", SqlDbType.Int, 6, "Monto")
        oUComando.Parameters.Add("@Fecha", SqlDbType.VarChar, 20, "Fecha")
        oUComando.Parameters.Add("@Hora", SqlDbType.VarChar, 20, "Hora")

        oUComando.Parameters.Add("@Id_Ticket", SqlDbType.Int, 10, "Id_Ticket")

        oDataAdapter.UpdateCommand = oUComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtTICKETS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtTICKETS)
        Catch oEX As Exception
            MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub

    Public Function BuscarID_OPERADOR(ByVal IdOperador)
        Dim con As sqlConnection
        Dim cmd As sqlCommand
        Dim Reader As sqlDataReader
        Dim OPERADOR As String

        con = New sqlConnection
        cmd = New sqlCommand
        con.ConnectionString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;Initial Catalog=dataserv;Integrated Security=SSPI;"
        cmd.Connection = con
        cmd.CommandText = "SELECT * FROM T_Operadores WHERE Id_Operador = " & IdOperador & " "
        con.Open()
        Reader = cmd.ExecuteReader
        While (Reader.Read())
            OPERADOR = Reader.GetString(1)
        End While
        Reader.Close()
        Return OPERADOR
    End Function

    Public Sub ACTUALIZA_TICKET(ByVal Myticket As ENTIDADES.Ticket)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' INSERT COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("INSERT INTO T_Tickets (Id_Cliente,Id_Operador,Id_Tecnico,Falla,Nconforme,Cobranza,Monto,Fecha,Hora) VALUES (@Id_Cliente,@Id_Operador,@Id_Tecnico,@Falla,@Nconforme,@Cobranza,@Monto,@Fecha,@Hora)", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Cliente", Myticket.ticCliente)
        oIComando.Parameters.AddWithValue("@Id_Operador", Myticket.ticOperador)
        oIComando.Parameters.AddWithValue("@Id_Tecnico", Myticket.ticTecnico)
        oIComando.Parameters.AddWithValue("@Falla", Myticket.ticFalla)
        oIComando.Parameters.AddWithValue("@Nconforme", Myticket.ticNoconforme)
        oIComando.Parameters.AddWithValue("@Cobranza", Myticket.ticCobranza)
        oIComando.Parameters.AddWithValue("@Monto", Myticket.ticMonto)
        oIComando.Parameters.AddWithValue("@Fecha", Myticket.ticFechati)
        oIComando.Parameters.AddWithValue("@Hora", Myticket.ticHorati)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub MODIFICA_TICKET(ByVal Myticket As ENTIDADES.Ticket)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' UPDATE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("UPDATE T_Tickets SET Id_Cliente=@Id_Cliente, Id_Operador=@Id_Operador, Id_Tecnico=@Id_Tecnico, Falla=@Falla, Nconforme=@Nconforme, Cobranza=@Cobranza, Monto=@Monto,Fecha= @Fecha , Hora = @Hora  WHERE Id_Ticket = @Id_Ticket ", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Cliente", Myticket.ticCliente)
        oIComando.Parameters.AddWithValue("@Id_Operador", Myticket.ticOperador)
        oIComando.Parameters.AddWithValue("@Id_Tecnico", Myticket.ticTecnico)
        oIComando.Parameters.AddWithValue("@Falla", Myticket.ticFalla)
        oIComando.Parameters.AddWithValue("@Nconforme", Myticket.ticNoconforme)
        oIComando.Parameters.AddWithValue("@Cobranza", Myticket.ticCobranza)
        oIComando.Parameters.AddWithValue("@Monto", Myticket.ticMonto)
        oIComando.Parameters.AddWithValue("@Fecha", Myticket.ticFechati)
        oIComando.Parameters.AddWithValue("@Hora", Myticket.ticHorati)
        oIComando.Parameters.AddWithValue("@Id_Ticket", Myticket.ticIdticket)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ELIMINA_TICKET(ByVal Myticket As ENTIDADES.Ticket)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' DELETE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("DELETE T_Tickets WHERE Id_Ticket = @Id_Ticket", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Ticket", Myticket.ticIdticket)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

End Class