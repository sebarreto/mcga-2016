Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class PERMISO

    Private Conexion As New sqlConnection()
    Private oDataAdapter As sqlDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New sqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As sqlCommand
        oSComando = New sqlCommand("SELECT * FROM T_Permisos", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As sqlCommand
        oIComando = New sqlCommand("INSERT INTO T_Permisos(perNombre,perDescripcion) VALUES (@perNombre,@perDescripcion)", Conexion)
        oIComando.Parameters.Add("@perNombre", SqlDbType.VarChar, 20, "perNombre")
        oIComando.Parameters.Add("@perDescripcion", SqlDbType.VarChar, 200, "perDescripcion")

        oIComando.Parameters.Add("@Id_Permiso", SqlDbType.Int, 10, "Id_Permiso")
        oDataAdapter.InsertCommand = oIComando

        'UPDATE COMMAND
        Dim oUComando As sqlCommand
        oUComando = New sqlCommand("UPDATE T_Permisos SET  perNombre = @perNombre,perDescripcion = @perDescripcion   WHERE Id_Permiso = @Id_Permiso ", Conexion)
        oUComando.Parameters.Add("@perNombre", SqlDbType.VarChar, 20, "perNombre")
        oUComando.Parameters.Add("@perDescripcion", SqlDbType.VarChar, 20, "perDescripcion")

        oUComando.Parameters.Add("@Id_Permiso", SqlDbType.Int, 10, "Id_Permiso")
        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As sqlCommand
        oDComando = New SqlCommand("DELETE T_Permisos WHERE Id_Permiso = @Id_Permiso", Conexion)
        oDComando.Parameters.Add("@Id_Permiso", SqlDbType.Int, 10, "Id_Permiso")
        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtPERMISOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtPERMISOS)
        Catch oEX As Exception
            MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub

    Public Sub ACTUALIZA_PERMISO(ByVal Mypermiso As ENTIDADES.Permiso)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' INSERT COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("INSERT INTO T_Permisos(perNombre,perDescripcion) VALUES (@perNombre,@perDescripcion)", Conexion)
        oIComando.Parameters.AddWithValue("@perNombre", Mypermiso.permNombre)
        oIComando.Parameters.AddWithValue("@perDescripcion", Mypermiso.permDescripcion)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub MODIFICA_PERMISO(ByVal Mypermiso As ENTIDADES.Permiso)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' UPDATE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("UPDATE T_Permisos SET  perNombre = @perNombre,perDescripcion = @perDescripcion   WHERE Id_Permiso = @Id_Permiso ", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Permiso", Mypermiso.permIdpermiso)
        oIComando.Parameters.AddWithValue("@perNombre", Mypermiso.permNombre)
        oIComando.Parameters.AddWithValue("@perDescripcion", Mypermiso.permDescripcion)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ELIMINA_PERMISO(ByVal Mypermiso As ENTIDADES.Permiso)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' DELETE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("DELETE T_Permisos WHERE Id_Permiso = @Id_Permiso", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Permiso", Mypermiso.permIdpermiso)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

End Class

