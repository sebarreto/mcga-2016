Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient

Public Class PERFIL

    Private Conexion As New sqlConnection()
    Private oDataAdapter As sqlDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New sqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As sqlCommand
        oSComando = New sqlCommand("SELECT * FROM T_Perfiles", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As sqlCommand
        oIComando = New sqlCommand("INSERT INTO T_Perfiles (Id_Grupo, Id_Formulario, Id_Permiso) VALUES (@Id_Grupo, @Id_Formulario, @Id_Permiso)", Conexion)
        oIComando.Parameters.Add("@Id_Grupo", SqlDbType.Int, 20, "Id_Grupo")
        oIComando.Parameters.Add("@Id_Formulario", SqlDbType.Int, 20, "Id_Formulario")
        oIComando.Parameters.Add("@Id_Permiso", SqlDbType.Int, 20, "Id_Permiso")

        ' oIComando.Parameters.Add("@Id_Perfil", sqlDbType.Int, 10, "Id_Perfil")
        oDataAdapter.InsertCommand = oIComando

        'UPDATE COMMAND
        Dim oUComando As sqlCommand
        oUComando = New sqlCommand("UPDATE T_Perfiles SET  perPermiso = @perPermiso WHERE perFormulario = @perFormulario AND perGrupo=@perGrupo ", Conexion)
        oUComando.Parameters.Add("@pePermisos", SqlDbType.VarChar, 50, "pePermisos")

        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As sqlCommand
        oDComando = New SqlCommand("DELETE T_Perfiles  WHERE Id_Perfil = @Id_Perfil", Conexion)
        oDComando.Parameters.Add("@Id_Perfil", SqlDbType.Int, 10, "Id_Perfil")

        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
        ' oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Function ELIMINAR_PERFIL(ByVal Grupo, ByVal Formulario, ByVal Perm)
        Dim con As sqlConnection
        Dim cmd As sqlCommand
        Dim Reader As sqlDataReader

        con = New sqlConnection
        cmd = New sqlCommand
        con.ConnectionString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;Initial Catalog=dataserv;Integrated Security=SSPI;"
        cmd.Connection = con
        cmd.CommandText = "DELETE T_Perfiles WHERE Id_Grupo = " & Grupo & " AND Id_Formulario = " & Formulario & " AND Id_Permiso = " & Perm & "  "
        con.Open()
        Reader = cmd.ExecuteReader
    End Function

    Public Function MODIFICAR_PERFIL(ByVal Grupo, ByVal Formulario, ByVal Perm)
        Dim con As sqlConnection
        Dim cmd As sqlCommand
        Dim Reader As sqlDataReader

        con = New sqlConnection
        cmd = New sqlCommand
        con.ConnectionString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;Initial Catalog=dataserv;Integrated Security=SSPI;"
        cmd.Connection = con
        cmd.CommandText = "DELETE T_Perfiles WHERE Id_Grupo = " & Grupo & " AND Id_Formulario = " & Formulario & " AND Id_Permiso = " & Perm & "  "
        con.Open()
        Reader = cmd.ExecuteReader

    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtCLIENTES As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtCLIENTES)
        Catch oEX As Exception
            MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub

    Public Sub ACTUALIZA_PERFIL(ByVal Myperfil As ENTIDADES.Perfil)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' INSERT COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("INSERT INTO T_Perfiles (Id_Grupo, Id_Formulario, Id_Permiso) VALUES (@Id_Grupo, @Id_Formulario, @Id_Permiso)", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Grupo", Myperfil.perIdgrupo)
        oIComando.Parameters.AddWithValue("@Id_Formulario", Myperfil.perFormulario)
        oIComando.Parameters.AddWithValue("@Id_Permiso", Myperfil.perPermiso)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub MODIFICA_PERFIL(ByVal Myperfil As ENTIDADES.Perfil)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' UPDATE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("DELETE T_Perfiles  WHERE Id_Grupo = @Id_Grupo AND Id_Formulario = @Id_Formulario AND Id_Permiso = @Id_Permiso ", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Grupo", Myperfil.perIdgrupo)
        oIComando.Parameters.AddWithValue("@Id_Formulario", Myperfil.perFormulario)
        oIComando.Parameters.AddWithValue("@Id_Permiso", Myperfil.perPermiso)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ELIMINA_PERFIL(ByVal Myperfil As ENTIDADES.Perfil)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' DELETE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("DELETE T_Perfiles  WHERE Id_Grupo = @Id_Grupo AND Id_Formulario = @Id_Formulario AND Id_Permiso = @Id_Permiso ", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Grupo", Myperfil.perIdgrupo)
        oIComando.Parameters.AddWithValue("@Id_Formulario", Myperfil.perFormulario)
        oIComando.Parameters.AddWithValue("@Id_Permiso", Myperfil.perPermiso)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

End Class