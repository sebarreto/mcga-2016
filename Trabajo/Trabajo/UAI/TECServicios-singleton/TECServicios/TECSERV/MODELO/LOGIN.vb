Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class LOGIN
    Private Conexion As New sqlConnection()
    Private oDataAdapter As sqlDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New sqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As sqlCommand
        oSComando = New sqlCommand("SELECT * FROM T_Operadores", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As sqlCommand
        oIComando = New sqlCommand("INSERT INTO T_Operadores(logNombre, logClave,logNivel) VALUES (@logNombre,@logClave,@logNivel)", Conexion)
        oIComando.Parameters.Add("@logNombre", SqlDbType.VarChar, 20, "logNombre")
        oIComando.Parameters.Add("@logClave", SqlDbType.VarChar, 50, "logClave")
        oIComando.Parameters.Add("@logNivel", SqlDbType.Int, 4, "logNivel")

        oIComando.Parameters.Add("@Id_Login", SqlDbType.Int, 10, "Id_Login")
        oDataAdapter.InsertCommand = oIComando

        ' DELETECOMMAND
        Dim oDComando As sqlCommand
        oDComando = New sqlCommand("DELETE * FROM T_Operadores  WHERE Id_Login = @Id_Login", Conexion)
        oDComando.Parameters.Add("@Id_Login", SqlDbType.Int, 1, "Id_Login")
        oDataAdapter.DeleteCommand = oDComando

        'UPDATE COMMAND
        Dim oUComando As sqlCommand
        oUComando = New sqlCommand("UPDATE USUARIOS SET logNombre = @logNombre, logClave = @logClave, logNivel = @logNivel WHERE Id_Login = @Id_Login ", Conexion)
        oIComando.Parameters.Add("@logNombre", SqlDbType.VarChar, 20, "logNombre")
        oIComando.Parameters.Add("@logClave", SqlDbType.VarChar, 50, "logClave")
        oIComando.Parameters.Add("@logNivel", SqlDbType.Int, 4, "logNivel")

        oUComando.Parameters.Add("@Id_Login", SqlDbType.Int, 2, "Id_Login")
        oDataAdapter.UpdateCommand = oUComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtUSUARIOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtUSUARIOS)
        Catch oEX As Exception
            MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub

End Class
