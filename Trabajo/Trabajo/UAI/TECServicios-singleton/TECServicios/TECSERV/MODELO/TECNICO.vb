Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class TECNICO
    Private Conexion As New sqlConnection()
    Private oDataAdapter As sqlDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New sqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As sqlCommand
        oSComando = New sqlCommand("SELECT * FROM T_Tecnicos", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As sqlCommand
        oIComando = New sqlCommand("INSERT INTO T_Tecnicos (tecNombre,tecApellido,tecDomicilio,tecLocalidad,tecTelefono) VALUES (@tecNombre,@tecApellido,@tecDomicilio,@tecLocalidad,@tecTelefono)", Conexion)
        oIComando.Parameters.Add("@tecNombre", SqlDbType.VarChar, 20, "tecNombre")
        oIComando.Parameters.Add("@tecApellido", SqlDbType.VarChar, 20, "tecApellido")
        oIComando.Parameters.Add("@tecDomicilio", SqlDbType.VarChar, 20, "tecDomicilio")
        oIComando.Parameters.Add("@tecLocalidad", SqlDbType.VarChar, 20, "tecLocalidad")
        oIComando.Parameters.Add("@tecTelefono", SqlDbType.VarChar, 20, "tecTelefono")


        oIComando.Parameters.Add("@Id_Tecnico", SqlDbType.Int, 10, "Id_Tecnico")

        oDataAdapter.InsertCommand = oIComando

        'UPDATE COMMAND
        Dim oUComando As sqlCommand
        oUComando = New sqlCommand("UPDATE T_Tecnicos SET  tecNombre = @tecNombre, tecApellido = @tecApellido, tecDomicilio=@tecDomicilio, tecLocalidad=@tecLocalidad, tecTelefono=@tecTelefono WHERE Id_Tecnico = @Id_Tecnico ", Conexion)
        oUComando.Parameters.Add("@tecNombre", SqlDbType.VarChar, 20, "tecNombre")
        oUComando.Parameters.Add("@tecApellido", SqlDbType.VarChar, 20, "tecApellido")
        oUComando.Parameters.Add("@tecDomicilio", SqlDbType.VarChar, 20, "tecDomicilio")
        oUComando.Parameters.Add("@tecLocalidad", SqlDbType.VarChar, 20, "tecLocalidad")
        oUComando.Parameters.Add("@tecTelefono", SqlDbType.VarChar, 20, "tecTelefono")

        oUComando.Parameters.Add("@Id_Tecnico", SqlDbType.Int, 10, "Id_Tecnico")

        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As sqlCommand
        oDComando = New SqlCommand("DELETE T_Tecnicos FROM WHERE Id_Tecnico = @Id_Tecnico", Conexion)
        oDComando.Parameters.Add("@Id_Tecnico", SqlDbType.Int, 10, "Id_Tecnico")

        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtTECNICOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtTECNICOS)
        Catch oEX As Exception
            MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub

    Public Sub ACTUALIZA_TECNICO(ByVal Mytecnico As ENTIDADES.Tecnico)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' INSERT COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("INSERT INTO T_Tecnicos (tecNombre,tecApellido,tecDomicilio,tecLocalidad,tecTelefono) VALUES (@tecNombre,@tecApellido,@tecDomicilio,@tecLocalidad,@tecTelefono)", Conexion)
        oIComando.Parameters.AddWithValue("@tecNombre", Mytecnico.tecNombre)
        oIComando.Parameters.AddWithValue("@tecApellido", Mytecnico.tecApellido)
        oIComando.Parameters.AddWithValue("@tecDomicilio", Mytecnico.tecDomicilio)
        oIComando.Parameters.AddWithValue("@tecLocalidad", Mytecnico.tecLocalidad)
        oIComando.Parameters.AddWithValue("@tecTelefono", Mytecnico.tecTelefono)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub MODIFICA_TECNICO(ByVal Mytecnico As ENTIDADES.Tecnico)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' UPDATE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("UPDATE T_Tecnicos SET  tecNombre = @tecNombre, tecApellido = @tecApellido, tecDomicilio=@tecDomicilio, tecLocalidad=@tecLocalidad, tecTelefono=@tecTelefono WHERE Id_Tecnico = @Id_Tecnico ", Conexion)
        oIComando.Parameters.AddWithValue("@tecNombre", Mytecnico.tecNombre)
        oIComando.Parameters.AddWithValue("@tecApellido", Mytecnico.tecApellido)
        oIComando.Parameters.AddWithValue("@tecDomicilio", Mytecnico.tecDomicilio)
        oIComando.Parameters.AddWithValue("@tecLocalidad", Mytecnico.tecLocalidad)
        oIComando.Parameters.AddWithValue("@tecTelefono", Mytecnico.tecTelefono)
        oIComando.Parameters.AddWithValue("@Id_Tecnico", Mytecnico.tecIdtecnico)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ELIMINA_TECNICO(ByVal Mytecnico As ENTIDADES.Tecnico)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' DELETE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("DELETE T_Tecnicos FROM WHERE Id_Tecnico = @Id_Tecnico", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Tecnico", Mytecnico.tecIdtecnico)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

End Class

