Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient

Public Class APERMISO
    Private Conexion As New sqlConnection()
    Private oDataAdapter As sqlDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.ConectarAud
        oDataAdapter = New sqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As sqlCommand
        oSComando = New sqlCommand("SELECT * FROM T_AuditoriaPermisos", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As sqlCommand
        oIComando = New SqlCommand("INSERT INTO T_AuditoriaPermisos (Id_AudPerm,perNombre,audFecha,audHora,audTipo,audUsuario) VALUES (@Id_AudPerm,@perNombre,@audFecha,@audHora,@audTipo,@audUsuario)", Conexion)
        oIComando.Parameters.Add("@perNombre", SqlDbType.VarChar, 20, "perNombre")
        oIComando.Parameters.Add("@audFecha", SqlDbType.VarChar, 20, "audFecha")
        oIComando.Parameters.Add("@audHora", SqlDbType.VarChar, 20, "audHora")
        oIComando.Parameters.Add("@audTipo", SqlDbType.VarChar, 20, "audTipo")
        oIComando.Parameters.Add("@audUsuario", SqlDbType.VarChar, 20, "audUsuario")

        oIComando.Parameters.Add("@Id_AudPerm", SqlDbType.Int, 10, "Id_AudPerm")

        oDataAdapter.InsertCommand = oIComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtPERMISOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtPERMISOS)
        Catch oEX As Exception
            MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class

