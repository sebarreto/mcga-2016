Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient

Public Class ACLIENTES
    Private Conexion As New SqlConnection()
    Private oDataAdapter As SqlDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.ConectarAud
        oDataAdapter = New SqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As SqlCommand
        oSComando = New SqlCommand("SELECT * FROM T_AuditoriaClientes", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As SqlCommand

        oIComando = New SqlCommand("INSERT INTO T_AuditoriaClientes (Id_Cliente,cliCodigo, cliNombre, cliDomicilio,cliTelefono,cliLocalidad,cliHorario,cliFecha,cliHora,cliTipo,Usuario) VALUES (@Id_Cliente,@cliCodigo,@cliNombre,@cliDomicilio,@cliTelefono,@cliLocalidad,@cliHorario,@cliFecha,@cliHora,@cliTipo,@Usuario)", Conexion)
        oIComando.Parameters.Add("@cliCodigo", SqlDbType.VarChar, 20, "cliCodigo")
        oIComando.Parameters.Add("@cliNombre", SqlDbType.VarChar, 20, "cliNombre")
        oIComando.Parameters.Add("@cliDomicilio", SqlDbType.VarChar, 20, "cliDomicilio")
        oIComando.Parameters.Add("@cliTelefono", SqlDbType.VarChar, 20, "cliTelefono")
        oIComando.Parameters.Add("@cliLocalidad", SqlDbType.VarChar, 20, "cliLocalidad")
        oIComando.Parameters.Add("@cliHorario", SqlDbType.VarChar, 20, "cliHorario")
        oIComando.Parameters.Add("@cliFecha", SqlDbType.VarChar, 20, "cliFecha")
        oIComando.Parameters.Add("@cliHora", SqlDbType.VarChar, 20, "cliHora")
        oIComando.Parameters.Add("@cliTipo", SqlDbType.VarChar, 20, "cliTipo")
        oIComando.Parameters.Add("@Usuario", SqlDbType.VarChar, 20, "Usuario")


        oIComando.Parameters.Add("@Id_Cliente", SqlDbType.Int, 10, "Id_Cliente")
        oDataAdapter.InsertCommand = oIComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtACLIENTES As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.ConectarAud
        Try
            oDataAdapter.Update(dtACLIENTES)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class