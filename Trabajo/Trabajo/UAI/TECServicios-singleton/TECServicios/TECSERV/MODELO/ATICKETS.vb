Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient

Public Class ATICKETS
    Private Conexion As New sqlConnection()
    Private oDataAdapter As sqlDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.ConectarAud
        oDataAdapter = New sqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As sqlCommand
        oSComando = New sqlCommand("SELECT * FROM T_AuditoriaTickets", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As sqlCommand
        oIComando = New SqlCommand("INSERT INTO T_AuditoriaTickets (Id_Ticket,Id_Cliente,Id_Operador,Id_Tecnico,Falla,Nconforme,Cobranza,Monto,Fecha,Hora,audTipo,audFecha,audHora,audUsuario) VALUES (@Id_Ticket,@Id_Cliente,@Id_Operador,@Id_Tecnico,@Falla,@Nconforme,@Cobranza,@Monto,@Fecha,@Hora,@audTipo,@audFecha,@audHora,@audUsuario)", Conexion)
        oIComando.Parameters.Add("@Id_Cliente", SqlDbType.Int, 200, "Id_Cliente")
        ' oIComando.Parameters.Add("@NomCliente", sqlDbType.VarChar, 50, "NomCliente")
        oIComando.Parameters.Add("@Id_Operador", SqlDbType.Int, 10, "Id_Operador")
        oIComando.Parameters.Add("@Id_Tecnico", SqlDbType.Int, 20, "Id_Tecnico")
        oIComando.Parameters.Add("@Falla", SqlDbType.VarChar, 200, "Falla")
        oIComando.Parameters.Add("@Nconforme", SqlDbType.Int, 10, "Nconforme")
        oIComando.Parameters.Add("@Cobranza", SqlDbType.VarChar, 1, "Cobranza")
        oIComando.Parameters.Add("@Monto", SqlDbType.Int, 10, "Monto")
        oIComando.Parameters.Add("@Fecha", SqlDbType.VarChar, 20, "Fecha")
        oIComando.Parameters.Add("@Hora", SqlDbType.VarChar, 20, "Hora")
        oIComando.Parameters.Add("@audTipo", SqlDbType.VarChar, 20, "audTipo")
        oIComando.Parameters.Add("@audFecha", SqlDbType.VarChar, 20, "audFecha")
        oIComando.Parameters.Add("@audHora", SqlDbType.VarChar, 20, "audHora")
        oIComando.Parameters.Add("@audUsuario", SqlDbType.VarChar, 20, "audUsuario")

        oIComando.Parameters.Add("@Id_Ticket", SqlDbType.Int, 10, "Id_Ticket")
        oDataAdapter.InsertCommand = oIComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtATICKETS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.ConectarAud
        Try
            oDataAdapter.Update(dtATICKETS)
        Catch oEX As Exception
            MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class