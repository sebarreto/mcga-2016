Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class CLIENTES
    Private Conexion As New sqlConnection()
    Private oDataAdapter As sqlDataAdapter
    Private oDataTable As DataTable

    Private con As sqlConnection
    Private cmd As sqlCommand
    Private Reader As SqlDataReader

    Public Shared Crear_Cliente As CLIENTES

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New sqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As sqlCommand
        oSComando = New sqlCommand("SELECT * FROM T_Clientes ORDER BY cliNombre", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As sqlCommand
        oIComando = New sqlCommand("INSERT INTO T_Clientes(cliCodigo, cliNombre, cliDomicilio,cliTelefono,cliLocalidad,cliHorario) VALUES (@cliCodigo,@cliNombre,@cliDomicilio,@cliTelefono,@cliLocalidad,@cliHorario)", Conexion)
        oIComando.Parameters.Add("@cliCodigo", SqlDbType.VarChar, 20, "cliCodigo")
        oIComando.Parameters.Add("@cliNombre", SqlDbType.VarChar, 20, "cliNombre")
        oIComando.Parameters.Add("@cliDomicilio", SqlDbType.VarChar, 20, "cliDomicilio")
        oIComando.Parameters.Add("@cliTelefono", SqlDbType.VarChar, 20, "cliTelefono")
        oIComando.Parameters.Add("@cliLocalidad", SqlDbType.VarChar, 20, "cliLocalidad")
        oIComando.Parameters.Add("@cliHorario", SqlDbType.VarChar, 20, "cliHorario")

        oIComando.Parameters.Add("@Id_Cliente", SqlDbType.Int, 10, "Id_Cliente")
        oDataAdapter.InsertCommand = oIComando

        'UPDATE COMMAND
        Dim oUComando As sqlCommand
        oUComando = New sqlCommand("UPDATE T_Clientes SET  cliCodigo=@cliCodigo ,cliNombre = @cliNombre, cliDomicilio = @cliDomicilio, cliTelefono = @cliTelefono, cliLocalidad =@cliLocalidad,cliHorario=@cliHorario WHERE Id_Cliente = @Id_Cliente ", Conexion)
        oUComando.Parameters.Add("@cliCodigo", SqlDbType.VarChar, 20, "cliCodigo")
        oUComando.Parameters.Add("@cliNombre", SqlDbType.VarChar, 20, "cliNombre")
        oUComando.Parameters.Add("@cliDomicilio", SqlDbType.VarChar, 20, "cliDomicilio")
        oUComando.Parameters.Add("@cliTelefono", SqlDbType.VarChar, 20, "cliTelefono")
        oUComando.Parameters.Add("@cliLocalidad", SqlDbType.VarChar, 20, "cliLocalidad")
        oUComando.Parameters.Add("@cliHorario", SqlDbType.VarChar, 20, "cliHorario")

        oUComando.Parameters.Add("@Id_Cliente", SqlDbType.Int, 10, "Id_Cliente")

        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As sqlCommand
        oDComando = New SqlCommand("DELETE * FROM T_Clientes WHERE Id_Cliente = @Id_Cliente", Conexion)
        oDComando.Parameters.Add("@Id_Cliente", SqlDbType.Int, 10, "Id_Cliente")
        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Shared Function Crear() As CLIENTES
        If Crear_Cliente Is Nothing Then
            Crear_Cliente = New CLIENTES
        Else
            '   MsgBox("El objeto ya existe")
        End If

        Return Crear_Cliente
    End Function

    Function DEVUELVE_CLIENTES() As List(Of ENTIDADES.Cliente)
        Dim list As New List(Of ENTIDADES.Cliente)
        con = New SqlConnection
        cmd = New SqlCommand
        con.ConnectionString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;" & _
            "Initial Catalog = dataserv;" & _
            "Integrated Security=SSPI" & ";"
        cmd.Connection = con
        cmd.CommandText = "SELECT * FROM T_Clientes"
        con.Open()
        Reader = cmd.ExecuteReader
        While (Reader.Read())
            list.Add(CARGA_CLIENTE(Reader))
        End While
        Return list
    End Function

    Function CARGA_CLIENTE(ByVal reader As IDataReader) As ENTIDADES.Cliente
        Dim CLIENTE_DAT As New ENTIDADES.Cliente
        CLIENTE_DAT.IdCliente = reader.GetInt16(0)
        CLIENTE_DAT.cliCodigo = reader.GetString(1)
        CLIENTE_DAT.cliNombre = reader.GetString(2)
        CLIENTE_DAT.cliDomicilio = reader.GetString(3)
        CLIENTE_DAT.cliTelefono = reader.GetString(4)
        CLIENTE_DAT.cliLocalidad = reader.GetString(5)
        CLIENTE_DAT.cliHorario = reader.GetString(6)
        Return CLIENTE_DAT
    End Function

    Public Sub ACTUALIZA_CLIENTE(ByVal Mycliente As ENTIDADES.Cliente)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' INSERT COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("INSERT INTO T_Clientes(cliCodigo, cliNombre, cliDomicilio,cliTelefono,cliLocalidad,cliHorario) VALUES (@cliCodigo,@cliNombre,@cliDomicilio,@cliTelefono,@cliLocalidad,@cliHorario)", Conexion)
        oIComando.Parameters.AddWithValue("@cliCodigo", Mycliente.cliCodigo)
        oIComando.Parameters.AddWithValue("@cliNombre", Mycliente.cliNombre)
        oIComando.Parameters.AddWithValue("@cliDomicilio", Mycliente.cliDomicilio)
        oIComando.Parameters.AddWithValue("@cliTelefono", Mycliente.cliTelefono)
        oIComando.Parameters.AddWithValue("@cliLocalidad", Mycliente.cliLocalidad)
        oIComando.Parameters.AddWithValue("@cliHorario", Mycliente.cliHorario)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub MODIFICA_CLIENTE(ByVal Mycliente As ENTIDADES.Cliente)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' UPDATE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("UPDATE T_Clientes SET  cliCodigo=@cliCodigo ,cliNombre = @cliNombre, cliDomicilio = @cliDomicilio, cliTelefono = @cliTelefono, cliLocalidad =@cliLocalidad,cliHorario=@cliHorario WHERE Id_Cliente = @Id_Cliente ", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Cliente", Mycliente.IdCliente)
        oIComando.Parameters.AddWithValue("@cliCodigo", Mycliente.cliCodigo)
        oIComando.Parameters.AddWithValue("@cliNombre", Mycliente.cliNombre)
        oIComando.Parameters.AddWithValue("@cliDomicilio", Mycliente.cliDomicilio)
        oIComando.Parameters.AddWithValue("@cliTelefono", Mycliente.cliTelefono)
        oIComando.Parameters.AddWithValue("@cliLocalidad", Mycliente.cliLocalidad)
        oIComando.Parameters.AddWithValue("@cliHorario", Mycliente.cliHorario)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ELIMINA_CLIENTE(ByVal Mycliente As ENTIDADES.Cliente)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' DELETE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("DELETE T_Clientes WHERE Id_Cliente = @Id_Cliente", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Cliente", Mycliente.IdCliente)
        Try
            oIComando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("IMPOSIBLE ELIMINAR CLIENTE, EXISTE RELACION", MsgBoxStyle.Exclamation, "RELACION TICKET - CLIENTE")
        End Try

        Conexion.Close()

    End Sub

    Public Function BuscarId_Cliente(ByVal ID_Cliente)
        Dim NombreCliente As String
        con = New SqlConnection
        cmd = New SqlCommand
        con.ConnectionString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;" & _
            "Initial Catalog = dataserv;" & _
            "Integrated Security=SSPI" & ";"
        cmd.Connection = con
        cmd.CommandText = "SELECT * FROM T_Clientes WHERE Id_Cliente = " & ID_Cliente & " "
        con.Open()
        Reader = cmd.ExecuteReader
        While (Reader.Read())
            NombreCliente = Reader.GetString(2)
        End While
        Return NombreCliente
    End Function

End Class