﻿Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class PRODUCTOS
    Private Conexion As New SqlConnection()
    Private oDataAdapter As SqlDataAdapter
    Private oDataTable As DataTable

    Private con As SqlConnection
    Private cmd As SqlCommand
    Private Reader As SqlDataReader

    Public Shared Crear_Producto As PRODUCTOS

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New sqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As sqlCommand
        oSComando = New SqlCommand("SELECT * FROM T_Productos ORDER BY IdProducto", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As sqlCommand
        oIComando = New SqlCommand("INSERT INTO T_Productos(DesProducto, StkMinimo) VALUES (@DesProducto,@StkMinimo)", Conexion)
        oIComando.Parameters.Add("@DesProducto", SqlDbType.VarChar, 50, "DesProducto")
        oIComando.Parameters.Add("@StkMinimo", SqlDbType.Int, 10, "StkMinimo")
        oDataAdapter.InsertCommand = oIComando

        'UPDATE COMMAND
        Dim oUComando As sqlCommand
        oUComando = New SqlCommand("UPDATE T_Productos SET  IdProducto=@IdProducto ,DesProducto = @DesProducto, StkMinimo = @StkMinimo ", Conexion)
        oUComando.Parameters.Add("@IdProducto", SqlDbType.Int, 10, "idProducto")
        oUComando.Parameters.Add("@DesProducto", SqlDbType.VarChar, 20, "DesProducto")
        oUComando.Parameters.Add("@StkMinimo", SqlDbType.Int, 10, "StkMinimo")
        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As sqlCommand
        oDComando = New SqlCommand("DELETE * FROM T_Productos WHERE IdProducto = @IdProducto", Conexion)
        oDComando.Parameters.Add("@IdProducto", SqlDbType.Int, 10, "IdProducto")
        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Shared Function Crear() As PRODUCTOS
        If Crear_Producto Is Nothing Then
            Crear_Producto = New PRODUCTOS
        Else
            '   MsgBox("El objeto ya existe")
        End If

        Return Crear_Producto
    End Function

    Function DEVUELVE_PRODUCTOS() As List(Of ENTIDADES.Producto)
        Dim list As New List(Of ENTIDADES.Producto)
        con = New SqlConnection
        cmd = New SqlCommand
        con.ConnectionString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;" & _
            "Initial Catalog = dataserv;" & _
            "Integrated Security=SSPI" & ";"
        cmd.Connection = con
        cmd.CommandText = "SELECT * FROM T_Productos"
        con.Open()
        Reader = cmd.ExecuteReader
        While (Reader.Read())
            list.Add(CARGA_PRODUCTO(Reader))
        End While
        Return list
    End Function

    Function CARGA_PRODUCTO(ByVal reader As IDataReader) As ENTIDADES.Producto
        Dim PRODUCTO_DAT As New ENTIDADES.Producto
        PRODUCTO_DAT.IdProducto = reader.GetInt16(0)
        PRODUCTO_DAT.DesProducto = reader.GetString(1)
        PRODUCTO_DAT.StkMinimo = reader.GetInt16(2)
        Return PRODUCTO_DAT
    End Function

    Public Sub ACTUALIZA_PRODUCTO(ByVal Myproducto As ENTIDADES.Producto)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' INSERT COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("INSERT INTO T_Productos(DesProducto, StkMinimo) VALUES (@DesProducto,@StkMinimo)", Conexion)
        oIComando.Parameters.AddWithValue("@DesProducto", Myproducto.DesProducto)
        oIComando.Parameters.AddWithValue("@StkMinimo", Myproducto.StkMinimo)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub MODIFICA_PRODUCTO(ByVal Myproducto As ENTIDADES.Producto)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' UPDATE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("UPDATE T_Productos SET DesProducto = @DesProducto, StkMinimo = @StkMinimo WHERE IdProducto = @IdProducto ", Conexion)
        oIComando.Parameters.AddWithValue("@IdProducto", Myproducto.IdProducto)
        oIComando.Parameters.AddWithValue("@DesProducto", Myproducto.DesProducto)
        oIComando.Parameters.AddWithValue("@StkMinimo", Myproducto.StkMinimo)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ELIMINA_Producto(ByVal Myproducto As ENTIDADES.Producto)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' DELETE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("DELETE T_Productos WHERE IdProducto = @IdProducto", Conexion)
        oIComando.Parameters.AddWithValue("@IdProducto", Myproducto.IdProducto)
        Try
            oIComando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("IMPOSIBLE ELIMINAR PRODUCTO, EXISTE RELACION", MsgBoxStyle.Exclamation, "RELACION TICKET - PRODUCTO")
        End Try

        Conexion.Close()

    End Sub

    Public Function BuscarId_PRODUCTO(ByVal IdProducto)
        Dim NombreProducto As String
        con = New SqlConnection
        cmd = New SqlCommand
        con.ConnectionString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;" & _
            "Initial Catalog = dataserv;" & _
            "Integrated Security=SSPI" & ";"
        cmd.Connection = con
        cmd.CommandText = "SELECT * FROM T_Productos WHERE IdProducto = " & IdProducto & " "
        con.Open()
        Reader = cmd.ExecuteReader
        While (Reader.Read())
            NombreProducto = Reader.GetString(2)
        End While
        Return NombreProducto
    End Function

End Class
