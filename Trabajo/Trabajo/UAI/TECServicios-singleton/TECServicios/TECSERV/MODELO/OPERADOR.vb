Imports System.Data.sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class OPERADOR
    Private Conexion As New SqlConnection()
    Private oDataAdapter As SqlDataAdapter
    Private oDataTable As DataTable

    Private con As SqlConnection
    Private cmd As SqlCommand
    Private Reader As SqlDataReader

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New SqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As SqlCommand
        oSComando = New SqlCommand("SELECT * FROM T_Operadores", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("INSERT INTO T_Operadores (logApellido,logNombre,logTelefono,logNomUsuario,logClave,Id_Grupo) VALUES (@logApellido,@logNombre,@logTelefono,@logNomUsuario,@logClave,@Id_Grupo)", Conexion)
        oIComando.Parameters.Add("@logApellido", SqlDbType.VarChar, 20, "logApellido")
        oIComando.Parameters.Add("@logNombre", SqlDbType.VarChar, 20, "logNombre")
        oIComando.Parameters.Add("@logTelefono", SqlDbType.VarChar, 20, "logTelefono")
        oIComando.Parameters.Add("@logNomUsuario", SqlDbType.VarChar, 20, "logNomUsuario")
        oIComando.Parameters.Add("@logClave", SqlDbType.VarChar, 50, "logClave")
        oIComando.Parameters.Add("@Id_Grupo", SqlDbType.VarChar, 20, "Id_Grupo")

        oIComando.Parameters.Add("@Id_Operador", SqlDbType.Int, 10, "Id_Operador")

        oDataAdapter.InsertCommand = oIComando

        'UPDATE COMMAND
        Dim oUComando As SqlCommand
        oUComando = New SqlCommand("UPDATE T_Operadores SET  logApellido = @logApellido, logNombre = @logNombre, logTelefono = @logTelefono, logNomUsuario = @logNomUsuario, logClave = @logClave, Id_Grupo = @Id_Grupo WHERE Id_Operador = @Id_Operador ", Conexion)
        oUComando.Parameters.Add("@logApellido", SqlDbType.VarChar, 20, "logApellido")
        oUComando.Parameters.Add("@logNombre", SqlDbType.VarChar, 20, "logNombre")
        oUComando.Parameters.Add("@logTelefono", SqlDbType.VarChar, 20, "logTelefono")
        oUComando.Parameters.Add("@logNomUsuario", SqlDbType.VarChar, 20, "logNomUsuario")
        oUComando.Parameters.Add("@logClave", SqlDbType.VarChar, 50, "logClave")
        oUComando.Parameters.Add("@Id_Grupo", SqlDbType.VarChar, 20, "Id_Grupo")

        oUComando.Parameters.Add("@Id_Operador", SqlDbType.Int, 10, "Id_Operador")

        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As SqlCommand
        oDComando = New SqlCommand("DELETE T_Operadores WHERE Id_Operador = @Id_Operador", Conexion)
        oDComando.Parameters.Add("@Id_Operador", SqlDbType.Int, 10, "Id_Operador")

        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtOPERADOR As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtOPERADOR)
        Catch oEX As Exception
            MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub

    Public Function BuscarApellido_Operador(ByVal Id_Operador)
        Dim IdOperador As String
        con = New SqlConnection
        cmd = New SqlCommand
        con.ConnectionString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;Initial Catalog=dataserv;Integrated Security=SSPI;"
        cmd.Connection = con
        cmd.CommandText = "SELECT * FROM T_Operadores WHERE Id_Operador = " & Id_Operador & " "
        con.Open()
        Reader = cmd.ExecuteReader
        While (Reader.Read())
            IdOperador = Reader.GetString(4)
        End While
        Return IdOperador
    End Function

    Public Function BuscarID_Operador(ByVal Apellido)
        Dim IdOperador As Integer
        con = New SqlConnection
        cmd = New SqlCommand
        con.ConnectionString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;Initial Catalog=dataserv;Integrated Security=SSPI;"
        cmd.Connection = con
        cmd.CommandText = "SELECT * FROM T_Operadores WHERE logNomUsuario = '" & Apellido & "' "
        con.Open()
        Reader = cmd.ExecuteReader
        While (Reader.Read())
            IdOperador = Reader.GetInt32(0)
        End While
        Return IdOperador
    End Function

    Public Sub ACTUALIZA_OPERADORES(ByVal MyOperadores As ENTIDADES.Operadores)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' INSERT COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("INSERT INTO T_Operadores (logApellido,logNombre,logTelefono,logNomUsuario,logClave,Id_Grupo) VALUES (@logApellido,@logNombre,@logTelefono,@logNomUsuario,@logClave,@Id_Grupo)", Conexion)
        oIComando.Parameters.AddWithValue("@logApellido", MyOperadores.opeApellido)
        oIComando.Parameters.AddWithValue("@logNombre", MyOperadores.opeNombre)
        oIComando.Parameters.AddWithValue("@logTelefono", MyOperadores.opeTelefono)
        oIComando.Parameters.AddWithValue("@logNomUsuario", MyOperadores.opeNomUsuario)
        oIComando.Parameters.AddWithValue("@logClave", MyOperadores.opeClave)
        oIComando.Parameters.AddWithValue("@logNombre", MyOperadores.opeNombre)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub MODIFICA_OPERADORES(ByVal MyOperadores As ENTIDADES.Operadores)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' UPDATE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("UPDATE T_Operadores SET  logApellido = @logApellido, logNombre = @logNombre, logTelefono = @logTelefono, logNomUsuario = @logNomUsuario, logClave = @logClave, Id_Grupo = @Id_Grupo WHERE Id_Operador = @Id_Operador ", Conexion)
        oIComando.Parameters.AddWithValue("@logApellido", MyOperadores.opeApellido)
        oIComando.Parameters.AddWithValue("@logNombre", MyOperadores.opeNombre)
        oIComando.Parameters.AddWithValue("@logTelefono", MyOperadores.opeTelefono)
        oIComando.Parameters.AddWithValue("@logNomUsuario", MyOperadores.opeNomUsuario)
        oIComando.Parameters.AddWithValue("@logClave", MyOperadores.opeClave)
        oIComando.Parameters.AddWithValue("@logNombre", MyOperadores.opeNombre)
        oIComando.Parameters.AddWithValue("@Id_Operador", MyOperadores.opeIdoperador)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ELIMINA_OPERADORES(ByVal MyOperadores As ENTIDADES.Operadores)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' DELETE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("DELETE T_Operadores WHERE Id_Operador = @Id_Operador", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Operador", MyOperadores.opeIdoperador)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub
End Class
