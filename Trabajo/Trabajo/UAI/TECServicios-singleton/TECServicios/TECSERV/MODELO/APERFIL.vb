Imports System.Data.sql
Imports System.Data
Imports System.Data.SqlClient

Public Class APERFIL
    Private Conexion As New SqlConnection()
    Private oDataAdapter As SqlDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.ConectarAud
        oDataAdapter = New SqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As SqlCommand
        oSComando = New SqlCommand("SELECT * FROM T_AuditoriaPerfiles", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("INSERT INTO T_AuditoriaPerfiles (Id_AudPerfil,audpeNombre,audpePermisos,audpeGrupo,audFecha,audHora,audTipo,audUsuario) VALUES (@Id_AudPerfil,@audpeNombre,@audpePermisos,@audpeGrupo,@audFecha,@audHora,@audTipo,@audUsuario)", Conexion)
        oIComando.Parameters.Add("@audpeNombre", SqlDbType.VarChar, 20, "audpeNombre")
        oIComando.Parameters.Add("@audpePermisos", SqlDbType.VarChar, 50, "audpePermisos")
        oIComando.Parameters.Add("@audpeGrupo", SqlDbType.VarChar, 20, "audpeGrupo")
        oIComando.Parameters.Add("@audFecha", SqlDbType.VarChar, 20, "audFecha")
        oIComando.Parameters.Add("@audHora", SqlDbType.VarChar, 20, "audHora")
        oIComando.Parameters.Add("@audTipo", SqlDbType.VarChar, 20, "audTipo")
        oIComando.Parameters.Add("@audUsuario", SqlDbType.VarChar, 20, "audUsuario")

        oIComando.Parameters.Add("@Id_AudPerfil", SqlDbType.Int, 10, "Id_AudPerfil")

        oDataAdapter.InsertCommand = oIComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtTECNICOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtTECNICOS)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class

