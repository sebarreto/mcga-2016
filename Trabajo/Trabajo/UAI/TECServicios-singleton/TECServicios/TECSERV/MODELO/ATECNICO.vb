Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient

Public Class ATECNICO
    Private Conexion As New sqlConnection()
    Private oDataAdapter As sqlDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.ConectarAud
        oDataAdapter = New sqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As sqlCommand
        oSComando = New sqlCommand("SELECT * FROM T_AuditoriaTecnicos", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As sqlCommand
        oIComando = New SqlCommand("INSERT INTO T_AuditoriaTecnicos (Id_Tecnico,tecNombre,tecApellido,tecDomicilio,tecLocalidad,tecTelefono,audTipo,audFecha,audHora,audUsuario) VALUES (@Id_Tecnico,@tecNombre,@tecApellido,@tecDomicilio,@tecLocalidad,@tecTelefono,@audTipo,@audFecha,@audHora,@audUsuario)", Conexion)
        oIComando.Parameters.Add("@tecNombre", SqlDbType.VarChar, 20, "tecNombre")
        oIComando.Parameters.Add("@tecApellido", SqlDbType.VarChar, 20, "tecApellido")
        oIComando.Parameters.Add("@tecDomicilio", SqlDbType.VarChar, 20, "tecDomicilio")
        oIComando.Parameters.Add("@tecLocalidad", SqlDbType.VarChar, 20, "tecLocalidad")
        oIComando.Parameters.Add("@tecTelefono", SqlDbType.VarChar, 20, "tecTelefono")
        oIComando.Parameters.Add("@audTipo", SqlDbType.VarChar, 20, "audTipo")
        oIComando.Parameters.Add("@audFecha", SqlDbType.VarChar, 20, "audFecha")
        oIComando.Parameters.Add("@audHora", SqlDbType.VarChar, 20, "audHora")
        oIComando.Parameters.Add("@audUsuario", SqlDbType.VarChar, 20, "audUsuario")

        oIComando.Parameters.Add("@Id_Tecnico", SqlDbType.Int, 10, "Id_Tecnico")

        oDataAdapter.InsertCommand = oIComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtTECNICOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtTECNICOS)
        Catch oEX As Exception
            MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class

