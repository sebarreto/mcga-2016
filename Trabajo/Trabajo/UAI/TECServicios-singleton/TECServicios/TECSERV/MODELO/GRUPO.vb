Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class GRUPO

    Private Conexion As New sqlConnection()
    Private oDataAdapter As sqlDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New sqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As sqlCommand
        oSComando = New sqlCommand("SELECT * FROM T_Grupos", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As sqlCommand
        oIComando = New sqlCommand("INSERT INTO T_Grupos(GrNombre,GrDescripcion) VALUES (@GrNombre,@GrDescripcion)", Conexion)
        oIComando.Parameters.Add("@GrNombre", SqlDbType.VarChar, 20, "GrNombre")
        oIComando.Parameters.Add("@GrDescripcion", SqlDbType.VarChar, 200, "GrDescripcion")

        oIComando.Parameters.Add("@Id_Grupo", SqlDbType.Int, 10, "Id_Grupo")
        oDataAdapter.InsertCommand = oIComando

        'UPDATE COMMAND
        Dim oUComando As sqlCommand
        oUComando = New sqlCommand("UPDATE T_Grupos SET  GrNombre = @GrNombre ,GrDescripcion = @GrDescripcion WHERE Id_Grupo = @Id_Grupo ", Conexion)
        oUComando.Parameters.Add("@GrNombre", SqlDbType.VarChar, 20, "GrNombre")
        oUComando.Parameters.Add("@GrDescripcion", SqlDbType.VarChar, 200, "GrDescripcion")

        oUComando.Parameters.Add("@Id_Grupo", SqlDbType.Int, 10, "Id_Grupo")
        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As sqlCommand
        oDComando = New SqlCommand("DELETE T_Grupos  WHERE Id_Grupo = @Id_Grupo", Conexion)
        oDComando.Parameters.Add("@Id_Grupo", SqlDbType.Int, 10, "Id_Grupo")
        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtCLIENTES As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtCLIENTES)
        Catch oEX As Exception
            MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub

    Public Sub ACTUALIZA_GRUPO(ByVal MyGRUPO As ENTIDADES.Grupo)
        Conexion = ModuloConexion.Conectar
        'oDataAdapter = New SqlDataAdapter()

        ' INSERT COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("INSERT INTO T_Grupos(GrNombre,GrDescripcion) VALUES (@GrNombre,@GrDescripcion)", Conexion)
        oIComando.Parameters.AddWithValue("@GrNombre", MyGRUPO.GrNombre)
        oIComando.Parameters.AddWithValue("@GrDescripcion", MyGRUPO.GrDescripcion)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub MODIFICA_GRUPO(ByVal MyGRUPO As ENTIDADES.Grupo)
        Conexion = ModuloConexion.Conectar
        'oDataAdapter = New SqlDataAdapter()

        ' INSERT COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("UPDATE T_Grupos SET  GrNombre = @GrNombre ,GrDescripcion = @GrDescripcion WHERE Id_Grupo = @Id_Grupo", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Grupo", MyGRUPO.Id_Grupo)
        oIComando.Parameters.AddWithValue("@GrNombre", MyGRUPO.GrNombre)
        oIComando.Parameters.AddWithValue("@GrDescripcion", MyGRUPO.GrDescripcion)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ELIMINA_GRUPO(ByVal MyGRUPO As ENTIDADES.Grupo)
        Conexion = ModuloConexion.Conectar
        'oDataAdapter = New SqlDataAdapter()

        ' INSERT COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("DELETE T_Grupos  WHERE Id_Grupo = @Id_Grupo", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Grupo", MyGRUPO.Id_Grupo)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub


End Class

