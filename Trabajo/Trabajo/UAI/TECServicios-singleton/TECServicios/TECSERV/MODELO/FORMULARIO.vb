Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class FORMULARIO

    Private Conexion As New sqlConnection()
    Private oDataAdapter As sqlDataAdapter
    Private oDataTable As DataTable

    Public Shared Crear_Formulario As FORMULARIO

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New sqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As sqlCommand
        oSComando = New sqlCommand("SELECT * FROM T_Formularios ORDER BY Id_Formulario", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As sqlCommand
        oIComando = New sqlCommand("INSERT INTO T_Formularios(frmNombre,frmDescripcion) VALUES (@frmNombre,frmDescripcion)", Conexion)
        oIComando.Parameters.Add("@frmNombre", SqlDbType.VarChar, 20, "frmNombre")
        oIComando.Parameters.Add("@frmDescripcion", SqlDbType.VarChar, 200, "frmDescripcion")

        oIComando.Parameters.Add("@Id_Formulario", SqlDbType.Int, 10, "Id_Formulario")
        oDataAdapter.InsertCommand = oIComando

        'UPDATE COMMAND
        Dim oUComando As sqlCommand
        oUComando = New sqlCommand("UPDATE T_Formularios SET  frmNombre = @frmNombre, frmDescripcion = @frmDescripcion  WHERE Id_Formulario = @Id_Formulario ", Conexion)
        oUComando.Parameters.Add("@frmNombre", SqlDbType.VarChar, 20, "frmNombre")
        oUComando.Parameters.Add("@frmDescripcion", SqlDbType.VarChar, 200, "frmDescripcion")

        oUComando.Parameters.Add("@Id_Formulario", SqlDbType.Int, 10, "Id_Formulario")
        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As sqlCommand
        oDComando = New SqlCommand("DELETE T_Formularios  WHERE Id_Formulario = @Id_Formulario", Conexion)
        oDComando.Parameters.Add("@Id_Formulario", SqlDbType.Int, 10, "Id_Formulario")
        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtFORMULARIOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtFORMULARIOS)
        Catch oEX As Exception
            MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub

    Public Sub ACTUALIZA_FORMULARIO(ByVal Myformulario As ENTIDADES.Formulario)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' INSERT COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("INSERT INTO T_Formularios(frmNombre,frmDescripcion) VALUES (@frmNombre,frmDescripcion)", Conexion)
        oIComando.Parameters.AddWithValue("@frmNombre", Myformulario.forNombre)
        oIComando.Parameters.AddWithValue("@frmDescripcion", Myformulario.forDescripcion)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub MODIFICA_FORMULARIO(ByVal Myformulario As ENTIDADES.Formulario)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' UPDATE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("UPDATE T_Formularios SET  frmNombre = @frmNombre, frmDescripcion = @frmDescripcion  WHERE Id_Formulario = @Id_Formulario", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Formulario", Myformulario.forIdformulario)
        oIComando.Parameters.AddWithValue("@frmNombre", Myformulario.forNombre)
        oIComando.Parameters.AddWithValue("@frmDescripcion", Myformulario.forDescripcion)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ELIMINA_FORMULARIO(ByVal Myformulario As ENTIDADES.Formulario)
        Conexion = ModuloConexion.Conectar
        ' oDataAdapter = New SqlDataAdapter()

        ' DELETE COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("DELETE * FROM T_Formularios  WHERE Id_Formulario = @Id_Formulario", Conexion)
        oIComando.Parameters.AddWithValue("@Id_Formulario", Myformulario.forIdformulario)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Shared Function Crear() As FORMULARIO
        If Crear_Formulario Is Nothing Then
            Crear_Formulario = New FORMULARIO
        Else
            '   MsgBox("El objeto ya existe")
        End If

        Return Crear_Formulario
    End Function

End Class

