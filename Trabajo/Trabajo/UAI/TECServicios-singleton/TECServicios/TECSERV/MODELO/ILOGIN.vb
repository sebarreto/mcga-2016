﻿Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class ILOGIN
    Private Conexion As New sqlConnection()
    Private oDataAdapter As sqlDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.ConectarAud
        oDataAdapter = New sqlDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As sqlCommand
        oSComando = New SqlCommand("SELECT * FROM T_AuditoriaIngresos", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As sqlCommand
        oIComando = New SqlCommand("INSERT INTO T_AuditoriaIngresos(log_ingrUsuario,log_ingrFecha,log_ingrTipo) VALUES (@Grlog_ingrUsuario,@Grlog_ingrFecha,@Grlog_ingrTipo)", Conexion)
        oIComando.Parameters.Add("@Grlog_ingrUsuario", SqlDbType.VarChar, 50, "log_ingrUsuario")
        oIComando.Parameters.Add("@Grlog_ingrFecha", SqlDbType.VarChar, 10, "log_ingrFecha")
        oIComando.Parameters.Add("@Grlog_ingrTipo", SqlDbType.VarChar, 10, "log_ingrTipo")
        oDataAdapter.InsertCommand = oIComando

        'UPDATE COMMAND
        Dim oUComando As sqlCommand
        oUComando = New SqlCommand("UPDATE T_AuditoriaIngresos SET  log_ingrUsuario,log_ingrFecha = @Grlog_ingrUsuario ,@Grlog_ingrFecha WHERE log_ingrTipo = @log_ingrTipo ", Conexion)
        oUComando.Parameters.Add("@Grlog_ingrUsuario", SqlDbType.VarChar, 50, "log_ingrUsuario")
        oUComando.Parameters.Add("@Grlog_ingrFecha", SqlDbType.VarChar, 10, "log_ingrFecha")
        oUComando.Parameters.Add("@Grlog_ingrTipo", SqlDbType.VarChar, 10, "log_ingrTipo")

        oUComando.Parameters.Add("@Id_Grupo", SqlDbType.Int, 10, "Id_Grupo")
        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As sqlCommand
        oDComando = New SqlCommand("DELETE T_AuditoriaIngresos  WHERE log_ingrUsuario = @log_ingrUsuario", Conexion)
        oDComando.Parameters.Add("@log_ingrUsuario", SqlDbType.VarChar, 50, "log_ingrUsuario")
        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Sub ACTUALIZA_ILOGIN(ByVal MyLOGIN As ENTIDADES.aIngresos)
        Conexion = ModuloConexion.ConectarAud
        'oDataAdapter = New SqlDataAdapter()

        ' INSERT COMMAND
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("INSERT INTO T_AuditoriaIngresos(log_ingrUsuario,log_ingrFecha,log_ingrTipo) VALUES (@gringrUsario,@gringrFecha,@gringrTipo)", Conexion)
        oIComando.Parameters.AddWithValue("@gringrUsario", MyLOGIN.log_ingrUsuario)
        oIComando.Parameters.AddWithValue("@gringrFecha", MyLOGIN.log_ingrFecha)
        oIComando.Parameters.AddWithValue("@gringrTipo", MyLOGIN.log_ingrTipo)
        oIComando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

End Class

