Imports System.Data.sql
Imports ENTIDADES
Public Class CLIENTES
    Private modCLIENTES As MODELO.CLIENTES ' OBJETO
    Private dtCLIENTES As DataTable
    Private dvCLIENTES As DataView
    Private cliCodigo As String
    Private cliNombre As String
    Private OCLIENTES As ENTIDADES.Cliente ' OBJETO

    Public Sub New()
        modCLIENTES = New MODELO.CLIENTES
        dtCLIENTES = modCLIENTES.OBTENER_TABLA()
    End Sub

    Public Sub GUARDAR_CLIENTE(ByVal Mycliente As ENTIDADES.Cliente)
        Try
            modCLIENTES.ACTUALIZA_CLIENTE(Mycliente)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Public Sub ELIMINA_CLIENTE(ByVal Mycliente As ENTIDADES.Cliente)
        Try
            modCLIENTES.ELIMINA_CLIENTE(Mycliente)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Public Sub MODIFICA_CLIENTE(ByVal Mycliente As ENTIDADES.Cliente)
        Try
            modCLIENTES.MODIFICA_CLIENTE(Mycliente)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Public Function OBTENER_CLIENTE(ByVal NRO) As DataRow
        Return dtCLIENTES.Rows.Find(NRO)
    End Function

    Public Function OBTENER_CLIENTES() As DataTable
        Return dtCLIENTES
    End Function

    Public Function BuscarID_Cliente(ByVal ID_Cliente)
        Dim NombreCli As String
        NombreCli = modCLIENTES.BuscarId_Cliente(ID_Cliente)
        Return NombreCli
    End Function

    Public Function DEVLUEVE_CLIENTES() As List(Of ENTIDADES.Cliente)

        Return modCLIENTES.DEVUELVE_CLIENTES()

    End Function

End Class