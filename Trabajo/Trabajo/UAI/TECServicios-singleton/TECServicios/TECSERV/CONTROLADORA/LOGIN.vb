Imports System.Data.Sql
Imports ENTIDADES

Public Class LOGIN
    Private modLOGIN As MODELO.LOGIN
    Private dtLOGIN As DataTable
    Private dvLOGIN As DataView

    Public Sub New()
        modLOGIN = New MODELO.LOGIN
        dtLOGIN = modLOGIN.OBTENER_TABLA()
    End Sub

  Public Function OBTENER_USUARIOS(ByVal APELLIDO As Integer) As DataView
    'CREO UN DATAVIEW Y LE ASIGNO EL CONTENIDO DEL DATATABLE dtCONTACTO
    dvLOGIN = New DataView
    dvLOGIN.Table = dtLOGIN
    dvLOGIN.RowFilter = ("Id_Usuario like '*" & APELLIDO & "*'")
    Return dvLOGIN
    dvLOGIN = Nothing
  End Function

    Public Function OBTENER_USUARIOS() As DataView
        'CREO UN DATAVIEW QUE LE ASIGNO EL DATATABLE "dtCONTACTOS"
        dvLOGIN = New DataView
        dvLOGIN.Table = dtLOGIN
        Return dvLOGIN
        dvLOGIN = Nothing
    End Function

  Public Function OBTENER_USUARIO(ByVal Nombre As String) As DataRow
    Try
      'Return dtLOGIN.Select("Nombre LIKE '%" & Nombre & "%'")(0)
      Return dtLOGIN.Select("logNombre = '" & Nombre & "'")(0)
    Catch oEX As Exception
  '    MsgBox(oEX.Message)
    Finally
    End Try
  End Function

  Public Function OBTENER_USUARIO2(ByVal NRO As String) As DataRow
    Try
      Return dtLOGIN.Rows.Find(NRO)
    Catch oEX As Exception
      MsgBox(oEX.Message)
    Finally
    End Try
  End Function

    Public Function CREAR_USUARIO() As DataRow
        Return dtLOGIN.NewRow
    End Function

    Public Sub AGREGAR_USUARIO(ByVal drUSUARIO As DataRow)
        Try
            dtLOGIN.Rows.Add(drUSUARIO)
            ACTUALIZA_TABLA()

        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub ACTUALIZA_TABLA()
        modLOGIN.ACTUALIZAR_TABLA(dtLOGIN)
    End Sub

    Public Sub MODIFICAR_USUARIO(ByVal drUSUARIO As DataRow)
        Dim rowAUTO As DataRow
        rowAUTO = dtLOGIN.Rows.Find(drUSUARIO("Id_Usuario"))
        rowAUTO("Nombre") = drUSUARIO("Nombre")
        rowAUTO("Contraseņa") = drUSUARIO("Contraseņa")
        rowAUTO("PerfilUsr") = drUSUARIO("PerfilUsr")

        ACTUALIZA_TABLA()
    End Sub

    Public Sub ELIMINAR_USUARIO(ByVal NRO As Integer)
        Dim rowUSUARIO As DataRow
        rowUSUARIO = dtLOGIN.Rows.Find(NRO)
        rowUSUARIO.Delete()
        ACTUALIZA_TABLA()
    End Sub

End Class
