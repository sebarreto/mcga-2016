﻿Imports System.Data.Sql
Imports ENTIDADES
Public Class PRODUCTOS
    Private modPRODUCTOS As MODELO.PRODUCTOS ' OBJETO
    Private dtPRODUCTOS As DataTable
    Private dvPRODUCTOS As DataView
    Private OPRODUCTOS As ENTIDADES.Producto ' OBJETO

    Public Sub New()
        modPRODUCTOS = New MODELO.PRODUCTOS
        dtPRODUCTOS = modPRODUCTOS.OBTENER_TABLA()
    End Sub

    Public Sub GUARDAR_PRODUCTO(ByVal MyPRODUCTO As ENTIDADES.Producto)
        Try
            modPRODUCTOS.ACTUALIZA_PRODUCTO(MyPRODUCTO)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Public Sub ELIMINA_PRODUCTO(ByVal MyPRODUCTO As ENTIDADES.Producto)
        Try
            modPRODUCTOS.ELIMINA_PRODUCTO(MyPRODUCTO)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Public Sub MODIFICA_PRODUCTO(ByVal MyPRODUCTO As ENTIDADES.Producto)
        Try
            modPRODUCTOS.MODIFICA_PRODUCTO(MyPRODUCTO)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Public Function OBTENER_PRODUCTO(ByVal NRO) As DataRow
        Return dtPRODUCTOS.Rows.Find(NRO)
    End Function

    Public Function OBTENER_PRODUCTOS() As DataTable
        Return dtPRODUCTOS
    End Function

    Public Function BuscarID_PRODUCTO(ByVal ID_PRODUCTO)
        Dim NombreCli As String
        NombreCli = modPRODUCTOS.BuscarId_PRODUCTO(ID_PRODUCTO)
        Return NombreCli
    End Function

    Public Function DEVLUEVE_PRODUCTOS() As List(Of ENTIDADES.Producto)

        Return modPRODUCTOS.DEVUELVE_PRODUCTOS()

    End Function

End Class
