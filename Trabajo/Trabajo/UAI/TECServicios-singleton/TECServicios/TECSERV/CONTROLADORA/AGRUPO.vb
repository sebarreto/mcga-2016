Imports System.Data.sql

Public Class AGRUPO

    Private modGRUPOS As MODELO.AGRUPO
    Private dtGRUPOS As DataTable
    Private dvGRUPOS As DataView

    Public Sub New()
        modGRUPOS = New MODELO.AGRUPO
        dtGRUPOS = modGRUPOS.OBTENER_TABLA()
    End Sub

   Public Function CREAR_GRUPO() As DataRow
        Return dtGRUPOS.NewRow
    End Function

    Public Sub AGREGAR_GRUPO(ByVal drTecnico As DataRow)
        Try
            dtGRUPOS.Rows.Add(drTecnico)
            ACTUALIZA_TABLA()
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub ACTUALIZA_TABLA()
        modGRUPOS.ACTUALIZAR_TABLA(dtGRUPOS)
    End Sub

    Public Function OBTENER_GRUPOS() As DataTable
        Return dtGRUPOS
    End Function

End Class
