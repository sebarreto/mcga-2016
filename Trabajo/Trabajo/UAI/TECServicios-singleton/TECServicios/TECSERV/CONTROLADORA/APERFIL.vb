Imports System.Data.sql

Public Class APERFIL

    Private modPERFILES As MODELO.APERFIL
    Private dtPERFILES As DataTable
    Private dvPERFILES As DataView

    Public Sub New()
        modPERFILES = New MODELO.APERFIL
        dtPERFILES = modPERFILES.OBTENER_TABLA()
    End Sub

   Public Function CREAR_PERFIL() As DataRow
        Return dtPERFILES.NewRow
    End Function

    Public Sub AGREGAR_PERFIL(ByVal drTecnico As DataRow)
        Try
            dtPERFILES.Rows.Add(drTecnico)
            ACTUALIZA_TABLA()
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub ACTUALIZA_TABLA()
        modPERFILES.ACTUALIZAR_TABLA(dtPERFILES)
    End Sub

    Public Function OBTENER_PERFILES() As DataTable
        Return dtPERFILES
    End Function

End Class
