Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class TICKET
    Private modTICKETS As MODELO.TICKET  ' OBJETO
    Private dtTICKETS As DataTable
    Private dvTICKETS As DataView
   
Public Sub New()
    modTICKETS = New MODELO.TICKET
    dtTICKETS = modTICKETS.OBTENER_TABLA()
End Sub

Public Function CREAR_TICKET() As DataRow
    Return dtTICKETS.NewRow
End Function

Public Sub AGREGAR_TICKET(ByVal drTICKET As DataRow)
        Try
            'seba
            drTICKET("Id_Ticket") = 0
            dtTICKETS.Rows.Add(drTICKET)
            ACTUALIZA_TABLA()
        Catch ex As Exception
            Throw (ex)
        End Try
End Sub

Private Sub ACTUALIZA_TABLA()
    modTICKETS.ACTUALIZAR_TABLA(dtTICKETS)
End Sub

    Public Sub MODIFICAR_TICKET(ByVal drTICKET As DataRow)
        Dim rowCLIENTE As DataRow

        rowCLIENTE = dtTICKETS.Rows.Find(drTICKET("Id_Ticket"))
        rowCLIENTE("Id_Cliente") = drTICKET("Id_Cliente")
      '  rowCLIENTE("NomCliente") = drTICKET("NomCliente")
        rowCLIENTE("Id_Operador") = drTICKET("Id_Operador")
        rowCLIENTE("Id_Tecnico") = drTICKET("Id_Tecnico")
        rowCLIENTE("Falla") = drTICKET("Falla")
        rowCLIENTE("Nconforme") = drTICKET("Nconforme")
        rowCLIENTE("Cobranza") = drTICKET("Cobranza")
        rowCLIENTE("Monto") = drTICKET("Monto")
        rowCLIENTE("Fecha") = drTICKET("Fecha")
        rowCLIENTE("Hora") = drTICKET("Hora")

        ACTUALIZA_TABLA()
 End Sub

    Public Function OBTENER_TICKET(ByVal NRO) As DataRow
        Return dtTICKETS.Rows.Find(NRO)
    End Function

    Public Function OBTENER_TICKETS() As DataTable
        Return dtTICKETS
    End Function

    Public Sub ELIMINAR_TICKET(ByVal NRO As Integer)
        Dim rowTICKET As DataRow
        rowTICKET = dtTICKETS.Rows.Find(NRO)
        rowTICKET.Delete()

        ACTUALIZA_TABLA()
    End Sub

    Public Function BuscarID_OPERADOR(ByVal Operador)
    Dim Op As String
      Op = modTICKETS.BuscarID_OPERADOR(Operador)
      Return Op
    End Function

    Public Sub GUARDAR_TICKET(ByVal Myticket As ENTIDADES.Ticket)
        Try
            modTICKETS.ACTUALIZA_TICKET(Myticket)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Public Sub ELIMINA_TICKET(ByVal Myticket As ENTIDADES.Ticket)
        Try
            modTICKETS.ELIMINA_TICKET(Myticket)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Public Sub MODIFICA_TICKET(ByVal Myticket As ENTIDADES.Ticket)
        Try
            modTICKETS.MODIFICA_TICKET(Myticket)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

End Class
