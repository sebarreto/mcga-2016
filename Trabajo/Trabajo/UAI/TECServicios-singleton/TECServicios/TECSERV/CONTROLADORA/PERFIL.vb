Imports System.Data.sql

Public Class PERFIL

    Private modPERFILES As MODELO.PERFIL
    Private dtPERFILES As DataTable
    Private dvPERFILES As DataView
    Private model As New MODELO.PERFIL

    Public Sub New()
        modPERFILES = New MODELO.PERFIL
        dtPERFILES = modPERFILES.OBTENER_TABLA()
    End Sub

    Public Function CREAR_PERFIL() As DataRow
        Return dtPERFILES.NewRow
    End Function

    Public Sub AGREGAR_PERFIL(ByVal drPERFIL As DataRow)
        Try
            dtPERFILES.Rows.Add(drPERFIL)
            ACTUALIZA_TABLA()
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub ACTUALIZA_TABLA()
        modPERFILES.ACTUALIZAR_TABLA(dtPERFILES)
    End Sub

    Public Function OBTENER_PERFIL(ByVal NRO) As DataRow
        Return dtPERFILES.Rows.Find(NRO)
    End Function

    Public Function OBTENER_PERFILES() As DataTable
        Return dtPERFILES
    End Function

    Public Sub ELIMINAR_PERFIL(ByVal Grupo, ByVal Form, ByVal Perm)
      model.ELIMINAR_PERFIL(Grupo, Form, Perm)
    End Sub

    Public Sub MODIFICAR_PERFIL(ByVal Grupo, ByVal Form, ByVal Perm)
      model.MODIFICAR_PERFIL(Grupo, Form, Perm)
    End Sub

    Public Function OBTENER_PERFIL_HAB(ByVal Nombre As String) As DataRow
        Try
            'Return dtLOGIN.Select("Nombre LIKE '%" & Nombre & "%'")(0)
            Return dtPERFILES.Select("peGrupo = '" & Nombre & "'")(0)
        Catch oEX As Exception
            '    MsgBox(oEX.Message)
        Finally
        End Try
    End Function


    Public Sub GUARDAR_PERFIL(ByVal Myperfil As ENTIDADES.Perfil)
        Try
            modPERFILES.ACTUALIZA_PERFIL(Myperfil)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Public Sub ELIMINA_PERFIL(ByVal Myperfil As ENTIDADES.Perfil)
        Try
            modPERFILES.ELIMINA_PERFIL(Myperfil)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Public Sub MODIFICA_PERFIL(ByVal Myperfil As ENTIDADES.Perfil)
        Try
            modPERFILES.MODIFICA_PERFIL(Myperfil)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

End Class
