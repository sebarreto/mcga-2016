Imports System.Data.sql

Public Class AFORMULARIO

    Private modFORMULARIO As MODELO.AFORMULARIO
    Private dtFORMULARIO As DataTable
    Private dvFORMULARIO As DataView

    Public Sub New()
        modFORMULARIO = New MODELO.AFORMULARIO
        dtFORMULARIO = modFORMULARIO.OBTENER_TABLA()
    End Sub

   Public Function CREAR_FORMULARIO() As DataRow
        Return dtFORMULARIO.NewRow
    End Function

    Public Sub AGREGAR_FORMULARIO(ByVal drTecnico As DataRow)
        Try
            dtFORMULARIO.Rows.Add(drTecnico)
            ACTUALIZA_TABLA()
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub ACTUALIZA_TABLA()
        modFORMULARIO.ACTUALIZAR_TABLA(dtFORMULARIO)
    End Sub

    Public Function OBTENER_FORMULARIOS() As DataTable
        Return dtFORMULARIO
    End Function

End Class
