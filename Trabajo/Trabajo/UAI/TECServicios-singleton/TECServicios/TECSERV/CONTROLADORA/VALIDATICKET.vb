﻿Public Class VALIDATICKET

    Private _tecnico As MODELO.VTECNICO
    Private _cliente As MODELO.VCLIENTE
    Private _operador As MODELO.VOPERADOR
    Private MyTecnico As ENTIDADES.Ticket

    Public Function EsValido(ByRef Vticket As ENTIDADES.Ticket) As Boolean

        Dim elegible As Boolean = True

        _tecnico = New MODELO.VTECNICO
        _cliente = New MODELO.VCLIENTE
        _operador = New MODELO.VOPERADOR

        If (_tecnico.buscaTecnico(Vticket.ticTecnico) = False) Then
            Return False
        End If
        If (_cliente.buscaCliente(Vticket.ticCliente) = False) Then
            Return False
        End If
        If (_operador.buscaOperador(Vticket.ticOperador) = False) Then
            Return False
        End If

        Return True

    End Function

End Class
