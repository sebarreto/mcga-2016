﻿Imports System.Data.Sql
Imports ENTIDADES

Public Class ILOGIN
    Private modLOGIN As MODELO.ILOGIN
    Private dtLOGIN As DataTable
    Private dvLOGIN As DataView

    Public Sub New()
        modLOGIN = New MODELO.ILOGIN
    End Sub

    Public Sub GUARDAR_ILOGIN(ByVal myLogin As ENTIDADES.aIngresos)
        Try
            modLOGIN.ACTUALIZA_ILOGIN(myLogin)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

End Class
