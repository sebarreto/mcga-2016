Imports System.Data.Sql
Imports ENTIDADES

Public Class FORMULARIO
    Private OFORMULARIOS As ENTIDADES.Formulario ' OBJETO

    Private modFORMULARIOS As MODELO.FORMULARIO
    Private dtFORMULARIOS As DataTable
    Private dvFORMULARIOS As DataView

    Public Sub New()
        modFORMULARIOS = New MODELO.FORMULARIO
        dtFORMULARIOS = modFORMULARIOS.OBTENER_TABLA()
    End Sub

    Public Sub GUARDAR_FORMULARIO(ByVal Myformulario As ENTIDADES.Formulario)
        Try
            modFORMULARIOS.ACTUALIZA_FORMULARIO(Myformulario)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Public Sub ELIMINA_FORMULARIO(ByVal Myformulario As ENTIDADES.Formulario)
        Try
            modFORMULARIOS.ELIMINA_FORMULARIO(Myformulario)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Public Sub MODIFICA_FORMULARIO(ByVal Myformulario As ENTIDADES.Formulario)
        Try
            modFORMULARIOS.MODIFICA_FORMULARIO(Myformulario)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub


    Public Function CREAR_FORMULARIO() As DataRow
        Return dtFORMULARIOS.NewRow
    End Function

    Public Sub AGREGAR_FORMULARIO(ByVal drFORMULARIO As DataRow)
        Try
            dtFORMULARIOS.Rows.Add(drFORMULARIO)
            ACTUALIZA_TABLA()
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub ACTUALIZA_TABLA()
        modFORMULARIOS.ACTUALIZAR_TABLA(dtFORMULARIOS)
    End Sub

    Public Sub MODIFICAR_FORMULARIO(ByVal drFORMULARIO As DataRow)
        Dim rowGRUPO As DataRow

        rowGRUPO = dtFORMULARIOS.Rows.Find(drFORMULARIO("Id_Formulario"))
        rowGRUPO("frmNombre") = drFORMULARIO("frmNombre")
        rowGRUPO("frmDescripcion") = drFORMULARIO("frmDescripcion")

        ACTUALIZA_TABLA()
    End Sub

    Public Function OBTENER_FORMULARIO(ByVal NRO) As DataRow
        Return dtFORMULARIOS.Rows.Find(NRO)
    End Function

    Public Function OBTENER_FORMULARIOS() As DataTable
        Return dtFORMULARIOS
    End Function

Public Function OBTENER_ID_FORMULARIO(ByVal NRO) As String
    Dim IdFROM() As DataRow
      IdFROM = dtFORMULARIOS.Select("frmNombre = '" & NRO & "'")
      Return IdFROM(0)(0)
    End Function

    Public Sub ELIMINAR_FORMULARIO(ByVal NRO As Integer)
        Dim rowGRUPO As DataRow
        rowGRUPO = dtFORMULARIOS.Rows.Find(NRO)
        rowGRUPO.Delete()

        ACTUALIZA_TABLA()
    End Sub

End Class