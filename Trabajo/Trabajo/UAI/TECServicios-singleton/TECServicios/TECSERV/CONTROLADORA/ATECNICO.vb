Imports System.Data.sql

Public Class ATECNICO

    Private modTECNICOS As MODELO.ATECNICO   ' OBJETO
    Private dtTECNICOS As DataTable
    Private dvTECNICOS As DataView

    Public Sub New()
        modTECNICOS = New MODELO.ATECNICO
        dtTECNICOS = modTECNICOS.OBTENER_TABLA()
    End Sub

   Public Function CREAR_TECNICO() As DataRow
        Return dtTECNICOS.NewRow
    End Function

    Public Sub AGREGAR_TECNICO(ByVal drTecnico As DataRow)
        Try
            dtTECNICOS.Rows.Add(drTecnico)
            ACTUALIZA_TABLA()
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub ACTUALIZA_TABLA()
        modTECNICOS.ACTUALIZAR_TABLA(dtTECNICOS)
    End Sub

    Public Function OBTENER_TECNICOS() As DataTable
        Return dtTECNICOS
    End Function

End Class
