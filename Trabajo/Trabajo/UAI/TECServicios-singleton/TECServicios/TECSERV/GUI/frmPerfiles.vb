Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class frmPerfiles

    Private con As sqlConnection
    Private cmd As sqlCommand
    Private Reader As sqlDataReader

    Private oPERFIL As CONTROLADORA.PERFIL
    Private oGRUPO As CONTROLADORA.GRUPO
    Private oFORMULARIO As CONTROLADORA.FORMULARIO
    Private oPERMISO As CONTROLADORA.PERMISO

    Private drPERFIL As DataRow
    Private dtPERFIL As DataTable
    Private dtGRUPO As DataTable
    Private dvPERFIL As DataView

    Private oAPERFIL As CONTROLADORA.APERFIL
    Private drAPERFIL As DataRow
    Private dtAPERFIL As DataTable
    Private Perfiles As New CONTROLADORA.PERFIL

    Private MyPERFIL As ENTIDADES.Perfil

    Private drnomGrupo As DataRow

    Dim OPT, OptForm, MP, Rta, BAN As Integer
    Dim OpForm, GrupoSeleccionado, FormSeleccionado, PermisoSeleccionado As String 'VARIABLE QUE CONTIENE LOS PERMISOS
    Dim cAlta, cBaja, cModificar As String
    Dim Operacion, Autorizacion, Formulario As String
    Dim cant, x, IdGRUPO, IdFORM, IdPERMISO As Integer
    Dim salvar, NombreGR, NombreForm, NombrePER As String

    Private Sub frmPerfiles_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        oGRUPO = New CONTROLADORA.GRUPO
        oFORMULARIO = New CONTROLADORA.FORMULARIO
        oPERMISO = New CONTROLADORA.PERMISO

        Formulario = "PERFILES"
        Cargar_grupos()
        lvwPerfiles.Enabled = True
        OPT = 0
        oPERFIL = New CONTROLADORA.PERFIL
        oGRUPO = New CONTROLADORA.GRUPO
        dtGRUPO = oGRUPO.OBTENER_GRUPOS
        dtPERFIL = oPERFIL.OBTENER_PERFILES

        oAPERFIL = New CONTROLADORA.APERFIL
        dtAPERFIL = oAPERFIL.OBTENER_PERFILES
        Cargar_Formularios()
        Cargar_Permisos()
        Cargar_Data_Grid()

    End Sub

    Private Sub tbPerfiles_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbPerfiles.ButtonClick

        Select Case tbPerfiles.Buttons.IndexOf(e.Button)
            Case 0 'ALTA
                ' Operacion = "Alta"
                ' Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, Operacion)
                ' If Autorizacion = "NO" Then
                '   MsgBox("Usted no tiene Permisos para Ingresar un Nuevo Perfil")
                '   Exit Sub
                ' End If

                OPT = 1
                Cancelar()
                Habilitar()
                tbSave.Enabled = True
                MP = 0

            Case 1 'MODIFICAR
                Operacion = "Modificar"
                '  Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, Operacion)
                '  If Autorizacion = "NO" Then
                '    MsgBox("Usted no tiene Permisos para Modificar un Perfil")
                '    Exit Sub
                '  End If
                OPT = 2
                gbPermisos.Enabled = True
                tbSave.Enabled = True
                'Perfiles.ELIMINAR_PERFIL(GrupoSeleccionado, FormSeleccionado, PermisoSeleccionado)
                Guardar_Datos_mod()

            Case 2 'BAJA
                Dim a As Integer
                Operacion = "Baja"
                ' Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, Operacion)
                ' If Autorizacion = "NO" Then
                '   MsgBox("Usted no tiene Permisos para Eliminar un Perfil")
                '   Exit Sub
                ' End If
                Rta = MsgBox("�Desea Eliminar Permanentemente el Perfil seleccionado?", MsgBoxStyle.YesNo, "TECSERV")
                If Rta = 7 Then Exit Sub

                Perfiles = New CONTROLADORA.PERFIL
                'Perfiles.ELIMINAR_PERFIL(GrupoSeleccionado, FormSeleccionado, PermisoSeleccionado)
                Guardar_Datos_baj()
                'Generar_Auditoria("BAJA")
                Cancelar()
                Cargar_Data_Grid()
                MsgBox("Perfil eliminado con Exito", MsgBoxStyle.Information, "TECSERV")
                Exit Sub

            Case 3 'GUARDAR
                If GrupoSeleccionado <> "" And FormSeleccionado <> "" And PermisoSeleccionado <> "" Then
                    If OPT = 1 Then
                        drPERFIL = oPERFIL.CREAR_PERFIL()
                        'Guardar_Datos()
                        'oPERFIL.AGREGAR_PERFIL(drPERFIL)
                        Guardar_Datos_alt()
                        MsgBox("Perfil creado con Exito", MsgBoxStyle.Information, "TECSERV")
                        'Generar_Auditoria("ALTA")
                    Else
                        drPERFIL = oPERFIL.CREAR_PERFIL()
                        'Guardar_Datos()
                        'oPERFIL.AGREGAR_PERFIL(drPERFIL)
                        Guardar_Datos_alt()
                        'Generar_Auditoria("MODIFICACION")
                        Cancelar()
                        MsgBox("Perfil modificado con Exito", MsgBoxStyle.Information, "TECSERV")
                    End If
                    Cargar_Data_Grid()
                    Cancelar()
                Else
                    Exit Sub
                End If
            Case 4 'CANCELAR
                Cancelar()

            Case 5 'VOLVER
                Me.Close()

        End Select
    End Sub

    Private Sub Guardar_Datos()
        drPERFIL("Id_Grupo") = GrupoSeleccionado
        drPERFIL("Id_Formulario") = FormSeleccionado
        drPERFIL("Id_Permiso") = PermisoSeleccionado
    End Sub

    Private Sub Guardar_Datos_alt()
        MyPERFIL = New ENTIDADES.Perfil
        MyPERFIL.perIdgrupo = GrupoSeleccionado
        MyPERFIL.perFormulario = FormSeleccionado
        MyPERFIL.perPermiso = PermisoSeleccionado
        oPERFIL.GUARDAR_PERFIL(MyPERFIL)
    End Sub

    Private Sub Guardar_Datos_mod()
        MyPERFIL = New ENTIDADES.Perfil
        MyPERFIL.perIdgrupo = GrupoSeleccionado
        MyPERFIL.perFormulario = FormSeleccionado
        MyPERFIL.perPermiso = PermisoSeleccionado
        oPERFIL.MODIFICA_PERFIL(MyPERFIL)
    End Sub

    Private Sub Guardar_Datos_baj()
        MyPERFIL = New ENTIDADES.Perfil
        MyPERFIL.perIdgrupo = GrupoSeleccionado
        MyPERFIL.perFormulario = FormSeleccionado
        MyPERFIL.perPermiso = PermisoSeleccionado
        oPERFIL.ELIMINA_PERFIL(MyPERFIL)
    End Sub

    Private Sub Generar_Auditoria(ByVal TipoEvento)
        drAPERFIL = oAPERFIL.CREAR_PERFIL()
        drAPERFIL("Id_AudPerfil") = (PermisoSeleccionado + FormSeleccionado + GrupoSeleccionado)
        drAPERFIL("audpePermisos") = PermisoSeleccionado
        drAPERFIL("audpeNombre") = FormSeleccionado
        drAPERFIL("audpeGrupo") = GrupoSeleccionado
        drAPERFIL("audFecha") = Microsoft.VisualBasic.DateString
        drAPERFIL("audHora") = Microsoft.VisualBasic.Left(Microsoft.VisualBasic.TimeString, 5)
        drAPERFIL("audTipo") = TipoEvento
        drAPERFIL("audUsuario") = UsuarioLogeado

        oAPERFIL.AGREGAR_PERFIL(drAPERFIL)
    End Sub

    Private Sub Habilitar()
        gbPermisos.Enabled = True
        cant = CLBPermisos.Items.Count
        For x = 0 To (cant - 1)
            CLBPermisos.SetItemChecked(x, False)
        Next
    End Sub

    Private Sub Cancelar()
        tbSave.Enabled = False
        gbPermisos.Enabled = False
        cmbGrupos.Text = ""
        dtPERFIL = oPERFIL.OBTENER_PERFILES
    End Sub

    Private Sub cmbGrupos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbGrupos.SelectedIndexChanged
        NombreGR = cmbGrupos.Text
        IdGRUPO = oGRUPO.OBTENER_ID_GRUPO(NombreGR)
        GrupoSeleccionado = IdGRUPO
    End Sub

    Private Sub lstForms_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstForms.SelectedIndexChanged
        NombreForm = lstForms.Text
        IdFORM = oFORMULARIO.OBTENER_ID_FORMULARIO(NombreForm)
        FormSeleccionado = IdFORM
    End Sub

    Private Sub CLBPermisos_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles CLBPermisos.ItemCheck
        If BAN <> 5 Then
            NombrePER = CLBPermisos.Text
        End If
        IdPERMISO = oPERMISO.OBTENER_ID_PERMISO(NombrePER)
        PermisoSeleccionado = IdPERMISO
        BAN = 0
    End Sub

    Private Sub Cargar_Data_Grid()
        Dim IdGR, IdFRM, IdPER As Integer
        Dim NomGR, NomFRM, NomPER As String

        DataGridView1.Columns.Clear()
        DataGridView1.Columns.Add("Grupo", "Grupo")
        DataGridView1.Columns.Add("Formulario", "Formulario")
        DataGridView1.Columns.Add("Permiso", "Permiso")
        con = New sqlConnection
        cmd = New sqlCommand
        con.ConnectionString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;Initial Catalog=dataserv;Integrated Security=SSPI;"
        cmd.Connection = con
        cmd.CommandText = "SELECT * FROM T_Perfiles"
        con.Open()
        Reader = cmd.ExecuteReader
        While (Reader.Read())
            IdGR = Reader.GetInt32(0)
            cmbGrupos.SelectedIndex = Val(IdGR - 1)
            IdFRM = Reader.GetInt32(1)
            lstForms.SelectedIndex = Val(IdFRM - 1)
            IdPER = Reader.GetInt32(2)
            CLBPermisos.SelectedIndex = Val(IdPER - 1)
            DataGridView1.Rows.Add(cmbGrupos.Text, lstForms.Text, CLBPermisos.Text)
            'DataGridView1.Rows.Add(Reader.GetInt32(0), Reader.GetInt32(1), Reader.GetInt32(2))
        End While
        Reader.Close()
        cmbGrupos.SelectedIndex = 0
        lstForms.SelectedIndex = 0
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Dim i As Integer
        cant = CLBPermisos.Items.Count
        For x = 0 To (cant - 1)
            CLBPermisos.SetItemChecked(x, False)
        Next
        i = DataGridView1.CurrentRow.Index
        NombreGR = DataGridView1.Item(0, i).Value
        IdGRUPO = oGRUPO.OBTENER_ID_GRUPO(NombreGR)
        cmbGrupos.SelectedIndex = IdGRUPO - 1

        NombreForm = DataGridView1.Item(1, i).Value
        IdFORM = oFORMULARIO.OBTENER_ID_FORMULARIO(NombreForm)
        lstForms.SelectedIndex = IdFORM - 1

        BAN = 5
        NombrePER = DataGridView1.Item(2, i).Value
        IdPERMISO = oPERMISO.OBTENER_ID_PERMISO(NombrePER)
        CLBPermisos.SetItemChecked((IdPERMISO - 1), True)

        'CLBPermisos.SelectedIndex = Val(DataGridView1.Item(2, i).Value) - 1
        'CLBPermisos.SetItemChecked((Val(DataGridView1.Item(2, i).Value) - 1), True)
    End Sub

    Private Sub Cargar_Grupos()
        con = New sqlConnection
        cmd = New sqlCommand
        con.ConnectionString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;Initial Catalog=dataserv;Integrated Security=SSPI;"
        cmd.Connection = con
        cmd.CommandText = "SELECT * FROM T_Grupos"
        con.Open()
        Reader = cmd.ExecuteReader
        While (Reader.Read())
            cmbGrupos.Items.Add(Reader.GetString(1))
        End While
    End Sub

    Private Sub Cargar_Formularios()
        con = New sqlConnection
        cmd = New sqlCommand
        con.ConnectionString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;Initial Catalog=dataserv;Integrated Security=SSPI;"
        cmd.Connection = con
        cmd.CommandText = "SELECT * FROM T_Formularios ORDER BY Id_Formulario"
        con.Open()
        Reader = cmd.ExecuteReader
        While (Reader.Read())
            lstForms.Items.Add(Reader.GetString(1))
        End While
        Reader.Close()
    End Sub

    Private Sub Cargar_Permisos()
        con = New sqlConnection
        con.ConnectionString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;Initial Catalog=dataserv;Integrated Security=SSPI;"
        cmd.Connection = con
        cmd.CommandText = "SELECT * FROM T_Permisos ORDER BY perNombre"
        con.Open()
        Reader = cmd.ExecuteReader
        While (Reader.Read())
            CLBPermisos.Items.Add(Reader.GetString(1))
        End While
        Reader.Close()
    End Sub

End Class