<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFormularios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
Me.GroupBox2 = New System.Windows.Forms.GroupBox
Me.txtDescripcion = New System.Windows.Forms.TextBox
Me.Label2 = New System.Windows.Forms.Label
Me.txtNro = New System.Windows.Forms.TextBox
Me.Label3 = New System.Windows.Forms.Label
Me.txtNombre = New System.Windows.Forms.TextBox
Me.Label1 = New System.Windows.Forms.Label
Me.GroupBox1 = New System.Windows.Forms.GroupBox
Me.lvwFormularios = New System.Windows.Forms.ListView
Me.Nro = New System.Windows.Forms.ColumnHeader
Me.Formulario = New System.Windows.Forms.ColumnHeader
Me.tbFormularios = New System.Windows.Forms.ToolBar
Me.tbAdd = New System.Windows.Forms.ToolBarButton
Me.tbEdit = New System.Windows.Forms.ToolBarButton
Me.tbDelete = New System.Windows.Forms.ToolBarButton
Me.tbSave = New System.Windows.Forms.ToolBarButton
Me.tbCancelar = New System.Windows.Forms.ToolBarButton
Me.tbExit = New System.Windows.Forms.ToolBarButton
Me.GroupBox2.SuspendLayout()
Me.GroupBox1.SuspendLayout()
Me.SuspendLayout()
'
'GroupBox2
'
Me.GroupBox2.Controls.Add(Me.txtDescripcion)
Me.GroupBox2.Controls.Add(Me.Label2)
Me.GroupBox2.Controls.Add(Me.txtNro)
Me.GroupBox2.Controls.Add(Me.Label3)
Me.GroupBox2.Controls.Add(Me.txtNombre)
Me.GroupBox2.Controls.Add(Me.Label1)
Me.GroupBox2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.GroupBox2.Location = New System.Drawing.Point(12, 48)
Me.GroupBox2.Name = "GroupBox2"
Me.GroupBox2.Size = New System.Drawing.Size(212, 166)
Me.GroupBox2.TabIndex = 16
Me.GroupBox2.TabStop = False
Me.GroupBox2.Text = "Detalles del Formulario"
'
'txtDescripcion
'
Me.txtDescripcion.Location = New System.Drawing.Point(22, 111)
Me.txtDescripcion.Multiline = True
Me.txtDescripcion.Name = "txtDescripcion"
Me.txtDescripcion.Size = New System.Drawing.Size(184, 49)
Me.txtDescripcion.TabIndex = 19
'
'Label2
'
Me.Label2.AutoSize = True
Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.Label2.Location = New System.Drawing.Point(19, 93)
Me.Label2.Name = "Label2"
Me.Label2.Size = New System.Drawing.Size(75, 15)
Me.Label2.TabIndex = 18
Me.Label2.Text = "Descripcion"
'
'txtNro
'
Me.txtNro.Enabled = False
Me.txtNro.Location = New System.Drawing.Point(77, 36)
Me.txtNro.Name = "txtNro"
Me.txtNro.Size = New System.Drawing.Size(38, 21)
Me.txtNro.TabIndex = 15
'
'Label3
'
Me.Label3.AutoSize = True
Me.Label3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.Label3.Location = New System.Drawing.Point(44, 39)
Me.Label3.Name = "Label3"
Me.Label3.Size = New System.Drawing.Size(27, 15)
Me.Label3.TabIndex = 14
Me.Label3.Text = "Nro"
'
'txtNombre
'
Me.txtNombre.Location = New System.Drawing.Point(77, 63)
Me.txtNombre.Name = "txtNombre"
Me.txtNombre.Size = New System.Drawing.Size(129, 21)
Me.txtNombre.TabIndex = 0
'
'Label1
'
Me.Label1.AutoSize = True
Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.Label1.Location = New System.Drawing.Point(19, 66)
Me.Label1.Name = "Label1"
Me.Label1.Size = New System.Drawing.Size(52, 15)
Me.Label1.TabIndex = 0
Me.Label1.Text = "Nombre"
'
'GroupBox1
'
Me.GroupBox1.Controls.Add(Me.lvwFormularios)
Me.GroupBox1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.GroupBox1.Location = New System.Drawing.Point(240, 48)
Me.GroupBox1.Name = "GroupBox1"
Me.GroupBox1.Size = New System.Drawing.Size(204, 166)
Me.GroupBox1.TabIndex = 15
Me.GroupBox1.TabStop = False
Me.GroupBox1.Text = "Formularios Creados"
'
'lvwFormularios
'
Me.lvwFormularios.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Nro, Me.Formulario})
Me.lvwFormularios.FullRowSelect = True
Me.lvwFormularios.GridLines = True
Me.lvwFormularios.Location = New System.Drawing.Point(6, 20)
Me.lvwFormularios.MultiSelect = False
Me.lvwFormularios.Name = "lvwFormularios"
Me.lvwFormularios.Size = New System.Drawing.Size(192, 140)
Me.lvwFormularios.TabIndex = 5
Me.lvwFormularios.UseCompatibleStateImageBehavior = False
Me.lvwFormularios.View = System.Windows.Forms.View.Details
'
'Nro
'
Me.Nro.Text = "Nro"
Me.Nro.Width = 35
'
'Formulario
'
Me.Formulario.Text = "Formulario"
Me.Formulario.Width = 153
'
'tbFormularios
'
Me.tbFormularios.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbAdd, Me.tbEdit, Me.tbDelete, Me.tbSave, Me.tbCancelar, Me.tbExit})
Me.tbFormularios.ButtonSize = New System.Drawing.Size(56, 56)
Me.tbFormularios.DropDownArrows = True
Me.tbFormularios.Location = New System.Drawing.Point(0, 0)
Me.tbFormularios.Name = "tbFormularios"
Me.tbFormularios.ShowToolTips = True
Me.tbFormularios.Size = New System.Drawing.Size(458, 42)
Me.tbFormularios.TabIndex = 14
'
'tbAdd
'
Me.tbAdd.ImageIndex = 1
Me.tbAdd.Name = "tbAdd"
Me.tbAdd.Text = "Agregar"
'
'tbEdit
'
Me.tbEdit.ImageIndex = 2
Me.tbEdit.Name = "tbEdit"
Me.tbEdit.Text = "Modificar"
'
'tbDelete
'
Me.tbDelete.ImageIndex = 6
Me.tbDelete.Name = "tbDelete"
Me.tbDelete.Text = "Eliminar"
'
'tbSave
'
Me.tbSave.Enabled = False
Me.tbSave.ImageIndex = 0
Me.tbSave.Name = "tbSave"
Me.tbSave.Text = "Guardar"
'
'tbCancelar
'
Me.tbCancelar.ImageIndex = 5
Me.tbCancelar.Name = "tbCancelar"
Me.tbCancelar.Text = "Cancelar"
'
'tbExit
'
Me.tbExit.ImageIndex = 3
Me.tbExit.Name = "tbExit"
Me.tbExit.Text = "Volver"
'
'frmFormularios
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.ClientSize = New System.Drawing.Size(458, 227)
Me.Controls.Add(Me.GroupBox2)
Me.Controls.Add(Me.GroupBox1)
Me.Controls.Add(Me.tbFormularios)
Me.Name = "frmFormularios"
Me.Text = "Formularios"
Me.GroupBox2.ResumeLayout(False)
Me.GroupBox2.PerformLayout()
Me.GroupBox1.ResumeLayout(False)
Me.ResumeLayout(False)
Me.PerformLayout()

End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtNro As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lvwFormularios As System.Windows.Forms.ListView
    Friend WithEvents Nro As System.Windows.Forms.ColumnHeader
    Friend WithEvents Formulario As System.Windows.Forms.ColumnHeader
    Friend WithEvents tbFormularios As System.Windows.Forms.ToolBar
    Friend WithEvents tbAdd As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbEdit As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbDelete As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbSave As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbCancelar As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbExit As System.Windows.Forms.ToolBarButton
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
