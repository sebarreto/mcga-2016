Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class frmClientes
    Private drCLIENTE As DataRow
    Private oCLIENTE As CONTROLADORA.CLIENTES
    Private dtCLIENTE As DataTable

    '  Private oACLIENTE As CONTROLADORA.ACLIENTES
    Private drACLIENTE As DataRow
    Private dtACLIENTE As DataTable

    Private MyCLIENTE As ENTIDADES.Cliente
    Private MyCONTEXT As CONTROLADORA.CONTEXVERI
    Private MyESTRATEA As CONTROLADORA.VERIFICAMONTOA
    Private MyESTRATEB As CONTROLADORA.VERIFICAMONTOB
    Private MyESTRATEC As CONTROLADORA.VERIFICAMONTOC

    Dim BAN, x, Rta As Integer
    Dim NC As String
    Dim Autorizacion, Grupo, Formulario, Operacion As String

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        BAN = 1
        GroupBox2.Enabled = True
        Limpiar()
        btnGuardar.Enabled = True
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        BAN = 2
        GroupBox2.Enabled = True
        btnGuardar.Enabled = True
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If txtCliNombre.Text <> "" And txtCliCuit.Text <> "" And txtCliDomicilio.Text <> "" And txtCliTelefono.Text <> "" And txtCliLocalidad.Text <> "" And txtCliHorario.Text <> "" Then
            If BAN = 1 Then
                'AGREGA UN NUEVO CLIENTE
                'drCLIENTE = oCLIENTE.CREAR_CLIENTE
                Alta_Clientes()
                'Generar_Auditoria("ALTA")
                MsgBox("El nuevo Cliente ha sido dado de Alta con �xito", MsgBoxStyle.Information, "TECSERV")

            ElseIf BAN = 2 Then
                Modificacion_Clientes()
                'oCLIENTE.MODIFICAR_CLIENTE(drCLIENTE)
                'Generar_Auditoria("MODIFICACION")
                MsgBox("El Cliente ha sido Modificado con �xito", MsgBoxStyle.Information, "TECSERV")
            End If

            oCLIENTE = New CONTROLADORA.CLIENTES
            dtCLIENTE = oCLIENTE.OBTENER_CLIENTES
            'LlenarListView()
            cargar_lista()
            Limpiar()
            GroupBox2.Enabled = False
            btnGuardar.Enabled = False
        Else
            MsgBox("Faltan Campos a completar!!", MsgBoxStyle.Exclamation, "TECSERV")
        End If
    End Sub

    Private Sub frmClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim x As Integer
        Formulario = "2"
        For x = 1 To 3
            Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, x)
            If x = 1 And Autorizacion = "NO" Then
                btnAgregar.Visible = False
            ElseIf x = 2 And Autorizacion = "NO" Then
                btnEliminar.Visible = False
            ElseIf x = 3 And Autorizacion = "NO" Then
                btnModificar.Visible = False
            End If
        Next
        GroupBox2.Enabled = False
        If frmPrincipal.AgregarToolStripMenuItem.Text = "Cancelar" Then
            btnAceptar.Enabled = True
        Else
            btnAceptar.Enabled = False
        End If
        Limpiar()
        oCLIENTE = New CONTROLADORA.CLIENTES
        dtCLIENTE = oCLIENTE.OBTENER_CLIENTES
        'LlenarListView()
        cargar_lista()
        'oACLIENTE = New CONTROLADORA.ACLIENTES
        ' dtACLIENTE = oACLIENTE.OBTENER_CLIENTES
    End Sub

    Private Sub Alta_Clientes()

        MyCLIENTE = New ENTIDADES.Cliente
        MyCLIENTE.cliDomicilio = UCase(Trim(txtCliDomicilio.Text))
        MyCLIENTE.cliCodigo = UCase(Trim(txtCliCuit.Text))
        MyCLIENTE.cliNombre = UCase(Trim(txtCliNombre.Text))
        MyCLIENTE.cliTelefono = UCase(Trim(txtCliTelefono.Text))
        MyCLIENTE.cliLocalidad = UCase(Trim(txtCliLocalidad.Text))
        MyCLIENTE.cliHorario = UCase(Trim(txtCliHorario.Text))

        oCLIENTE.GUARDAR_CLIENTE(MyCLIENTE)

        MyESTRATEA = New CONTROLADORA.VERIFICAMONTOA
        MyCONTEXT = New CONTROLADORA.CONTEXVERI(MyESTRATEA)
        MyCONTEXT.Contexto(MyCLIENTE)

        MyESTRATEB = New CONTROLADORA.VERIFICAMONTOB
        MyCONTEXT = New CONTROLADORA.CONTEXVERI(MyESTRATEB)
        MyCONTEXT.Contexto(MyCLIENTE)

        MyESTRATEC = New CONTROLADORA.VERIFICAMONTOC
        MyCONTEXT = New CONTROLADORA.CONTEXVERI(MyESTRATEC)
        MyCONTEXT.Contexto(MyCLIENTE)

        ' drCLIENTE("cliNombre") = UCase(Trim(txtCliNombre.Text))
        ' drCLIENTE("cliCodigo") = UCase(Trim(txtCliCuit.Text))
        ' drCLIENTE("cliDomicilio") = UCase(Trim(txtCliDomicilio.Text))
        ' drCLIENTE("cliTelefono") = UCase(Trim(txtCliTelefono.Text))
        ' drCLIENTE("cliLocalidad") = UCase(Trim(txtCliLocalidad.Text))
        ' drCLIENTE("cliHorario") = UCase(Trim(txtCliHorario.Text))
    End Sub

    Private Sub Modificacion_Clientes()

        MyCLIENTE = New ENTIDADES.Cliente
        MyCLIENTE.IdCliente = txtCliNro.Text
        MyCLIENTE.cliDomicilio = UCase(Trim(txtCliDomicilio.Text))
        MyCLIENTE.cliCodigo = UCase(Trim(txtCliCuit.Text))
        MyCLIENTE.cliNombre = UCase(Trim(txtCliNombre.Text))
        MyCLIENTE.cliTelefono = UCase(Trim(txtCliTelefono.Text))
        MyCLIENTE.cliLocalidad = UCase(Trim(txtCliLocalidad.Text))
        MyCLIENTE.cliHorario = UCase(Trim(txtCliHorario.Text))

        oCLIENTE.MODIFICA_CLIENTE(MyCLIENTE)

        'drCLIENTE("cliNombre") = UCase(Trim(txtCliNombre.Text))
        'drCLIENTE("cliCodigo") = UCase(Trim(txtCliCuit.Text))
        'drCLIENTE("cliDomicilio") = UCase(Trim(txtCliDomicilio.Text))
        'drCLIENTE("cliTelefono") = UCase(Trim(txtCliTelefono.Text))
        'drCLIENTE("cliLocalidad") = UCase(Trim(txtCliLocalidad.Text))
        'drCLIENTE("cliHorario") = UCase(Trim(txtCliHorario.Text))

    End Sub

    Private Sub Baja_Clientes()

        MyCLIENTE = New ENTIDADES.Cliente
        MyCLIENTE.IdCliente = txtCliNro.Text
        MyCLIENTE.cliDomicilio = UCase(Trim(txtCliDomicilio.Text))
        MyCLIENTE.cliCodigo = UCase(Trim(txtCliCuit.Text))
        MyCLIENTE.cliNombre = UCase(Trim(txtCliNombre.Text))
        MyCLIENTE.cliTelefono = UCase(Trim(txtCliTelefono.Text))
        MyCLIENTE.cliLocalidad = UCase(Trim(txtCliLocalidad.Text))
        MyCLIENTE.cliHorario = UCase(Trim(txtCliHorario.Text))

        oCLIENTE.ELIMINA_CLIENTE(MyCLIENTE)

    End Sub

    'Private Sub Generar_Auditoria(ByVal Tipo)
    '    'SE GENERA LA AUDITORIA
    '    drACLIENTE = oACLIENTE.CREAR_ACLIENTE()
    '    drACLIENTE("Id_Cliente") = txtCliNro.Text
    '    drACLIENTE("cliNombre") = UCase(Trim(txtCliNombre.Text))
    '    drACLIENTE("cliCodigo") = UCase(Trim(txtCliCuit.Text))
    '    drACLIENTE("cliDomicilio") = UCase(Trim(txtCliDomicilio.Text))
    '    drACLIENTE("cliTelefono") = UCase(Trim(txtCliTelefono.Text))
    '    drACLIENTE("cliLocalidad") = UCase(Trim(txtCliLocalidad.Text))
    '    drACLIENTE("cliHorario") = UCase(Trim(txtCliHorario.Text))
    '    'GUARDAR AUDITORIA
    '    drACLIENTE("cliFecha") = Microsoft.VisualBasic.DateString
    '    drACLIENTE("cliHora") = Microsoft.VisualBasic.Left(Microsoft.VisualBasic.TimeString, 5)
    '    drACLIENTE("cliTipo") = Tipo
    '    drACLIENTE("Usuario") = UsuarioLogeado

    '    oACLIENTE.AGREGAR_ACLIENTE(drACLIENTE)
    'End Sub

    Private Sub Limpiar()
        txtCliNro.Text = ""
        txtCliNombre.Text = ""
        txtCliDomicilio.Text = ""
        txtCliTelefono.Text = ""
        txtCliLocalidad.Text = ""
        txtCliHorario.Text = ""
        txtCliCuit.Text = ""
    End Sub

    ' Private Sub LlenarListView()
    ' Dim lvCLIENTES As ListViewItem
    ''dvAUTOS.Table = dtAUTOS
    '    lvwClientes.Items.Clear()
    '    For Each rwclientes As DataRow In dtCLIENTE.Rows
    '        lvCLIENTES = New ListViewItem(rwclientes(0).ToString())
    '        lvCLIENTES.SubItems.Add(rwclientes(1).ToString)
    '        lvCLIENTES.SubItems.Add(rwclientes(2).ToString)
    '        lvCLIENTES.SubItems.Add(rwclientes(3).ToString)
    '        lvwClientes.Items.Add(lvCLIENTES)
    '    Next
    'End Sub

    Private Sub cargar_lista()
        Dim lvCLIENTES As ListViewItem
        Dim Conexion As New SqlConnection()
        lvwClientes.Items.Clear()
        Dim strSQL As String
        strSQL = "SELECT * FROM T_Clientes"

        Conexion = ModuloConexion.Conectar

        Dim command As New SqlCommand(strSQL, Conexion)
        Dim reader As SqlDataReader = command.ExecuteReader()

        While reader.Read()
            lvCLIENTES = New ListViewItem(reader.GetInt32(0))
            lvCLIENTES.SubItems.Add(reader.GetString(1))
            lvCLIENTES.SubItems.Add(reader.GetString(2))
            lvCLIENTES.SubItems.Add(reader.GetString(5))
            lvwClientes.Items.Add(lvCLIENTES)
        End While

        reader.Close()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub lvwClientes_ItemActivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwClientes.ItemActivate
        GroupBox2.Enabled = False
        btnGuardar.Enabled = False
        drCLIENTE = oCLIENTE.OBTENER_CLIENTE(lvwClientes.FocusedItem.Text)
        txtCliNro.Text = drCLIENTE("Id_Cliente")
        txtCliNombre.Text = drCLIENTE("cliNombre")
        txtCliCuit.Text = drCLIENTE("cliCodigo")
        txtCliDomicilio.Text = drCLIENTE("cliDomicilio")
        txtCliTelefono.Text = drCLIENTE("cliTelefono")
        txtCliLocalidad.Text = drCLIENTE("cliLocalidad")
        txtCliHorario.Text = drCLIENTE("cliHorario")
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        frmPrincipal.txtnCliente.Text = txtCliNro.Text
        frmPrincipal.txtRazonsocial.Text = txtCliNombre.Text
        frmPrincipal.txtDomicilio.Text = txtCliDomicilio.Text
        frmPrincipal.txtCiudad.Text = txtCliLocalidad.Text
        frmPrincipal.txtTelefono.Text = txtCliTelefono.Text
        frmPrincipal.txtCUIT.Text = txtCliCuit.Text
        frmPrincipal.txtHorario.Text = txtCliHorario.Text
        Me.Hide()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        If txtCliNombre.Text <> "" And txtCliCuit.Text <> "" And txtCliDomicilio.Text <> "" And txtCliTelefono.Text <> "" And txtCliLocalidad.Text <> "" Then
            Rta = MsgBox("�Desea Eliminar Permanentemente el Cliente seleccionado?", MsgBoxStyle.YesNo, "TECSERV")
            If Rta = 7 Then Exit Sub

            'If MsgBox("Esta seguro de que desea eliminar el cLIENTEgrupo " & txtCliNombre.Text & " ?", MsgBoxStyle.YesNo, "TECSERV") = MsgBoxResult.Yes Then
            Baja_Clientes()
            'Generar_Auditoria("BAJA")
            'oACLIENTE.AGREGAR_ACLIENTE(drACLIENTE)
            oCLIENTE = New CONTROLADORA.CLIENTES
            dtCLIENTE = oCLIENTE.OBTENER_CLIENTES
            'LlenarListView()
            cargar_lista()
            Limpiar()
            MsgBox("Se ejecuto la sentencia indicada", MsgBoxStyle.Information, "TECSERV")
            'End If
        Else
            MsgBox("Seleccione un Cliente por favor", MsgBoxStyle.Information, "TECSERV")
        End If

    End Sub

    Private Sub txtCliCuit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCliCuit.KeyPress
        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

End Class