﻿Public Class grupoProspecto
    Private _nombre As String
    Private _descripcion As String

    Public Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(ByVal Value As String)
            _nombre = Value
        End Set
    End Property

    Public Property Descripcion As String
        Get
            Return _descripcion
        End Get
        Set(ByVal Value As String)
            _descripcion = Value
        End Set
    End Property


    Public ReadOnly Property SaveMemento() As memento
        Get
            Return New memento(_nombre, _descripcion)
        End Get
    End Property

    Public Sub RestoreMemento(ByVal memento As memento)
        Me._nombre = memento.Nombre
        Me._descripcion = memento.Descripcion
    End Sub


End Class
