﻿Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class frmProductos
    Private drPRODUCTO As DataRow
    Private oPRODUCTO As CONTROLADORA.PRODUCTOS
    Private dtPRODUCTO As DataTable

    Private MyPRODUCTO As ENTIDADES.Producto
    Private drAPRODUCTO As DataRow
    Private dtAPRODUCTO As DataTable

    Dim BAN, x, Rta As Integer
    Dim NC As String
    Dim Autorizacion, Grupo, Formulario, Operacion As String

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        BAN = 1
        GroupBox3.Enabled = True
        txtProdNro.Enabled = False
        txtStkMin.Enabled = True
        txtDesProd.Enabled = True
        Limpiar()
        btnGuardar.Enabled = True
    End Sub

    Private Sub btnGuardar_Click_1(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        If txtDesProd.Text <> "" And txtStkMin.Text <> "" Then
            If BAN = 1 Then
                Alta_PRODUCTOS()
                MsgBox("El nuevo Producto ha sido dado de Alta con éxito", MsgBoxStyle.Information, "TECSERV")

            ElseIf BAN = 2 Then
                If txtProdNro.Text <> "" Then
                    Modificacion_PRODUCTOS()
                    MsgBox("El Producto ha sido Modificado con éxito", MsgBoxStyle.Information, "TECSERV")
                End If
            End If

            oPRODUCTO = New CONTROLADORA.PRODUCTOS
            dtPRODUCTO = oPRODUCTO.OBTENER_PRODUCTOS
            cargar_lista()
            Limpiar()
            GroupBox3.Enabled = False
            txtProdNro.Enabled = False
            txtStkMin.Enabled = False
            txtDesProd.Enabled = False
            btnGuardar.Enabled = False
        Else
            MsgBox("Faltan Campos a completar!!", MsgBoxStyle.Exclamation, "TECSERV")
        End If
    End Sub

    Private Sub frmProductos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As Integer
        Formulario = "2"
        For x = 1 To 3
            Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, x)
            If x = 1 And Autorizacion = "NO" Then
                btnAgregar.Visible = False
            ElseIf x = 2 And Autorizacion = "NO" Then
                btnEliminar.Visible = False
            ElseIf x = 3 And Autorizacion = "NO" Then
                btnModificar.Visible = False
            End If
        Next
        GroupBox3.Enabled = False
        txtProdNro.Enabled = False
        txtStkMin.Enabled = False
        txtDesProd.Enabled = False
        If frmPrincipal.AgregarToolStripMenuItem.Text = "Cancelar" Then
            btnAceptar.Enabled = True
        Else
            btnAceptar.Enabled = False
        End If
        Limpiar()
        oPRODUCTO = New CONTROLADORA.PRODUCTOS
        dtPRODUCTO = oPRODUCTO.OBTENER_PRODUCTOS
        cargar_lista()
    End Sub

    Private Sub Alta_PRODUCTOS()
        oPRODUCTO = New CONTROLADORA.PRODUCTOS
        MyPRODUCTO = New ENTIDADES.PRODUCTO
        MyPRODUCTO.DesProducto = UCase(Trim(txtDesProd.Text))
        MyPRODUCTO.StkMinimo = txtStkMin.Text

        oPRODUCTO.GUARDAR_PRODUCTO(MyPRODUCTO)

    End Sub

    Private Sub Modificacion_PRODUCTOS()
        oPRODUCTO = New CONTROLADORA.PRODUCTOS
        MyPRODUCTO = New ENTIDADES.PRODUCTO
        MyPRODUCTO.IdProducto = txtProdNro.Text
        MyPRODUCTO.DesProducto = UCase(Trim(txtDesProd.Text))
        MyPRODUCTO.StkMinimo = txtStkMin.Text

        oPRODUCTO.MODIFICA_PRODUCTO(MyPRODUCTO)

    End Sub

    Private Sub Baja_PRODUCTOS()
        oPRODUCTO = New CONTROLADORA.PRODUCTOS
        MyPRODUCTO = New ENTIDADES.PRODUCTO
        MyPRODUCTO.IdProducto = txtProdNro.Text
        MyPRODUCTO.DesProducto = UCase(Trim(txtDesProd.Text))
        MyPRODUCTO.StkMinimo = txtStkMin.Text

        oPRODUCTO.ELIMINA_PRODUCTO(MyPRODUCTO)

    End Sub

    Private Sub Limpiar()
        txtProdNro.Text = ""
        txtDesProd.Text = ""
        txtStkMin.Text = ""
    End Sub

    Private Sub cargar_lista()
        Dim lvPRODUCTOS As ListViewItem
        Dim Conexion As New SqlConnection()
        lvwProductos.Items.Clear()
        Dim strSQL As String
        strSQL = "SELECT * FROM T_Productos"

        Conexion = ModuloConexion.Conectar

        Dim command As New SqlCommand(strSQL, Conexion)
        Dim reader As SqlDataReader = command.ExecuteReader()

        While reader.Read()
            lvPRODUCTOS = New ListViewItem(reader.GetInt32(0))
            lvPRODUCTOS.SubItems.Add(reader.GetString(1))
            lvPRODUCTOS.SubItems.Add(reader.GetInt32(2))
            lvwProductos.Items.Add(lvPRODUCTOS)
        End While

        reader.Close()

    End Sub

    'Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    frmPrincipal.txtnIdProducto.Text = txtProdNro.Text
    '    frmPrincipal.txtDesProducto.Text = txtDesProd.Text
    '    frmPrincipal.txtStkMinimo.Text = txtStkMin.Text
    '    Me.Hide()
    'End Sub

    Private Sub btnEliminar_Click_1(sender As System.Object, e As System.EventArgs) Handles btnEliminar.Click
        If txtDesProd.Text <> "" And txtStkMin.Text <> "" Then
            Rta = MsgBox("¿Desea Eliminar Permanentemente el PRODUCTO seleccionado?", MsgBoxStyle.YesNo, "TECSERV")
            If Rta = 7 Then Exit Sub

            Baja_PRODUCTOS()
            oPRODUCTO = New CONTROLADORA.PRODUCTOS
            dtPRODUCTO = oPRODUCTO.OBTENER_PRODUCTOS
            cargar_lista()
            Limpiar()
            MsgBox("Se ejecuto la sentencia indicada", MsgBoxStyle.Information, "TECSERV")
        Else
            MsgBox("Seleccione un PRODUCTO por favor", MsgBoxStyle.Information, "TECSERV")
        End If
    End Sub

    Private Sub btnModificar_Click_1(sender As System.Object, e As System.EventArgs) Handles btnModificar.Click
        BAN = 2
        GroupBox3.Enabled = True
        txtProdNro.Enabled = False
        txtStkMin.Enabled = True
        txtDesProd.Enabled = True
        btnGuardar.Enabled = True
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click

    End Sub


    Private Sub btnCancelar_Click_1(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub frmProductos_Load_1(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim x As Integer
        Formulario = "2"
        For x = 1 To 3
            Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, x)
            If x = 1 And Autorizacion = "NO" Then
                btnAgregar.Visible = False
            ElseIf x = 2 And Autorizacion = "NO" Then
                btnEliminar.Visible = False
            ElseIf x = 3 And Autorizacion = "NO" Then
                btnModificar.Visible = False
            End If
        Next
        GroupBox3.Enabled = False
        txtProdNro.Enabled = False
        txtStkMin.Enabled = False
        txtDesProd.Enabled = False
        If frmPrincipal.AgregarToolStripMenuItem.Text = "Cancelar" Then
            btnAceptar.Enabled = True
        Else
            btnAceptar.Enabled = False
        End If
        Limpiar()
        oPRODUCTO = New CONTROLADORA.PRODUCTOS
        dtPRODUCTO = oPRODUCTO.OBTENER_PRODUCTOS
        cargar_lista()
    End Sub

    Private Sub lvwProductos_ItemActivate1(sender As Object, e As System.EventArgs) Handles lvwProductos.ItemActivate
        GroupBox3.Enabled = False
        txtProdNro.Enabled = False
        txtStkMin.Enabled = False
        txtDesProd.Enabled = False
        btnGuardar.Enabled = False
        drPRODUCTO = oPRODUCTO.OBTENER_PRODUCTO(lvwProductos.FocusedItem.Text)
        txtProdNro.Text = drPRODUCTO("IdProducto")
        txtDesProd.Text = drPRODUCTO("DesProducto")
        txtStkMin.Text = drPRODUCTO("StkMinimo")
    End Sub
End Class