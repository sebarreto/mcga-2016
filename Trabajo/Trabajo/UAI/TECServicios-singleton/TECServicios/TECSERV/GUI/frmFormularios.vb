Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class frmFormularios
    Private drFORMULARIO As DataRow
    Private oFORMULARIO As CONTROLADORA.FORMULARIO
    Private dtFORMULARIO As DataTable
    Private dvFORMULARIO As DataView

    'Private oAFORMULARIO As CONTROLADORA.AFORMULARIO
    Private drAFORMULARIO As DataRow
    Private dtAFORMULARIO As DataTable
    Private MyFORMULARIO As ENTIDADES.Formulario

    Dim OPT, Rta As Integer
    Dim Operacion, Autorizacion, Form As String

Private Sub tbFormularios_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbFormularios.ButtonClick
        Select Case tbFormularios.Buttons.IndexOf(e.Button)
            Case 0 'ALTA
                OPT = 1
                Cancelar()
                tbSave.Enabled = True
                txtNombre.Enabled = True
                txtDescripcion.Enabled = True

            Case 1 'MODIFICAR
                OPT = 2
                tbSave.Enabled = True
                txtNombre.Enabled = True
                txtDescripcion.Enabled = True

            Case 2 'BAJA
                Rta = MsgBox("�Desea Eliminar Permanentemente el Formulario seleccionado?", MsgBoxStyle.YesNo, "TECSERV")
                If Rta = 7 Then Exit Sub

                If txtNombre.Text <> "" Then
                    If MsgBox("Esta seguro de que desea eliminar el grupo " & txtNombre.Text & " ?", MsgBoxStyle.YesNo, "TECSERV") = MsgBoxResult.Yes Then
                        'oFORMULARIO.ELIMINAR_FORMULARIO(txtNro.Text)
                        Guardar_Datos_eli()
                        MsgBox("El Grupo ha sido Eliminado con Exito", MsgBoxStyle.Exclamation, "TECSERV")
                        oFORMULARIO = New CONTROLADORA.FORMULARIO
                        'oAFORMULARIO = New CONTROLADORA.AFORMULARIO
                        'dtFORMULARIO = oAFORMULARIO.OBTENER_FORMULARIOS
                        dtFORMULARIO = oFORMULARIO.OBTENER_FORMULARIOS
                        'Generar_Auditoria("BAJA")
                        Cancelar()
                        'LlenarListView()
                        cargar_lista()
                    End If
                Else
                    MsgBox("Seleccione un formulario por favor")
                End If

            Case 3 'GUARDAR
                If txtNombre.Text <> "" Then
                    If OPT = 1 Then
                        drFORMULARIO = oFORMULARIO.CREAR_FORMULARIO()
                        Guardar_Datos_alt()
                        'FORMULARIO.AGREGAR_FORMULARIO(drFORMULARIO)
                        'Generar_Auditoria("ALTA")
                        MsgBox("Formulario creado con Exito", MsgBoxStyle.Information, "TECSERV")
                    Else
                        Guardar_Datos_mod()
                        'oFORMULARIO.MODIFICAR_FORMULARIO(drFORMULARIO)
                        dtFORMULARIO = oFORMULARIO.OBTENER_FORMULARIOS
                        'Generar_Auditoria("MODIFICACION")
                        MsgBox("Formulario Modificado con Exito", MsgBoxStyle.Information, "TECSERV")
                    End If
                    oFORMULARIO = New CONTROLADORA.FORMULARIO
                    'oAFORMULARIO = New CONTROLADORA.AFORMULARIO
                    dtFORMULARIO = oFORMULARIO.OBTENER_FORMULARIOS
                    'dtFORMULARIO = oAFORMULARIO.OBTENER_FORMULARIOS
                    Cancelar()
                    'LlenarListView()
                    cargar_lista()
                    txtNombre.Enabled = False
                    txtDescripcion.Enabled = False
                Else
                    MsgBox("Por Favor asigne un nombre al nuevo grupo", MsgBoxStyle.Exclamation, "TECSERV")
                End If

            Case 4 'CANCELAR
                Cancelar()

            Case 5 'VOLVER
                Me.Close()
        End Select
    End Sub

    'Private Sub Generar_Auditoria(ByVal TipoEvento)
    '    'SE GENERA LA AUDITORIA
    '    drAFORMULARIO = oAFORMULARIO.CREAR_FORMULARIO()

    '    drAFORMULARIO("Id_AudForm") = txtNro.Text
    '    drAFORMULARIO("frmNombre") = Trim(txtNombre.Text)
    '    drAFORMULARIO("audFecha") = Microsoft.VisualBasic.DateString
    '    drAFORMULARIO("audHora") = Microsoft.VisualBasic.Left(Microsoft.VisualBasic.TimeString, 5)
    '    drAFORMULARIO("audTipo") = TipoEvento
    '    drAFORMULARIO("audUsuario") = UsuarioLogeado

    '    oAFORMULARIO.AGREGAR_FORMULARIO(drAFORMULARIO)
    'End Sub

    Private Sub Cancelar()
        txtNro.Text = ""
        txtNro.Enabled = False
        txtNombre.Text = ""
        txtNombre.Enabled = False
        txtDescripcion.Text = ""
        txtDescripcion.Enabled = False
        tbSave.Enabled = False
    End Sub

    Private Sub Guardar_Datos_alt()
        ' drFORMULARIO("frmNombre") = Trim(txtNombre.Text)
        ' drFORMULARIO("frmDescripcion") = Trim(txtDescripcion.Text)
        MyFORMULARIO = New ENTIDADES.Formulario
        MyFORMULARIO.forNombre = Trim(txtNombre.Text)
        MyFORMULARIO.forDescripcion = Trim(txtDescripcion.Text)
        oFORMULARIO.GUARDAR_FORMULARIO(MyFORMULARIO)
    End Sub

    Private Sub Guardar_Datos_mod()
        ' drFORMULARIO("frmNombre") = Trim(txtNombre.Text)
        ' drFORMULARIO("frmDescripcion") = Trim(txtDescripcion.Text)
        MyFORMULARIO = New ENTIDADES.Formulario
        MyFORMULARIO.forIdformulario = txtNro.Text
        MyFORMULARIO.forNombre = Trim(txtNombre.Text)
        MyFORMULARIO.forDescripcion = Trim(txtDescripcion.Text)
        oFORMULARIO.MODIFICA_FORMULARIO(MyFORMULARIO)
    End Sub

    Private Sub Guardar_Datos_eli()
        MyFORMULARIO = New ENTIDADES.Formulario
        MyFORMULARIO.forIdformulario = txtNro.Text
        oFORMULARIO.ELIMINA_FORMULARIO(MyFORMULARIO)
    End Sub

    Private Sub frmFormularios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim x As Integer
        Form = "7"
        For x = 1 To 3
            Autorizacion = Verificar_Perfil(GrupoUsuario, Form, x)
            If x = 1 And Autorizacion = "NO" Then
                tbFormularios.Buttons(0).Visible = False
            ElseIf x = 2 And Autorizacion = "NO" Then
                tbFormularios.Buttons(1).Visible = False
            ElseIf x = 3 And Autorizacion = "NO" Then
                tbFormularios.Buttons(2).Visible = False
            End If
        Next
        OPT = 0
        oFORMULARIO = New CONTROLADORA.FORMULARIO
        dtFORMULARIO = oFORMULARIO.OBTENER_FORMULARIOS
        Cancelar()
        'LlenarListView()
        cargar_lista()
        txtNombre.Enabled = False
        txtDescripcion.Enabled = False
        'oAFORMULARIO = New CONTROLADORA.AFORMULARIO
        'dtFORMULARIO = oAFORMULARIO.OBTENER_FORMULARIOS
    End Sub

Private Sub LlenarListView()
  Dim itmGrupo As ListViewItem

  lvwFormularios.Items.Clear()
  For Each rwGrupos As DataRow In dtFORMULARIO.Rows
    itmGrupo = New ListViewItem(rwGrupos(0).ToString())
    itmGrupo.SubItems.Add(rwGrupos(1).ToString)
    lvwFormularios.Items.Add(itmGrupo)
  Next
End Sub

    Private Sub cargar_lista()
        Dim lvFORMULARIO As ListViewItem
        Dim Conexion As New SqlConnection()
        lvwFormularios.Items.Clear()
        Dim strSQL As String
        strSQL = "SELECT * FROM T_Formularios"

        Conexion = ModuloConexion.Conectar

        Dim command As New SqlCommand(strSQL, Conexion)
        Dim reader As SqlDataReader = command.ExecuteReader()

        While reader.Read()
            lvFORMULARIO = New ListViewItem(reader.GetInt32(0))
            lvFORMULARIO.SubItems.Add(reader.GetString(1))
            lvwFormularios.Items.Add(lvFORMULARIO)
        End While

        reader.Close()

    End Sub

Private Sub lvwFormularios_ItemActivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwFormularios.ItemActivate
  drFORMULARIO = oFORMULARIO.OBTENER_FORMULARIO(lvwFormularios.FocusedItem.Text)
  txtNro.Text = drFORMULARIO("Id_Formulario")
  txtNombre.Text = drFORMULARIO("frmNombre")
  txtDescripcion.Text = drFORMULARIO("frmDescripcion")
  tbSave.Enabled = False
End Sub

End Class