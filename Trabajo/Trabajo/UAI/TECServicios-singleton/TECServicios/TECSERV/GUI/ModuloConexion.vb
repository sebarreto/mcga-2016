Imports System.Data.sql
Imports System.Data
Imports System.Data.SqlClient

Module ModuloConexion
    Private oConnection As SqlConnection
    Private oConnectionAud As SqlConnection

    Public Function Conectar() As SqlConnection
        Dim rConString As String
        rConString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;" & _
            "Initial Catalog = dataserv;" & _
            "Integrated Security=SSPI" & ";"
        oConnection = New SqlConnection()
        If oConnection.State = ConnectionState.Closed Then
            oConnection.ConnectionString = rConString
            oConnection.Open()
            Return oConnection
        End If
    End Function


    Public Function ConectarAud() As SqlConnection
        Dim rConString As String
        rConString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;" & _
            "Initial Catalog = Auditoria;" & _
            "Integrated Security=SSPI" & ";"
        oConnectionAud = New SqlConnection()
        If oConnectionAud.State = ConnectionState.Closed Then
            oConnectionAud.ConnectionString = rConString
            oConnectionAud.Open()
            Return oConnectionAud
        End If
    End Function

End Module