Imports System.IO
Imports System.Data.sql
Imports System.Security.Cryptography
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient

Module SEGURIDAD
    Friend VALSEGURIDAD As String
    Friend UsuarioLogeado, Id_OperadorLog As String
    Friend GrupoUsuario As String
    Private con As sqlConnection
    Private cmd As sqlCommand
    Private Reader As sqlDataReader
    Friend md5 As New MD5CryptoServiceProvider

    Public Function getMd5Hash(ByVal input As String) As String
        Dim md5Hasher As New MD5CryptoServiceProvider
        Dim data As Byte() = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input))
        Dim sBuilder As New StringBuilder()
        Dim i As Integer
        For i = 0 To data.Length - 1
            sBuilder.Append(data(i).ToString("x2"))
        Next i
        Return sBuilder.ToString()
    End Function

    Public Function Verificar_Perfil(ByVal Grupo, ByVal Form, ByVal Operacion)
        Dim Rta As String
        con = New sqlConnection
        cmd = New sqlCommand
        con.ConnectionString = "Data Source=SEBASTIAN-PC\SQLEXPRESS;" & _
            "Initial Catalog = dataserv;" & _
            "Integrated Security=SSPI" & ";"
        cmd.Connection = con
        cmd.CommandText = "SELECT * FROM T_Perfiles WHERE Id_Grupo = " & Grupo & " AND Id_Formulario = " & Form & " AND Id_Permiso = " & Operacion & " "
        con.Open()
        Reader = cmd.ExecuteReader
        Rta = "NO"
        While (Reader.Read())
            Rta = "SI"
        End While
        Return Rta
    End Function
End Module
