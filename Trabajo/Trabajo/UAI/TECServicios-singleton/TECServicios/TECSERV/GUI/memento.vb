﻿Public Class memento
    Private _nombre As String
    Private _descripcion As String

    Public Sub New(ByVal nombre As String, ByVal descripcion As String)
        Me._nombre = nombre
        Me._descripcion = descripcion
    End Sub

    Public Property Nombre As String
        Get
            Return _nombre
        End Get
        Set(ByVal Value As String)
            _nombre = Value
        End Set
    End Property

    Public Property Descripcion As String
        Get
            Return _descripcion
        End Get
        Set(ByVal Value As String)
            _descripcion = Value
        End Set
    End Property

End Class
