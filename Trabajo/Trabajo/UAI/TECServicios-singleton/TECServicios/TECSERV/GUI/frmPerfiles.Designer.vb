<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPerfiles
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
Me.GroupBox2 = New System.Windows.Forms.GroupBox
Me.GroupBox4 = New System.Windows.Forms.GroupBox
Me.lstForms = New System.Windows.Forms.ListBox
Me.cmbGrupos = New System.Windows.Forms.ComboBox
Me.Label2 = New System.Windows.Forms.Label
Me.gbPermisos = New System.Windows.Forms.GroupBox
Me.CLBPermisos = New System.Windows.Forms.CheckedListBox
Me.GroupBox1 = New System.Windows.Forms.GroupBox
Me.DataGridView1 = New System.Windows.Forms.DataGridView
Me.lvwPerfiles = New System.Windows.Forms.ListView
Me.Grupo = New System.Windows.Forms.ColumnHeader
Me.Form = New System.Windows.Forms.ColumnHeader
Me.Permiso = New System.Windows.Forms.ColumnHeader
Me.tbPerfiles = New System.Windows.Forms.ToolBar
Me.tbAdd = New System.Windows.Forms.ToolBarButton
Me.tbEdit = New System.Windows.Forms.ToolBarButton
Me.tbDelete = New System.Windows.Forms.ToolBarButton
Me.tbSave = New System.Windows.Forms.ToolBarButton
Me.tbCancelar = New System.Windows.Forms.ToolBarButton
Me.tbExit = New System.Windows.Forms.ToolBarButton
Me.GroupBox2.SuspendLayout()
Me.GroupBox4.SuspendLayout()
Me.gbPermisos.SuspendLayout()
Me.GroupBox1.SuspendLayout()
CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
Me.SuspendLayout()
'
'GroupBox2
'
Me.GroupBox2.Controls.Add(Me.GroupBox4)
Me.GroupBox2.Controls.Add(Me.cmbGrupos)
Me.GroupBox2.Controls.Add(Me.Label2)
Me.GroupBox2.Controls.Add(Me.gbPermisos)
Me.GroupBox2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.GroupBox2.Location = New System.Drawing.Point(25, 48)
Me.GroupBox2.Name = "GroupBox2"
Me.GroupBox2.Size = New System.Drawing.Size(250, 308)
Me.GroupBox2.TabIndex = 16
Me.GroupBox2.TabStop = False
Me.GroupBox2.Text = "Asociacion de Perfil"
'
'GroupBox4
'
Me.GroupBox4.Controls.Add(Me.lstForms)
Me.GroupBox4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.GroupBox4.ForeColor = System.Drawing.Color.Blue
Me.GroupBox4.Location = New System.Drawing.Point(15, 51)
Me.GroupBox4.Name = "GroupBox4"
Me.GroupBox4.Size = New System.Drawing.Size(223, 113)
Me.GroupBox4.TabIndex = 20
Me.GroupBox4.TabStop = False
Me.GroupBox4.Text = "Formularios"
'
'lstForms
'
Me.lstForms.FormattingEnabled = True
Me.lstForms.ItemHeight = 15
Me.lstForms.Location = New System.Drawing.Point(18, 26)
Me.lstForms.Name = "lstForms"
Me.lstForms.Size = New System.Drawing.Size(191, 79)
Me.lstForms.TabIndex = 0
'
'cmbGrupos
'
Me.cmbGrupos.FormattingEnabled = True
Me.cmbGrupos.Location = New System.Drawing.Point(59, 22)
Me.cmbGrupos.Name = "cmbGrupos"
Me.cmbGrupos.Size = New System.Drawing.Size(179, 23)
Me.cmbGrupos.TabIndex = 19
'
'Label2
'
Me.Label2.AutoSize = True
Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.Label2.ForeColor = System.Drawing.Color.Black
Me.Label2.Location = New System.Drawing.Point(12, 25)
Me.Label2.Name = "Label2"
Me.Label2.Size = New System.Drawing.Size(41, 15)
Me.Label2.TabIndex = 18
Me.Label2.Text = "Grupo"
'
'gbPermisos
'
Me.gbPermisos.Controls.Add(Me.CLBPermisos)
Me.gbPermisos.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.gbPermisos.Location = New System.Drawing.Point(15, 170)
Me.gbPermisos.Name = "gbPermisos"
Me.gbPermisos.Size = New System.Drawing.Size(223, 132)
Me.gbPermisos.TabIndex = 17
Me.gbPermisos.TabStop = False
Me.gbPermisos.Text = "Permisos"
'
'CLBPermisos
'
Me.CLBPermisos.FormattingEnabled = True
Me.CLBPermisos.Location = New System.Drawing.Point(18, 25)
Me.CLBPermisos.Name = "CLBPermisos"
Me.CLBPermisos.Size = New System.Drawing.Size(187, 100)
Me.CLBPermisos.TabIndex = 0
'
'GroupBox1
'
Me.GroupBox1.Controls.Add(Me.DataGridView1)
Me.GroupBox1.Controls.Add(Me.lvwPerfiles)
Me.GroupBox1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.GroupBox1.Location = New System.Drawing.Point(281, 48)
Me.GroupBox1.Name = "GroupBox1"
Me.GroupBox1.Size = New System.Drawing.Size(386, 308)
Me.GroupBox1.TabIndex = 15
Me.GroupBox1.TabStop = False
Me.GroupBox1.Text = "Perfiles Existentes"
'
'DataGridView1
'
Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
Me.DataGridView1.Location = New System.Drawing.Point(6, 25)
Me.DataGridView1.Name = "DataGridView1"
Me.DataGridView1.Size = New System.Drawing.Size(374, 277)
Me.DataGridView1.TabIndex = 18
'
'lvwPerfiles
'
Me.lvwPerfiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Grupo, Me.Form, Me.Permiso})
Me.lvwPerfiles.FullRowSelect = True
Me.lvwPerfiles.GridLines = True
Me.lvwPerfiles.Location = New System.Drawing.Point(6, 25)
Me.lvwPerfiles.MultiSelect = False
Me.lvwPerfiles.Name = "lvwPerfiles"
Me.lvwPerfiles.Size = New System.Drawing.Size(312, 277)
Me.lvwPerfiles.TabIndex = 4
Me.lvwPerfiles.UseCompatibleStateImageBehavior = False
Me.lvwPerfiles.View = System.Windows.Forms.View.Details
'
'Grupo
'
Me.Grupo.Text = "Grupo"
Me.Grupo.Width = 105
'
'Form
'
Me.Form.Text = "Formulario"
Me.Form.Width = 138
'
'Permiso
'
Me.Permiso.Text = "Permiso"
Me.Permiso.Width = 75
'
'tbPerfiles
'
Me.tbPerfiles.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.tbAdd, Me.tbEdit, Me.tbDelete, Me.tbSave, Me.tbCancelar, Me.tbExit})
Me.tbPerfiles.ButtonSize = New System.Drawing.Size(56, 56)
Me.tbPerfiles.DropDownArrows = True
Me.tbPerfiles.Location = New System.Drawing.Point(0, 0)
Me.tbPerfiles.Name = "tbPerfiles"
Me.tbPerfiles.ShowToolTips = True
Me.tbPerfiles.Size = New System.Drawing.Size(679, 42)
Me.tbPerfiles.TabIndex = 14
'
'tbAdd
'
Me.tbAdd.ImageIndex = 1
Me.tbAdd.Name = "tbAdd"
Me.tbAdd.Text = "Agregar"
'
'tbEdit
'
Me.tbEdit.ImageIndex = 2
Me.tbEdit.Name = "tbEdit"
Me.tbEdit.Text = "Modificar"
'
'tbDelete
'
Me.tbDelete.ImageIndex = 6
Me.tbDelete.Name = "tbDelete"
Me.tbDelete.Text = "Eliminar"
'
'tbSave
'
Me.tbSave.Enabled = False
Me.tbSave.ImageIndex = 0
Me.tbSave.Name = "tbSave"
Me.tbSave.Text = "Guardar"
'
'tbCancelar
'
Me.tbCancelar.ImageIndex = 5
Me.tbCancelar.Name = "tbCancelar"
Me.tbCancelar.Text = "Cancelar"
'
'tbExit
'
Me.tbExit.ImageIndex = 3
Me.tbExit.Name = "tbExit"
Me.tbExit.Text = "Volver"
'
'frmPerfiles
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.ClientSize = New System.Drawing.Size(679, 367)
Me.Controls.Add(Me.GroupBox2)
Me.Controls.Add(Me.GroupBox1)
Me.Controls.Add(Me.tbPerfiles)
Me.Name = "frmPerfiles"
Me.Text = "Perfiles"
Me.GroupBox2.ResumeLayout(False)
Me.GroupBox2.PerformLayout()
Me.GroupBox4.ResumeLayout(False)
Me.gbPermisos.ResumeLayout(False)
Me.GroupBox1.ResumeLayout(False)
CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
Me.ResumeLayout(False)
Me.PerformLayout()

End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tbPerfiles As System.Windows.Forms.ToolBar
    Friend WithEvents tbAdd As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbEdit As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbDelete As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbSave As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbCancelar As System.Windows.Forms.ToolBarButton
    Friend WithEvents tbExit As System.Windows.Forms.ToolBarButton
    Friend WithEvents gbPermisos As System.Windows.Forms.GroupBox
    Friend WithEvents cmbGrupos As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lvwPerfiles As System.Windows.Forms.ListView
    Friend WithEvents Grupo As System.Windows.Forms.ColumnHeader
    Friend WithEvents Form As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents lstForms As System.Windows.Forms.ListBox
    Friend WithEvents CLBPermisos As System.Windows.Forms.CheckedListBox
    Friend WithEvents Permiso As System.Windows.Forms.ColumnHeader
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
End Class
