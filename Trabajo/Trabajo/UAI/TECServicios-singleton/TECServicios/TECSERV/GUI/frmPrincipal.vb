Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Office.Interop
Imports System.Text.RegularExpressions
Imports System.Text

Public Class frmPrincipal
    Private drTICKETS As DataRow
    Private dtTICKETS As DataTable
    Private dvTICKETS As DataView
    Private oTICKETS As CONTROLADORA.TICKET

    Private drTECNICO As DataRow
    Private oTECNICO As CONTROLADORA.TECNICO
    Private dtTECNICO As DataTable

    Private drCLIENTE As DataRow
    Private oCLIENTE As CONTROLADORA.CLIENTES
    Private dtCLIENTE As DataTable

    'Private oATICKET As CONTROLADORA.ATICKETS
    Private drATICKETS As DataRow
    Private dtATICKETS As DataTable
    Private oOPERADOR As CONTROLADORA.OPERADOR
    'seba ini
    Private drOPERADOR As DataRow
    Private dtOPERADOR As DataTable
    Private validaticket As CONTROLADORA.VALIDATICKET
    'seba fin

    Private MyTICKET As ENTIDADES.Ticket

    Dim Autorizacion, Grupo, Formulario, Operacion As String
    Dim BAN, Rta As Integer

    Private Sub frmPrincipal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim x As Integer
        Formulario = "1"
        For x = 1 To 3
            Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, x)
            If x = 1 And Autorizacion = "NO" Then
                AgregarToolStripMenuItem.Visible = False
            ElseIf x = 2 And Autorizacion = "NO" Then
                EliminarToolStripMenuItem.Visible = False
            ElseIf x = 3 And Autorizacion = "NO" Then
                ModificarToolStripMenuItem.Visible = False
            End If
        Next

        Deshabilitar_Textos()

        GuardarToolStripMenuItem.Enabled = False
        oTICKETS = New CONTROLADORA.TICKET
        dtTICKETS = oTICKETS.OBTENER_TICKETS

        oTECNICO = New CONTROLADORA.TECNICO
        oCLIENTE = New CONTROLADORA.CLIENTES

        dtTECNICO = oTECNICO.OBTENER_TECNICOS
        dtCLIENTE = oCLIENTE.OBTENER_CLIENTES

        'oATICKET = New CONTROLADORA.ATICKETS
        'dtATICKETS = oATICKET.OBTENER_TICKETS
        oOPERADOR = New CONTROLADORA.OPERADOR

        'LlenarListView()
        cargar_lista()
        LlenarComboTecnicos()
        txtAtendio.Text = oOPERADOR.BuscarApellido_Operador(Id_OperadorLog)

        'Mapa invisible
        WebBrowser1.Visible = False

    End Sub

    'Private Sub LlenarListView()
    '    Dim a As String
    '    Dim itmTicket As ListViewItem
    '    lvwServicios.Items.Clear()
    '    For Each rwClientes As DataRow In dtTICKETS.Rows
    '        If rwClientes(5).ToString = "0" Then
    '            itmTicket = New ListViewItem(rwClientes(0).ToString())
    '            itmTicket.SubItems.Add(oCLIENTE.BuscarID_Cliente(rwClientes(1).ToString))
    '            itmTicket.SubItems.Add(Microsoft.VisualBasic.Left(rwClientes(8).ToString, 10))
    '            itmTicket.SubItems.Add(rwClientes(4).ToString)
    '            lvwServicios.Items.Add(itmTicket)
    '        End If
    '    Next
    'End Sub

    Private Sub cargar_lista()
        Dim itmTicket As ListViewItem
        Dim Conexion As New SqlConnection()
        lvwServicios.Items.Clear()
        Dim strSQL As String
        strSQL = "SELECT * FROM T_TICKETS"

        Conexion = ModuloConexion.Conectar

        Dim command As New SqlCommand(strSQL, Conexion)
        Dim reader As SqlDataReader = command.ExecuteReader()

        While reader.Read()
            '     If reader.ToString(5) = "0" Then
            itmTicket = New ListViewItem(reader.GetInt32(0))
            itmTicket.SubItems.Add(oCLIENTE.BuscarID_Cliente(reader(1).ToString))
            itmTicket.SubItems.Add(Microsoft.VisualBasic.Left(reader(8).ToString, 10))
            itmTicket.SubItems.Add(reader(4).ToString)
            If (reader(6).ToString.Contains("S")) Then
                lvwServicios.Items.Add(itmTicket).BackColor = Color.Red
            Else
                lvwServicios.Items.Add(itmTicket).BackColor = Color.White
            End If
            '   End If
        End While

        reader.Close()

    End Sub

    Private Sub cargar_lista_like()
        Dim itmTicket As ListViewItem
        Dim Conexion As New SqlConnection()
        lvwServicios.Items.Clear()
        Dim strSQL As String
        strSQL = "SELECT * FROM T_TICKETS WHERE Falla like '%" + txtBuscotick.Text + "%'"

        Conexion = ModuloConexion.Conectar

        Dim command As New SqlCommand(strSQL, Conexion)
        Dim reader As SqlDataReader = command.ExecuteReader()

        While reader.Read()
            '     If reader.ToString(5) = "0" Then
            itmTicket = New ListViewItem(reader.GetInt32(0))
            itmTicket.SubItems.Add(oCLIENTE.BuscarID_Cliente(reader(1).ToString))
            itmTicket.SubItems.Add(Microsoft.VisualBasic.Left(reader(8).ToString, 10))
            itmTicket.SubItems.Add(reader(4).ToString)
            lvwServicios.Items.Add(itmTicket)
            '   End If
        End While

        reader.Close()

    End Sub

    Private Sub ClientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientesToolStripMenuItem.Click
        frmClientes.Show()
    End Sub

    Private Sub TecnicosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TecnicosToolStripMenuItem.Click
        frmTecnicos.Show()
    End Sub

    Private Sub ListadosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadosToolStripMenuItem.Click
        frmListados.Show()
    End Sub

    Private Sub OperadoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OperadoresToolStripMenuItem.Click
        frmOperadores.Show()
    End Sub

    Private Sub ProductosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProductosToolStripMenuItem.Click
        frmProductos.Show()
    End Sub

    Private Sub AgregarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgregarToolStripMenuItem.Click
        If AgregarToolStripMenuItem.Text <> "Cancelar" Then
            BAN = 1
            Operacion = "1"
            Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, Operacion)
            If Autorizacion = "NO" Then
                MsgBox("Usted no tiene Permisos para Ingresar un Nuevo Ticket")
                Exit Sub
            End If
            GuardarToolStripMenuItem.Enabled = True
            Limpiar_Textos()
            Habilitar_Textos()
            AgregarToolStripMenuItem.Text = "Cancelar"
            optCobranzaNo.Checked = True
            txtMonto.Text = 0
            txtnConforme.Text = 0
            frmClientes.Show()
        Else
            Limpiar_Textos()
            Deshabilitar_Textos()
            AgregarToolStripMenuItem.Text = "Agregar"
            GuardarToolStripMenuItem.Enabled = False
        End If
    End Sub

    Private Sub LlenarComboTecnicos()
        For Each rwTecnicos As DataRow In dtTECNICO.Rows
            cmbTecnico.Items.Add(rwTecnicos(0).ToString & "-" & rwTecnicos(2).ToString & "  " & rwTecnicos(1).ToString)
        Next
    End Sub

    Private Sub SalirToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        'BCKIC = "FIN"
        End
    End Sub

    Private Sub Limpiar_Textos()
        Dim fechaesp As String
        txtnTicket.Text = ""
        txtFecha.Text = Microsoft.VisualBasic.Left(DateTime.Now, 10)
        'txtFecha.Text = String.Format(Microsoft.VisualBasic.DateString, "dd/mm/yyyy")
        txtHora.Text = Microsoft.VisualBasic.TimeString
        txtnCliente.Text = ""
        txtRazonsocial.Text = ""
        txtDomicilio.Text = ""
        txtCiudad.Text = ""
        txtTelefono.Text = ""
        txtFalla.Text = ""
        cmbTecnico.Text = ""
        txtnConforme.Text = ""
        txtMonto.Text = ""
        txtHorario.Text = ""
        txtCUIT.Text = ""
        txtMail.Text = ""
        txtMail.Visible = False
        botonMail.Visible = False
    End Sub

    Private Sub Habilitar_Textos()
        GuardarToolStripMenuItem.Enabled = True
        ModificarToolStripMenuItem.Enabled = True
        GroupBox2.Enabled = True
    End Sub

    Private Sub Deshabilitar_Textos()
        GroupBox2.Enabled = False
        GuardarToolStripMenuItem.Enabled = False
        ModificarToolStripMenuItem.Enabled = False
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GuardarToolStripMenuItem.Click
        If txtRazonsocial.Text <> "" And txtDomicilio.Text <> "" And txtCiudad.Text <> "" And txtFalla.Text <> "" And txtCUIT.Text <> "" And cmbTecnico.Text <> "" Then
            If BAN = 1 Then

                drTICKETS = oTICKETS.CREAR_TICKET
                'Guardar_Datos()
                'oTICKETS.AGREGAR_TICKET(drTICKETS)
                Guardar_Datos_alt()
                'Generar_Auditoria("ALTA")
                oTICKETS = New CONTROLADORA.TICKET
                dtTICKETS = oTICKETS.OBTENER_TICKETS

                'LlenarListView()
                cargar_lista()
                Imprimir_Reporte()
                Limpiar_Textos()
                Deshabilitar_Textos()
                GuardarToolStripMenuItem.Enabled = False


            Else
                'Guardar_Datos()
                'oTICKETS.MODIFICAR_TICKET(drTICKETS)
                Guardar_Datos_mod()
                ' Generar_Auditoria("MODIFICACION")
                oTICKETS = New CONTROLADORA.TICKET
                dtTICKETS = oTICKETS.OBTENER_TICKETS
                'LlenarListView()
                cargar_lista()
                GroupBox2.Enabled = False
            End If
            If txtMail.Text <> "" Then
                EnviarMail()
            End If
            AgregarToolStripMenuItem.Text = "Agregar"
            GuardarToolStripMenuItem.Enabled = False
            Limpiar_Textos()
            BAN = 0
        Else
            MsgBox("Faltan Campos a Completar!!", MsgBoxStyle.Exclamation, "TECSERV")
        End If
    End Sub

    Private Sub strategy()

    End Sub

    'Private Sub Guardar_Datos()
    '    drTICKETS("Id_Cliente") = UCase(Trim(txtnCliente.Text))
    '    drTICKETS("Id_Operador") = oOPERADOR.BuscarID_Operador(Trim(txtAtendio.Text))
    '    drTICKETS("Id_tecnico") = cmbTecnico.SelectedIndex + 1
    '    'drTICKETS("NomCliente") = UCase(Trim(txtRazonsocial.Text))
    '    drTICKETS("Falla") = UCase(Trim(txtFalla.Text))
    '    drTICKETS("Nconforme") = Int(Trim(txtnConforme.Text))
    '    If optCobranzaSi.Checked = True Then
    '        drTICKETS("Cobranza") = "S"
    '    Else
    '        drTICKETS("Cobranza") = "N"
    '    End If
    '    drTICKETS("Monto") = Int(Trim(txtMonto.Text))
    '    drTICKETS("Fecha") = txtFecha.Text
    '    drTICKETS("Hora") = Microsoft.VisualBasic.Left(Microsoft.VisualBasic.TimeString, 5)
    'End Sub

    Private Sub Guardar_Datos_alt()
        MyTICKET = New ENTIDADES.Ticket
        MyTICKET.ticCliente = UCase(Trim(txtnCliente.Text))
        MyTICKET.ticOperador = oOPERADOR.BuscarID_Operador(Trim(txtAtendio.Text))
        MyTICKET.ticTecnico = cmbTecnico.SelectedIndex + 1
        MyTICKET.ticFalla = UCase(Trim(txtFalla.Text))
        MyTICKET.ticNoconforme = Int(Trim(txtnConforme.Text))
        If optCobranzaSi.Checked = True Then
            MyTICKET.ticCobranza = "S"
        Else
            MyTICKET.ticCobranza = "N"
        End If
        MyTICKET.ticMonto = Int(Trim(txtMonto.Text))
        MyTICKET.ticFechati = txtFecha.Text
        MyTICKET.ticHorati = Microsoft.VisualBasic.Left(Microsoft.VisualBasic.TimeString, 5)
        oTICKETS.GUARDAR_TICKET(MyTICKET)

        Dim elegible As Boolean = True
        validaticket = New CONTROLADORA.VALIDATICKET
        Dim TestArray() As String = Split(cmbTecnico.Text, "-")
        elegible = validaticket.EsValido(MyTICKET)

        If elegible = False Then
            MsgBox("CUIDADO NO COMPLE TODAS LAS CONDICIONES - FACADE!!", MsgBoxStyle.Exclamation, "TECSERV")
        End If

    End Sub

    Private Sub Guardar_Datos_mod()
        MyTICKET = New ENTIDADES.Ticket
        MyTICKET.ticIdticket = txtnTicket.Text
        MyTICKET.ticCliente = UCase(Trim(txtnCliente.Text))
        MyTICKET.ticOperador = oOPERADOR.BuscarID_Operador(Trim(txtAtendio.Text))
        MyTICKET.ticTecnico = cmbTecnico.SelectedIndex + 1
        MyTICKET.ticFalla = UCase(Trim(txtFalla.Text))
        MyTICKET.ticNoconforme = Int(Trim(txtnConforme.Text))
        If optCobranzaSi.Checked = True Then
            MyTICKET.ticCobranza = "S"
        Else
            MyTICKET.ticCobranza = "N"
        End If
        MyTICKET.ticMonto = Int(Trim(txtMonto.Text))
        MyTICKET.ticFechati = txtFecha.Text
        MyTICKET.ticHorati = Microsoft.VisualBasic.Left(Microsoft.VisualBasic.TimeString, 5)
        oTICKETS.MODIFICA_TICKET(MyTICKET)
    End Sub

    Private Sub Guardar_Datos_baj()
        MyTICKET = New ENTIDADES.Ticket
        MyTICKET.ticIdticket = txtnTicket.Text
        MyTICKET.ticCliente = UCase(Trim(txtnCliente.Text))
        MyTICKET.ticOperador = oOPERADOR.BuscarID_Operador(Trim(txtAtendio.Text))
        MyTICKET.ticTecnico = cmbTecnico.SelectedIndex + 1
        MyTICKET.ticFalla = UCase(Trim(txtFalla.Text))
        MyTICKET.ticNoconforme = Int(Trim(txtnConforme.Text))
        If optCobranzaSi.Checked = True Then
            MyTICKET.ticCobranza = "S"
        Else
            MyTICKET.ticCobranza = "N"
        End If
        MyTICKET.ticMonto = Int(Trim(txtMonto.Text))
        MyTICKET.ticFechati = txtFecha.Text
        MyTICKET.ticHorati = Microsoft.VisualBasic.Left(Microsoft.VisualBasic.TimeString, 5)
        oTICKETS.ELIMINA_TICKET(MyTICKET)
    End Sub

    Private Sub lvwServicios_ItemActivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwServicios.ItemActivate
        drTICKETS = oTICKETS.OBTENER_TICKET(lvwServicios.FocusedItem.Text)
        Cargar_Datos_Ticket()
        txtAtendio.Text = oTICKETS.BuscarID_OPERADOR(txtAtendio.Text)
        'txtAtendio.Text = txtAtendio.Text & "- " & Op

    End Sub

    Private Sub EliminarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.Click
        Dim NTicket As Integer
        Operacion = "2"
        Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, Operacion)
        If Autorizacion = "NO" Then
            MsgBox("Usted no tiene Permisos para Eliminar un Ticket")
            Exit Sub
        End If
        Rta = MsgBox("�Desea Eliminar Permanentemente el Ticket seleccionado?", MsgBoxStyle.YesNo, "TECSERV")
        If Rta = 7 Then Exit Sub

        If txtRazonsocial.Text <> "" And txtFalla.Text <> "" Then
            GuardarToolStripMenuItem.Enabled = False
            NTicket = txtnTicket.Text
            'oTICKETS.ELIMINAR_TICKET(txtnTicket.Text)
            Guardar_Datos_baj()
            MsgBox("EL TICKET N� " & NTicket & "HA SIDO ELIMINADO CON EXITO")
            'Generar_Auditoria("BAJA")
            oTICKETS = New CONTROLADORA.TICKET
            dtTICKETS = oTICKETS.OBTENER_TICKETS
            'LlenarListView()
            cargar_lista()
            Limpiar_Textos()
        Else
            MsgBox("POR FAVOR SELECCIONE UN TICKET")
        End If
    End Sub

    Private Sub Imprimir_Reporte()
        Dim objExcel As Excel.Application

        objExcel = New Excel.Application
        objExcel.Visible = True
        objExcel.Workbooks.Open("C:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\ticket.xls")

        Dim prueba As Integer = 20

        While prueba > 0
            Try
                objExcel.Cells(1, 4).Formula = "TICKET"
                objExcel.Worksheets(1).Range("A1:F1").Merge()
                objExcel.Range("A1").Select()
                objExcel.Selection.Font.Bold = True
                objExcel.Cells(3, 1).Formula = "N� TICKET:"
                objExcel.Range("A3").Select()
                objExcel.Selection.Font.Bold = True
                objExcel.Cells(3, 2).Formula = dtTICKETS.Rows.Count + 7
                objExcel.Cells(3, 3).Formula = "FECHA:"
                objExcel.Range("C3").Select()
                objExcel.Selection.Font.Bold = True
                objExcel.Cells(3, 4).Formula = txtFecha.Text
                objExcel.Cells(3, 5).Formula = "HORA:"
                objExcel.Range("E3").Select()
                objExcel.Selection.Font.Bold = True
                objExcel.Cells(3, 6).Formula = txtHora.Text
                objExcel.Cells(4, 1).Formula = "CLIENTE:"
                objExcel.Range("A4").Select()
                objExcel.Selection.Font.Bold = True
                objExcel.Cells(4, 2).Formula = txtRazonsocial.Text
                objExcel.Cells(4, 3).Formula = "DOMICILIO:"
                objExcel.Range("C4").Select()
                objExcel.Selection.Font.Bold = True
                objExcel.Cells(4, 4).Formula = txtDomicilio.Text
                objExcel.Worksheets(1).Range("D4:F4").Merge()
                objExcel.Cells(5, 1).Formula = "TELEFONO:"
                objExcel.Range("A5").Select()
                objExcel.Selection.Font.Bold = True
                objExcel.Cells(5, 2).Formula = txtTelefono.Text
                objExcel.Cells(5, 3).Formula = "CIUDAD:"
                objExcel.Range("C5").Select()
                objExcel.Selection.Font.Bold = True
                objExcel.Cells(5, 4).Formula = txtCiudad.Text
                objExcel.Cells(7, 1).Formula = "FALLA:"
                objExcel.Range("A7").Select()
                objExcel.Selection.Font.Bold = True
                objExcel.Worksheets(1).Range("B7:F7").Merge()
                objExcel.Cells(7, 2).Formula = txtFalla.Text
                objExcel.Cells(8, 1).Formula = "TECNICO:"
                objExcel.Range("A8").Select()
                objExcel.Selection.Font.Bold = True
                objExcel.Cells(8, 2).Formula = cmbTecnico.Text

                prueba = 0

            Catch ex As Exception
                prueba -= 1
                If prueba = 0 Then
                    Throw
                Else
                    System.Threading.Thread.Sleep(1000)
                End If
            End Try
        End While


    End Sub

    Private Sub GruposToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GruposToolStripMenuItem1.Click
        frmGrupos.Show()
    End Sub

    Private Sub PerfilesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PerfilesToolStripMenuItem.Click
        frmPerfiles.Show()
    End Sub

    Private Sub ModificarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarToolStripMenuItem.Click
        Operacion = "3"
        Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, Operacion)
        If Autorizacion = "NO" Then
            MsgBox("Usted no tiene Permisos para Modificar un Ticket")
            Exit Sub
        End If
        If txtRazonsocial.Text <> "" Then
            BAN = 2
            GuardarToolStripMenuItem.Enabled = True
            Habilitar_Textos()
            txtRazonsocial.Enabled = False
            txtDomicilio.Enabled = False
            txtCiudad.Enabled = False
            txtHorario.Enabled = False
            txtTelefono.Enabled = False
            txtCUIT.Enabled = False
            txtFecha.Enabled = False
            txtHora.Enabled = False
        Else
            MsgBox("POR FAVOR SELECCIONE UN TICKET")
        End If
    End Sub

    Private Sub BuscarTicketToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BuscarTicketToolStripMenuItem.Click
        Dim NT As Integer
        Try
            Limpiar_Textos()
            NT = InputBox("INGRESE EL NUMERO DE TICKET A BUSCAR")
            drTICKETS = oTICKETS.OBTENER_TICKET(NT)
            Cargar_Datos_Ticket()
            If txtFalla.Text <> "" Then
                If MsgBox("�DESEA IMPRIMIR UN COMPROBANTE?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    Imprimir_Reporte()
                End If
            Else
                MsgBox("NO SE HA ENCONTRADO EL TICKET", MsgBoxStyle.Exclamation, "TECSERV")
            End If
        Catch oEX As Exception
            MsgBox("TICKET INEXISTENTE", MsgBoxStyle.Critical, "TECSERVICIOS")
        Finally
        End Try
    End Sub

    Private Sub AyudaToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AyudaToolStripMenuItem1.Click
        Dim objWord As Word.Application
        objWord = New Word.Application
        objWord.Visible = True
        objWord.Documents.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\ayuda.doc")
    End Sub

    'Private Sub Generar_Auditoria(ByVal TipoEvento)
    '    'SE GENERA LA AUDITORIA
    '    drATICKETS = oATICKET.CREAR_ATICKET()
    '    drATICKETS("Id_Ticket") = txtnTicket.Text
    '    drATICKETS("Id_Cliente") = UCase(Trim(txtnCliente.Text))
    '    drATICKETS("Id_Operador") = Int(Id_OperadorLog)
    '    drATICKETS("Id_tecnico") = cmbTecnico.SelectedIndex + 1
    '    'drATICKETS("NomCliente") = UCase(Trim(txtRazonsocial.Text))
    '    drATICKETS("Falla") = UCase(Trim(txtFalla.Text))
    '    drATICKETS("Nconforme") = Int(txtnConforme.Text)
    '    If optCobranzaSi.Checked = True Then
    '        drATICKETS("Cobranza") = "S"
    '    Else
    '        drATICKETS("Cobranza") = "N"
    '    End If
    '    drATICKETS("Monto") = Int(txtMonto.Text)
    '    drATICKETS("Fecha") = txtFecha.Text
    '    drATICKETS("Hora") = txtHora.Text
    '    drATICKETS("audTipo") = TipoEvento
    '    drATICKETS("audFecha") = Microsoft.VisualBasic.DateString
    '    drATICKETS("audHora") = Microsoft.VisualBasic.Left(Microsoft.VisualBasic.TimeString, 5)
    '    drATICKETS("audUsuario") = UsuarioLogeado

    '    oATICKET.AGREGAR_ATICKET(drATICKETS)

    'End Sub

    Private Sub Cargar_Datos_Ticket()
        Try
            txtnTicket.Text = drTICKETS("Id_Ticket")
            txtnCliente.Text = drTICKETS("Id_Cliente")
            'txtRazonsocial.Text = drTICKETS("NomCliente")
            txtRazonsocial.Text = oCLIENTE.BuscarID_Cliente(drTICKETS("Id_Cliente"))

            txtAtendio.Text = drTICKETS("Id_Operador")
            cmbTecnico.SelectedIndex = Val(drTICKETS("Id_Tecnico") - 1)
            cmbTecnico.Text = cmbTecnico.SelectedItem
            txtFalla.Text = drTICKETS("Falla")
            txtnConforme.Text = drTICKETS("Nconforme")

            If drTICKETS("Cobranza").ToString.Contains("S") Then
                optCobranzaSi.Checked = True
            Else
                optCobranzaNo.Checked = True
            End If

            txtMonto.Text = drTICKETS("Monto")
            txtFecha.Text = drTICKETS("Fecha")
            txtHora.Text = drTICKETS("Hora")
            drCLIENTE = oCLIENTE.OBTENER_CLIENTE(txtnCliente.Text)
            txtCUIT.Text = drCLIENTE("cliCodigo")
            txtHorario.Text = drCLIENTE("cliHorario")
            txtTelefono.Text = drCLIENTE("cliTelefono")
            txtCiudad.Text = drCLIENTE("cliLocalidad")
            txtDomicilio.Text = drCLIENTE("cliDomicilio")
            ModificarToolStripMenuItem.Enabled = True
            GroupBox2.Enabled = False
        Catch
        Finally
        End Try

        Try

            'MAPA 1
            'Dim diremap As String
            'diremap = "Mendoza 3474, Rosario, Santa Fe, Argentina"
            'Dim queryaddress As New StringBuilder
            'queryaddress.Append("http://maps.google.es/maps?q=")
            'queryaddress.Append(diremap)
            'WebBrowser1.Navigate(queryaddress.ToString)

            'MAPA 2
            WebBrowser1.Visible = True
            Dim urlMaps As String
            Dim calleMaps As String = txtDomicilio.Text
            urlMaps = "http://maps.google.cl/maps?q=" & calleMaps & ", " & txtCiudad.Text & ", ARGENTINA"
            Dim direccion As New Uri(urlMaps)
            WebBrowser1.Url = direccion

        Catch ex As Exception

            MsgBox(AccessibleDescription, MsgBoxStyle.AbortRetryIgnore, "Sin Conexion - No Mapa")

        End Try

    End Sub

    Private Sub FormulariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FormulariosToolStripMenuItem.Click
        frmFormularios.Show()
    End Sub

    Private Sub PermisosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PermisosToolStripMenuItem.Click
        frmPermisos.Show()
    End Sub

    Private Sub lvwServicios_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvwServicios.SelectedIndexChanged

    End Sub

    Private Sub txtnTicket_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtnTicket.TextChanged

    End Sub

    Private Sub MenuStrip1_ItemClicked(sender As System.Object, e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles MenuStrip1.ItemClicked

    End Sub

    Private Sub TextBox1_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtBuscotick.TextChanged
        cargar_lista_like()
    End Sub

    Private Sub CheckMail_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckMail.CheckedChanged
        If CheckMail.Checked Then
            txtMail.Visible = True
            txtMail.Enabled = True
            botonMail.Visible = True
            botonMail.BackColor = Color.White
        Else
            txtMail.Visible = False
            txtMail.Enabled = False
            txtMail.Text = ""
            botonMail.Visible = False
        End If
    End Sub

    Private Sub txtMail_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtMail.TextChanged
        If IsValidEmail(LCase(txtMail.Text)) = False Then
            botonMail.BackColor = Color.Red
        Else
            botonMail.BackColor = Color.Green
        End If
    End Sub

    Public Function IsValidEmail(ByVal email As String) As Boolean
        Return Regex.IsMatch(email, "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]{2,4}$")
    End Function

    Public Function EnviarMail()
        Dim _Message As New System.Net.Mail.MailMessage()
        Dim _SMTP As New System.Net.Mail.SmtpClient

        'CONFIGURACI�N DEL STMP
        _SMTP.Credentials = New System.Net.NetworkCredential("seb.barreto@gmail.com", "ACA LA PASS")
        _SMTP.Host = "smtp.gmail.com"
        _SMTP.Port = 587
        _SMTP.EnableSsl = True

        ' CONFIGURACION DEL MENSAJE
        _Message.[To].Add(Me.txtMail.Text.ToString) 'Cuenta de Correo al que se le quiere enviar el e-mail
        _Message.From = New System.Net.Mail.MailAddress("seb.barreto@gmail.com", "TECSERV", System.Text.Encoding.UTF8) 'Quien lo env�a
        _Message.Subject = "Alta/Modificacion de Ticket" 'Sujeto del e-mail
        _Message.SubjectEncoding = System.Text.Encoding.UTF8 'Codificacion
        _Message.Body = "Contacte con TECSERV para mas informacion" 'contenido del mail
        _Message.BodyEncoding = System.Text.Encoding.UTF8
        _Message.Priority = System.Net.Mail.MailPriority.Normal
        _Message.IsBodyHtml = False

        'ENVIO
        Try
            _SMTP.Send(_Message)
        Catch ex As System.Net.Mail.SmtpException
            MessageBox.Show(ex.ToString, "Error - El mail no fue enviado!!!", MessageBoxButtons.OK)
        End Try
    End Function


    Private Sub txtMonto_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtMonto.KeyPress
        If Not IsNumeric(txtMonto.Text) Then
            MsgBox("CAMPO MONTO SOLO ACEPTA NUMEROS", MsgBoxStyle.Information, "TECSERVICIOS")
        Else
            If txtMonto.Text > 99999 Then
                MsgBox("CAMPO MONTO DEMASIADO GRANDE", MsgBoxStyle.Information, "TECSERVICIOS")
            End If
        End If
    End Sub


    Private Sub txtnConforme_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtnConforme.KeyPress
        If Not IsNumeric(txtnConforme.Text) Then
            MsgBox("CAMPO CONFORME SOLO ACEPTA NUMEROS", MsgBoxStyle.Information, "TECSERVICIOS")
        Else
            If txtnConforme.Text > 99999 Then
                MsgBox("CAMPO CONFORME DEMASIADO GRANDE", MsgBoxStyle.Information, "TECSERVICIOS")
            End If
        End If
    End Sub
End Class
