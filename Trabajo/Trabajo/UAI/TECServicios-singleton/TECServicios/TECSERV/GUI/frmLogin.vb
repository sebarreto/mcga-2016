Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES


Public Class frmLogin
    Private myLOGIN As ENTIDADES.aIngresos
    Private drLOGIN As DataRow
    Private oLOGIN As CONTROLADORA.OPERADOR
    Private ioLOGIN As CONTROLADORA.ILOGIN
    Public nombre, contrase�a, cat As String
    Dim PerfilUsr As String
    Dim contador As Integer

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If txtNombre.Text <> "" And txtContrase�a.Text <> "" Then
            If txtNombre.Text.Contains("*") Or
               txtNombre.Text.Contains("/") Then
                MsgBox("CAMPO USUARIO CON SIMBOLOS NO PERMITIDOS", MsgBoxStyle.Exclamation, "TECSERVICIOS")
            Else
                If txtContrase�a.Text.Contains("*") Or
                   txtContrase�a.Text.Contains("/") Then
                    MsgBox("CAMPO USUARIO CON SIMBOLOS NO PERMITIDOS", MsgBoxStyle.Exclamation, "TECSERVICIOS")
                Else
                    oLOGIN = New CONTROLADORA.OPERADOR
                    VERIFICAR()
                End If
            End If
        Else
            MsgBox("FALTAN CAMPOS A COMPLETAR", MsgBoxStyle.Exclamation, "TECSERVICIOS")
        End If
    End Sub

    Private Sub VERIFICAR()
        Dim Nombre, Clave, ClaveHASH As String

        ClaveHASH = (getMd5Hash(txtContrase�a.Text))

        Try
            drLOGIN = oLOGIN.OBTENER_OPERADOR_LOGIN(UCase(Trim(txtNombre.Text)))
            Nombre = drLOGIN("logNomUsuario")
            Clave = drLOGIN("logClave")

            If UCase(Trim(txtNombre.Text)) = Nombre Then
                If ClaveHASH = Clave Then
                    '   PerfilUsr = drLOGIN("logGrupo")
                    GrupoUsuario = drLOGIN("Id_Grupo")
                    Id_OperadorLog = drLOGIN("Id_Operador")
                    UsuarioLogeado = drLOGIN("logNomUsuario")
                    Guardar_Datos_alt()
                    frmPrincipal.Show()
                    Me.Hide()
                Else
                    contador = contador + 1
                    If contador <= 3 Then
                        MsgBox("Contrase�a Incorrecta", MsgBoxStyle.Critical, "TECSERVICIOS")
                        txtContrase�a.Text = ""
                        txtContrase�a.Focus()
                    Else
                        MsgBox("Contrase�a Incorrecta", MsgBoxStyle.Critical, "SE SUPERARON LOS 3 INTENTOS")
                        Me.Close()
                    End If
                End If
            End If

        Catch oEX As Exception
            MsgBox("USUARIO INEXISTENTE", MsgBoxStyle.Critical, "TECSERVICIOS")
            txtNombre.Text = ""
            txtContrase�a.Text = ""
        Finally
        End Try
    End Sub

    'Public Function getMd5Hash(ByVal input As String) As String
    'Dim md5Hasher As New MD5CryptoServiceProvider
    'Dim data As Byte() = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input))
    'Dim sBuilder As New StringBuilder()
    'Dim i As Integer
    'For i = 0 To data.Length - 1
    'sBuilder.Append(Data(i).ToString("x2"))
    'Next i
    'Return sBuilder.ToString()
    'End Function

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        End
    End Sub

    Private Sub frmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        contador = 0
        ioLOGIN = New CONTROLADORA.ILOGIN
    End Sub

    Private Sub Guardar_Datos_alt()

        'myLOGIN = New ENTIDADES.aIngresos
        'myLOGIN.log_ingrUsuario = UCase(Trim(txtNombre.Text))
        'myLOGIN.log_ingrFecha = Date.Now
        'myLOGIN.log_ingrTipo = "E"
        'ioLOGIN.GUARDAR_ILOGIN(myLOGIN)

        Dim Conexion As New SqlConnection()
        Conexion = ModuloConexion.ConectarAud
        'Dim strSQL As String
        Dim oIComando As SqlCommand
        oIComando = New SqlCommand("INSERT INTO T_AuditoriaIngresos(log_ingrUsuario,log_ingrFecha,log_ingrTipo) VALUES (@gringrUsario,@gringrFecha,@gringrTipo)", Conexion)
        oIComando.Parameters.AddWithValue("@gringrUsario", UCase(Trim(txtNombre.Text)))
        oIComando.Parameters.AddWithValue("@gringrFecha", Date.Now)
        oIComando.Parameters.AddWithValue("@gringrTipo", "E")
        oIComando.ExecuteNonQuery()
        'strSQL = "INSERT INTO T_AuditoriaIngresos(log_ingrUsuario,log_ingrFecha,log_ingrTipo) VALUES (txtNombre.Text,Date.Now,'E')"
        'Dim command As New SqlCommand(strSQL, Conexion)
        'Dim reader As SqlDataReader = command.ExecuteReader()
        'reader.Close()

    End Sub

End Class