Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Imports ENTIDADES

Public Class frmTecnicos
    Private drTECNICO As DataRow
    Private oTECNICO As CONTROLADORA.TECNICO  ' OBJETO CLIENTE
    Private dtTECNICO As DataTable
    Private dvTECNICO As DataView

    'Private oATECNICO As CONTROLADORA.ATECNICO
    Private drATECNICO As DataRow
    Private dtATECNICO As DataTable

    Private MyTECNICO As ENTIDADES.Tecnico

    Dim Autorizacion, Grupo, Formulario, Operacion As String
    Dim BAN, x, Rta As Integer

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If txtTecApellido.Text <> "" And txtTecNombre.Text <> "" And txtTecDomicilio.Text <> "" And txtTecLocalidad.Text <> "" Then
            If BAN = 1 Then
                drTECNICO = oTECNICO.CREAR_TECNICO
                'Guardar_Datos()
                Guardar_Datos_alt()
                oTECNICO.AGREGAR_CLIENTE(drTECNICO)
                'Generar_Auditoria("ALTA")
                Limpiar()
                oTECNICO = New CONTROLADORA.TECNICO
                dtTECNICO = oTECNICO.OBTENER_TECNICOS
                'LlenarListView()
                cargar_lista()
                MsgBox("El nuevo T�cnico ha sido dado de Alta con �xito", MsgBoxStyle.Information, "TECSERV")
            Else
                'Guardar_Datos()
                Guardar_Datos_mod()
                oTECNICO.MODIFICAR_TECNICO(drTECNICO)
                'Generar_Auditoria("MODIFICACION")
                Limpiar()
                oTECNICO = New CONTROLADORA.TECNICO
                dtTECNICO = oTECNICO.OBTENER_TECNICOS
                'LlenarListView()
                cargar_lista()
                MsgBox("El T�cnico ha sido Modificado con �xito", MsgBoxStyle.Information, "TECSERV")
            End If
            GroupBox2.Enabled = False
            btnGuardar.Enabled = False
        Else
            MsgBox("Faltan Campos a Completar!!", MsgBoxStyle.Exclamation, "TECSERV")
        End If
    End Sub

    'Private Sub Guardar_Datos()
    '    drTECNICO("tecNombre") = UCase(Trim(txtTecNombre.Text))
    '    drTECNICO("tecApellido") = UCase(Trim(txtTecApellido.Text))
    '    drTECNICO("tecDomicilio") = UCase(Trim(txtTecDomicilio.Text))
    '    drTECNICO("tecLocalidad") = UCase(Trim(txtTecLocalidad.Text))
    '    drTECNICO("tecTelefono") = UCase(Trim(txtTecTelefono.Text))
    'End Sub

    Private Sub Guardar_Datos_alt()
        MyTECNICO = New ENTIDADES.Tecnico
        MyTECNICO.tecNombre = UCase(Trim(txtTecNombre.Text))
        MyTECNICO.tecApellido = UCase(Trim(txtTecApellido.Text))
        MyTECNICO.tecDomicilio = UCase(Trim(txtTecDomicilio.Text))
        MyTECNICO.tecLocalidad = UCase(Trim(txtTecLocalidad.Text))
        MyTECNICO.tecTelefono = UCase(Trim(txtTecTelefono.Text))
        oTECNICO.GUARDAR_TECNICO(MyTECNICO)
    End Sub

    Private Sub Guardar_Datos_mod()
        MyTECNICO = New ENTIDADES.Tecnico
        MyTECNICO.tecIdtecnico = txtTecCodigo.Text
        MyTECNICO.tecNombre = UCase(Trim(txtTecNombre.Text))
        MyTECNICO.tecApellido = UCase(Trim(txtTecApellido.Text))
        MyTECNICO.tecDomicilio = UCase(Trim(txtTecDomicilio.Text))
        MyTECNICO.tecLocalidad = UCase(Trim(txtTecLocalidad.Text))
        MyTECNICO.tecTelefono = UCase(Trim(txtTecTelefono.Text))
        oTECNICO.MODIFICA_TECNICO(MyTECNICO)
    End Sub

    Private Sub Guardar_Datos_baj()
        MyTECNICO = New ENTIDADES.Tecnico
        MyTECNICO.tecIdtecnico = txtTecCodigo.Text
        MyTECNICO.tecNombre = UCase(Trim(txtTecNombre.Text))
        MyTECNICO.tecApellido = UCase(Trim(txtTecApellido.Text))
        MyTECNICO.tecDomicilio = UCase(Trim(txtTecDomicilio.Text))
        MyTECNICO.tecLocalidad = UCase(Trim(txtTecLocalidad.Text))
        MyTECNICO.tecTelefono = UCase(Trim(txtTecTelefono.Text))
        oTECNICO.ELIMINA_TECNICO(MyTECNICO)
    End Sub

    'Private Sub Generar_Auditoria(ByVal TipoEvento)
    '    'SE GENERA LA AUDITORIA
    '    drATECNICO = oATECNICO.CREAR_TECNICO()
    '    drATECNICO("Id_Tecnico") = UCase(Trim(txtTecCodigo.Text))
    '    drATECNICO("tecNombre") = UCase(Trim(txtTecNombre.Text))
    '    drATECNICO("tecApellido") = UCase(Trim(txtTecApellido.Text))
    '    drATECNICO("tecDomicilio") = UCase(Trim(txtTecDomicilio.Text))
    '    drATECNICO("tecLocalidad") = UCase(Trim(txtTecLocalidad.Text))
    '    drATECNICO("tecTelefono") = UCase(Trim(txtTecTelefono.Text))
    '    drATECNICO("audTipo") = TipoEvento
    '    drATECNICO("audFecha") = Microsoft.VisualBasic.DateString
    '    drATECNICO("audHora") = Microsoft.VisualBasic.Left(Microsoft.VisualBasic.TimeString, 5)
    '    drATECNICO("audUsuario") = UsuarioLogeado

    '    oATECNICO.AGREGAR_TECNICO(drATECNICO)
    'End Sub

    'Private Sub LlenarListView()
    '    Dim lvTECNICOS As ListViewItem

    '    lvwTecnicos.Items.Clear()
    '    For Each rwclientes As DataRow In dtTECNICO.Rows
    '        lvTECNICOS = New ListViewItem(rwclientes(0).ToString())
    '        lvTECNICOS.SubItems.Add(rwclientes(1).ToString)
    '        lvTECNICOS.SubItems.Add(rwclientes(2).ToString)
    '        lvwTecnicos.Items.Add(lvTECNICOS)
    '    Next
    'End Sub

    Private Sub cargar_lista()
        Dim lvTECNICOS As ListViewItem
        Dim Conexion As New SqlConnection()
        lvwTecnicos.Items.Clear()
        Dim strSQL As String
        strSQL = "SELECT * FROM T_Tecnicos"

        Conexion = ModuloConexion.Conectar

        Dim command As New SqlCommand(strSQL, Conexion)
        Dim reader As SqlDataReader = command.ExecuteReader()

        While reader.Read()
            lvTECNICOS = New ListViewItem(reader.GetInt32(0))
            lvTECNICOS.SubItems.Add(reader.GetString(1))
            lvTECNICOS.SubItems.Add(reader.GetString(2))
            lvwTecnicos.Items.Add(lvTECNICOS)
        End While

        reader.Close()

    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Operacion = "1"
        Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, Operacion)
        If Autorizacion = "NO" Then
            MsgBox("Usted no tiene Permisos para Ingresar un Nuevo Tecnico")
            Exit Sub
        End If
        GroupBox2.Enabled = True
        Limpiar()
        BAN = 1
        btnGuardar.Enabled = True
    End Sub

    Private Sub frmTecnicos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim x As Integer
        Formulario = "3"
        For x = 1 To 3
            Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, x)
            If x = 1 And Autorizacion = "NO" Then
                btnAgregar.Visible = False
            ElseIf x = 2 And Autorizacion = "NO" Then
                btnEliminar.Visible = False
            ElseIf x = 3 And Autorizacion = "NO" Then
                btnModificar.Visible = False
            End If
        Next
        GroupBox2.Enabled = False
        oTECNICO = New CONTROLADORA.TECNICO
        dtTECNICO = oTECNICO.OBTENER_TECNICOS
        'LlenarListView()
        cargar_lista()
        'oATECNICO = New CONTROLADORA.ATECNICO
        'dtATECNICO = oATECNICO.OBTENER_TECNICOS
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        If txtTecCodigo.Text <> "" And txtTecApellido.Text <> "" And txtTecNombre.Text <> "" Then
            Operacion = "2"
            Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, Operacion)
            If Autorizacion = "NO" Then
                MsgBox("Usted no tiene Permisos para Eliminar un Tecnico")
                Exit Sub
            End If
            Rta = MsgBox("�Desea Eliminar Permanentemente el Tecnico seleccionado?", MsgBoxStyle.YesNo, "TECSERV")
            If Rta = 7 Then Exit Sub

            If MsgBox("Est� seguro que desea Eliminar el T�cnico " & txtTecApellido.Text, MsgBoxStyle.YesNo, "TECSERV") = MsgBoxResult.Yes Then
                GroupBox2.Enabled = False
                'oTECNICO.ELIMINAR_TECNICO(txtTecCodigo.Text)
                Guardar_Datos_baj()
                'Generar_Auditoria("BAJA")
                MsgBox("El T�cnico ha sido Eliminado con �xito", MsgBoxStyle.Information, "TECSERV")
                Limpiar()
                dtTECNICO = oTECNICO.OBTENER_TECNICOS
                'LlenarListView()
                cargar_lista()
            End If
        Else
            MsgBox("Seleccione un Tecnico por favor", MsgBoxStyle.Exclamation, "TECSERV")
        End If
    End Sub

    Private Sub lvwTecnicos_ItemActivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwTecnicos.ItemActivate
        GroupBox2.Enabled = False
        drTECNICO = oTECNICO.OBTENER_CLIENTE(lvwTecnicos.FocusedItem.Text)
        Cargar_Datos_Tecnico()
        txtTecApellido.Enabled = True
        txtTecNombre.Enabled = True
        btnGuardar.Enabled = False
    End Sub

    Private Sub Cargar_Datos_Tecnico()
        txtTecCodigo.Text = drTECNICO("Id_Tecnico")
        txtTecApellido.Text = drTECNICO("tecApellido")
        txtTecNombre.Text = drTECNICO("tecNombre")
        txtTecDomicilio.Text = drTECNICO("tecDomicilio")
        txtTecLocalidad.Text = drTECNICO("tecLocalidad")
        txtTecTelefono.Text = drTECNICO("tecTelefono")
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Operacion = "3"
        Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, Operacion)
        If Autorizacion = "NO" Then
            MsgBox("Usted no tiene Permisos para Modificar un Tecnico")
            Exit Sub
        End If
        BAN = 2
        GroupBox2.Enabled = True
        btnGuardar.Enabled = True
    End Sub

    Private Sub Limpiar()
        txtTecCodigo.Text = ""
        txtTecApellido.Text = ""
        txtTecNombre.Text = ""
        txtTecDomicilio.Text = ""
        txtTecTelefono.Text = ""
        txtTecLocalidad.Text = ""
        txtTecNombre.Enabled = True
        txtTecApellido.Enabled = True
    End Sub

'Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
'Dim NT As String
'Try
'  NT = InputBox("Ingrese el Apellido del Tecnico a Buscar")
'  drTECNICO = oTECNICO.BUSCAR_TECNICO(UCase(Trim(NT)))
'  Cargar_Datos_Tecnico()
'    Catch oEX As Exception
'    MsgBox("Tecnico Inexistente", MsgBoxStyle.Critical, "TECSERV")
'  Finally
'End Try
'End Sub
End Class