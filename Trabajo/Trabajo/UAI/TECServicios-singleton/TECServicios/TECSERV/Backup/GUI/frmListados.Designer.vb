<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListados
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
Me.GroupBox3 = New System.Windows.Forms.GroupBox
Me.lbListados = New System.Windows.Forms.ListBox
Me.Button1 = New System.Windows.Forms.Button
Me.GroupBox1 = New System.Windows.Forms.GroupBox
Me.lbReportes = New System.Windows.Forms.ListBox
Me.GroupBox2 = New System.Windows.Forms.GroupBox
Me.Label3 = New System.Windows.Forms.Label
Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
Me.Label2 = New System.Windows.Forms.Label
Me.txtParametro1 = New System.Windows.Forms.TextBox
Me.Label1 = New System.Windows.Forms.Label
Me.GroupBox3.SuspendLayout()
Me.GroupBox1.SuspendLayout()
Me.GroupBox2.SuspendLayout()
Me.SuspendLayout()
'
'GroupBox3
'
Me.GroupBox3.Controls.Add(Me.lbListados)
Me.GroupBox3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.GroupBox3.Location = New System.Drawing.Point(12, 220)
Me.GroupBox3.Name = "GroupBox3"
Me.GroupBox3.Size = New System.Drawing.Size(198, 126)
Me.GroupBox3.TabIndex = 55
Me.GroupBox3.TabStop = False
Me.GroupBox3.Text = "Listados"
'
'lbListados
'
Me.lbListados.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.lbListados.FormattingEnabled = True
Me.lbListados.ItemHeight = 16
Me.lbListados.Items.AddRange(New Object() {"Tecnicos", "Operadores", "Clientes", "Cobranzas", "Tickets Pendientes", "Tickets resueltos"})
Me.lbListados.Location = New System.Drawing.Point(6, 19)
Me.lbListados.Name = "lbListados"
Me.lbListados.Size = New System.Drawing.Size(184, 100)
Me.lbListados.TabIndex = 1
'
'Button1
'
Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.Button1.ForeColor = System.Drawing.Color.Red
Me.Button1.Location = New System.Drawing.Point(12, 364)
Me.Button1.Name = "Button1"
Me.Button1.Size = New System.Drawing.Size(416, 47)
Me.Button1.TabIndex = 56
Me.Button1.Text = "Generar "
Me.Button1.UseVisualStyleBackColor = True
'
'GroupBox1
'
Me.GroupBox1.Controls.Add(Me.lbReportes)
Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
Me.GroupBox1.Name = "GroupBox1"
Me.GroupBox1.Size = New System.Drawing.Size(416, 192)
Me.GroupBox1.TabIndex = 57
Me.GroupBox1.TabStop = False
Me.GroupBox1.Text = "Reportes"
'
'lbReportes
'
Me.lbReportes.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.lbReportes.FormattingEnabled = True
Me.lbReportes.ItemHeight = 16
Me.lbReportes.Items.AddRange(New Object() {"Los n Tickets de Menor Monto", "Los n Tickets de Mayor Monto", "El Total Facturado", "Total Facturado por un Tecnico en Particular", "Total Facturado a una Empresa en Particular", "Total Facturado a una Empresa en una Fecha Particular", "Cantidad Total de Tickets Atendidios", "Cantidad de Tickets Atendidos por un Operador en Particular", "Cantidad de Tickets Resueltos por un Tecnico en Particular", "El Monto Promedio de cada Ticket"})
Me.lbReportes.Location = New System.Drawing.Point(6, 19)
Me.lbReportes.Name = "lbReportes"
Me.lbReportes.Size = New System.Drawing.Size(401, 164)
Me.lbReportes.TabIndex = 0
'
'GroupBox2
'
Me.GroupBox2.Controls.Add(Me.Label3)
Me.GroupBox2.Controls.Add(Me.DateTimePicker2)
Me.GroupBox2.Controls.Add(Me.DateTimePicker1)
Me.GroupBox2.Controls.Add(Me.Label2)
Me.GroupBox2.Controls.Add(Me.txtParametro1)
Me.GroupBox2.Controls.Add(Me.Label1)
Me.GroupBox2.Location = New System.Drawing.Point(216, 220)
Me.GroupBox2.Name = "GroupBox2"
Me.GroupBox2.Size = New System.Drawing.Size(212, 126)
Me.GroupBox2.TabIndex = 58
Me.GroupBox2.TabStop = False
Me.GroupBox2.Text = "Parámetros"
'
'Label3
'
Me.Label3.AutoSize = True
Me.Label3.Location = New System.Drawing.Point(9, 82)
Me.Label3.Name = "Label3"
Me.Label3.Size = New System.Drawing.Size(46, 13)
Me.Label3.TabIndex = 5
Me.Label3.Text = "Fecha 2"
'
'DateTimePicker2
'
Me.DateTimePicker2.Location = New System.Drawing.Point(9, 98)
Me.DateTimePicker2.Name = "DateTimePicker2"
Me.DateTimePicker2.Size = New System.Drawing.Size(200, 20)
Me.DateTimePicker2.TabIndex = 4
'
'DateTimePicker1
'
Me.DateTimePicker1.Location = New System.Drawing.Point(12, 58)
Me.DateTimePicker1.Name = "DateTimePicker1"
Me.DateTimePicker1.Size = New System.Drawing.Size(200, 20)
Me.DateTimePicker1.TabIndex = 3
'
'Label2
'
Me.Label2.AutoSize = True
Me.Label2.Location = New System.Drawing.Point(9, 42)
Me.Label2.Name = "Label2"
Me.Label2.Size = New System.Drawing.Size(46, 13)
Me.Label2.TabIndex = 0
Me.Label2.Text = "Fecha 1"
'
'txtParametro1
'
Me.txtParametro1.Location = New System.Drawing.Point(76, 19)
Me.txtParametro1.Name = "txtParametro1"
Me.txtParametro1.Size = New System.Drawing.Size(127, 20)
Me.txtParametro1.TabIndex = 1
'
'Label1
'
Me.Label1.AutoSize = True
Me.Label1.Location = New System.Drawing.Point(6, 22)
Me.Label1.Name = "Label1"
Me.Label1.Size = New System.Drawing.Size(55, 13)
Me.Label1.TabIndex = 0
Me.Label1.Text = "Parametro"
'
'frmListados
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.ClientSize = New System.Drawing.Size(442, 423)
Me.Controls.Add(Me.GroupBox2)
Me.Controls.Add(Me.GroupBox1)
Me.Controls.Add(Me.GroupBox3)
Me.Controls.Add(Me.Button1)
Me.Name = "frmListados"
Me.Text = "Listados"
Me.GroupBox3.ResumeLayout(False)
Me.GroupBox1.ResumeLayout(False)
Me.GroupBox2.ResumeLayout(False)
Me.GroupBox2.PerformLayout()
Me.ResumeLayout(False)

End Sub
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lbReportes As System.Windows.Forms.ListBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lbListados As System.Windows.Forms.ListBox
    Friend WithEvents txtParametro1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
