Imports System.Data.OleDb

Public Class frmGrupos
  Private drGRUPO As DataRow
  Private oGRUPO As CONTROLADORA.GRUPO
  Private dtGRUPO As DataTable
  Private dvGRUPO As DataView

  Private oAGRUPO As CONTROLADORA.AGRUPO
  Private drAGRUPO As DataRow
  Private dtAGRUPO As DataTable

  Dim OPT, Rta As Integer
  Dim Operacion, Autorizacion, Formulario As String

Private Sub btnNuevoPerfil_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
  frmPerfiles.Show()
End Sub

Private Sub tbPerfiles_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbPerfiles.ButtonClick
  Select Case tbPerfiles.Buttons.IndexOf(e.Button)
    Case 0 'ALTA
      OPT = 1
      Cancelar()
      tbSave.Enabled = True
      txtNombre.Enabled = True
      txtDescripcion.Enabled = True

    Case 1 'MODIFICAR
      OPT = 2
      tbSave.Enabled = True
      txtNombre.Enabled = True

    Case 2 'BAJA
      Rta = MsgBox("�Desea Eliminar Permanentemente el Grupo seleccionado?", MsgBoxStyle.YesNo, "TECSERV")
      If Rta = 7 Then Exit Sub

      If txtNombre.Text <> "" Then
        If MsgBox("Esta seguro de que desea eliminar el grupo " & txtNombre.Text & " ?", MsgBoxStyle.YesNo, "TECSERV") = MsgBoxResult.Yes Then
          oGRUPO.ELIMINAR_GRUPO(txtNro.Text)
          MsgBox("El Grupo ha sido Eliminado con Exito", MsgBoxStyle.Exclamation, "TECSERV")
          oGRUPO = New CONTROLADORA.GRUPO
          oAGRUPO = New CONTROLADORA.AGRUPO
          dtAGRUPO = oAGRUPO.OBTENER_GRUPOS
          dtGRUPO = oGRUPO.OBTENER_GRUPOS
          Generar_Auditoria("BAJA")
          Cancelar()
          LlenarListView()
        End If
      Else
        MsgBox("Seleccione un grupo por favor")
      End If

    Case 3 'GUARDAR
      If txtNombre.Text <> "" Then
        If OPT = 1 Then
          drGRUPO = oGRUPO.CREAR_GRUPO()
          Guardar_Datos()
          oGRUPO.AGREGAR_GRUPO(drGRUPO)
          Generar_Auditoria("ALTA")
          MsgBox("Grupo creado con Exito", MsgBoxStyle.Information, "TECSERV")
        Else
          Guardar_Datos()
          oGRUPO.MODIFICAR_GRUPO(drGRUPO)
          dtGRUPO = oGRUPO.OBTENER_GRUPOS
          Generar_Auditoria("MODIFICACION")
          MsgBox("grupo modificado con Exito", MsgBoxStyle.Information, "TECSERV")
        End If
        oGRUPO = New CONTROLADORA.GRUPO
        oAGRUPO = New CONTROLADORA.AGRUPO
        dtGRUPO = oGRUPO.OBTENER_GRUPOS
        dtAGRUPO = oAGRUPO.OBTENER_GRUPOS
        Cancelar()
        LlenarListView()
        txtNombre.Enabled = False
      Else
        MsgBox("Por Favor asigne un nombre al nuevo grupo", MsgBoxStyle.Exclamation, "TECSERV")
      End If

    Case 4 'CANCELAR
      Cancelar()

    Case 5 'VOLVER
      Me.Close()
  End Select
End Sub

Private Sub Generar_Auditoria(ByVal TipoEvento)
  'SE GENERA LA AUDITORIA
  drAGRUPO = oAGRUPO.CREAR_GRUPO()

  drAGRUPO("nomGrupo") = UCase(Trim(txtNombre.Text))
  drAGRUPO("audFecha") = Microsoft.VisualBasic.DateString
  drAGRUPO("audHora") = Microsoft.VisualBasic.Left(Microsoft.VisualBasic.TimeString, 5)
  drAGRUPO("audTipo") = TipoEvento
  drAGRUPO("audUsuario") = UsuarioLogeado

  oAGRUPO.AGREGAR_GRUPO(drAGRUPO)
End Sub

Private Sub Cancelar()
  txtNro.Text = ""
  txtNro.Enabled = False
  txtNombre.Text = ""
  txtNombre.Enabled = False
  txtDescripcion.Text = ""
  txtDescripcion.Enabled = False
  tbSave.Enabled = False
End Sub

Private Sub Guardar_Datos()
  drGRUPO("GrNombre") = UCase(Trim(txtNombre.Text))
  drGRUPO("GrDescripcion") = UCase(Trim(txtDescripcion.Text))
End Sub

Private Sub frmGrupos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
Dim x As Integer
  Formulario = "5"
  For x = 1 To 3
    Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, x)
    If x = 1 And Autorizacion = "NO" Then
      tbPerfiles.Buttons(0).Visible = False
    ElseIf x = 2 And Autorizacion = "NO" Then
      tbPerfiles.Buttons(1).Visible = False
    ElseIf x = 3 And Autorizacion = "NO" Then
      tbPerfiles.Buttons(2).Visible = False
    End If
  Next
  OPT = 0
  oGRUPO = New CONTROLADORA.GRUPO
  dtGRUPO = oGRUPO.OBTENER_GRUPOS
  Cancelar()
  LlenarListView()
  txtNombre.Enabled = False
  oAGRUPO = New CONTROLADORA.AGRUPO
  dtAGRUPO = oAGRUPO.OBTENER_GRUPOS
End Sub

Private Sub LlenarListView()
  Dim itmGrupo As ListViewItem

  lvwGrupos.Items.Clear()
  For Each rwGrupos As DataRow In dtGRUPO.Rows
    itmGrupo = New ListViewItem(rwGrupos(0).ToString())
    itmGrupo.SubItems.Add(rwGrupos(1).ToString)
    lvwGrupos.Items.Add(itmGrupo)
  Next
End Sub

Private Sub lvwGrupos_ItemActivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwGrupos.ItemActivate
  drGRUPO = oGRUPO.OBTENER_GRUPO(lvwGrupos.FocusedItem.Text)
  txtNro.Text = drGRUPO("Id_Grupo")
  txtNombre.Text = drGRUPO("GrNombre")
  txtDescripcion.Text = drGRUPO("GrDescripcion")
  tbSave.Enabled = False
End Sub

End Class