<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLogin
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
Me.txtContraseña = New System.Windows.Forms.TextBox
Me.txtNombre = New System.Windows.Forms.TextBox
Me.Label2 = New System.Windows.Forms.Label
Me.Label1 = New System.Windows.Forms.Label
Me.btnSalir = New System.Windows.Forms.Button
Me.btnAceptar = New System.Windows.Forms.Button
Me.SuspendLayout()
'
'txtContraseña
'
Me.txtContraseña.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.txtContraseña.Location = New System.Drawing.Point(106, 38)
Me.txtContraseña.MaxLength = 20
Me.txtContraseña.Name = "txtContraseña"
Me.txtContraseña.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
Me.txtContraseña.Size = New System.Drawing.Size(150, 20)
Me.txtContraseña.TabIndex = 5
Me.txtContraseña.Text = "1111"
'
'txtNombre
'
Me.txtNombre.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.txtNombre.Location = New System.Drawing.Point(106, 12)
Me.txtNombre.MaxLength = 20
Me.txtNombre.Name = "txtNombre"
Me.txtNombre.Size = New System.Drawing.Size(150, 20)
Me.txtNombre.TabIndex = 4
Me.txtNombre.Text = "BIAVA"
'
'Label2
'
Me.Label2.AutoSize = True
Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.Label2.Location = New System.Drawing.Point(27, 40)
Me.Label2.Name = "Label2"
Me.Label2.Size = New System.Drawing.Size(73, 15)
Me.Label2.TabIndex = 8
Me.Label2.Text = "Contraseña"
'
'Label1
'
Me.Label1.AutoSize = True
Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.Label1.Location = New System.Drawing.Point(48, 14)
Me.Label1.Name = "Label1"
Me.Label1.Size = New System.Drawing.Size(52, 15)
Me.Label1.TabIndex = 7
Me.Label1.Text = "Nombre"
'
'btnSalir
'
Me.btnSalir.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.btnSalir.ForeColor = System.Drawing.Color.Red
Me.btnSalir.Location = New System.Drawing.Point(176, 77)
Me.btnSalir.Name = "btnSalir"
Me.btnSalir.Size = New System.Drawing.Size(107, 35)
Me.btnSalir.TabIndex = 9
Me.btnSalir.Text = "Salir"
Me.btnSalir.UseVisualStyleBackColor = True
'
'btnAceptar
'
Me.btnAceptar.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.btnAceptar.ForeColor = System.Drawing.Color.Blue
Me.btnAceptar.Location = New System.Drawing.Point(42, 77)
Me.btnAceptar.Name = "btnAceptar"
Me.btnAceptar.Size = New System.Drawing.Size(107, 35)
Me.btnAceptar.TabIndex = 6
Me.btnAceptar.Text = "Aceptar"
Me.btnAceptar.UseVisualStyleBackColor = True
'
'frmLogin
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.ClientSize = New System.Drawing.Size(318, 129)
Me.Controls.Add(Me.txtContraseña)
Me.Controls.Add(Me.txtNombre)
Me.Controls.Add(Me.Label2)
Me.Controls.Add(Me.Label1)
Me.Controls.Add(Me.btnSalir)
Me.Controls.Add(Me.btnAceptar)
Me.Name = "frmLogin"
Me.Text = "Inicio de Sesion"
Me.ResumeLayout(False)
Me.PerformLayout()

End Sub
    Friend WithEvents txtContraseña As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
End Class
