Imports System.Data.OleDb

Public Class frmPermisos
  Private drPERMISO As DataRow
  Private oPERMISO As CONTROLADORA.PERMISO
  Private dtPERMISO As DataTable
  Private dvPERMISO As DataView

  Private oAPERMISO As CONTROLADORA.APERMISO
  Private drAPERMISO As DataRow
  Private dtAPERMISO As DataTable

  Dim OPT, Rta As Integer
  Dim Operacion, Autorizacion, Formulario As String

Private Sub tbPermisos_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles tbPermisos.ButtonClick
  Select Case tbPermisos.Buttons.IndexOf(e.Button)
    Case 0 'ALTA
      OPT = 1
      Cancelar()
      tbSave.Enabled = True
      txtNombre.Enabled = True
      txtDescripcion.Enabled = True

    Case 1 'MODIFICAR
      OPT = 2
      tbSave.Enabled = True
      txtNombre.Enabled = True

    Case 2 'BAJA
      Rta = MsgBox("�Desea Eliminar Permanentemente el Permiso seleccionado?", MsgBoxStyle.YesNo, "TECSERV")
      If Rta = 7 Then Exit Sub
      If txtNombre.Text <> "" Then
        If MsgBox("Esta seguro de que desea eliminar el permiso " & txtNombre.Text & " ?", MsgBoxStyle.YesNo, "TECSERV") = MsgBoxResult.Yes Then
          oPERMISO.ELIMINAR_PERMISO(txtNro.Text)
          MsgBox("El Permiso ha sido Eliminado con Exito", MsgBoxStyle.Exclamation, "TECSERV")
          oPERMISO = New CONTROLADORA.PERMISO
          oAPERMISO = New CONTROLADORA.APERMISO
          dtPERMISO = oPERMISO.OBTENER_PERMISOS
          dtAPERMISO = oAPERMISO.OBTENER_PERMISOS
          Generar_Auditoria("BAJA")
          Cancelar()
          LlenarListView()
        End If
      Else
        MsgBox("Seleccione un permiso por favor")
      End If
    Case 3 'GUARDAR
      If txtNombre.Text <> "" Then
        If OPT = 1 Then
          drPERMISO = oPERMISO.CREAR_PERMISO()
          Guardar_Datos()
          oPERMISO.AGREGAR_PERMISO(drPERMISO)
          Generar_Auditoria("ALTA")
          MsgBox("Permiso creado con Exito", MsgBoxStyle.Information, "TECSERV")
        Else
          Guardar_Datos()
          oPERMISO.MODIFICAR_PERMISO(drPERMISO)
          dtPERMISO = oPERMISO.OBTENER_PERMISOS
          Generar_Auditoria("MODIFICACION")
          MsgBox("Permiso modificado con Exito", MsgBoxStyle.Information, "TECSERV")
        End If
        oPERMISO = New CONTROLADORA.PERMISO
        oAPERMISO = New CONTROLADORA.APERMISO
        dtPERMISO = oPERMISO.OBTENER_PERMISOS
        dtAPERMISO = oAPERMISO.OBTENER_PERMISOS
        Cancelar()
        LlenarListView()
        txtNombre.Enabled = False
        txtDescripcion.Enabled = False
      Else
        MsgBox("Por Favor asigne un nombre al nuevo permiso", MsgBoxStyle.Exclamation, "TECSERV")
      End If

    Case 4 'CANCELAR
      Cancelar()

    Case 5 'VOLVER
      Me.Close()
  End Select
End Sub

Private Sub Generar_Auditoria(ByVal TipoEvento)
  'SE GENERA LA AUDITORIA
  drAPERMISO = oAPERMISO.CREAR_PERMISO()

  drAPERMISO("perNombre") = UCase(Trim(txtNombre.Text))
  drAPERMISO("audFecha") = Microsoft.VisualBasic.DateString
  drAPERMISO("audHora") = Microsoft.VisualBasic.Left(Microsoft.VisualBasic.TimeString, 5)
  drAPERMISO("audTipo") = TipoEvento
  drAPERMISO("audUsuario") = UsuarioLogeado

  oAPERMISO.AGREGAR_PERMISO(drAPERMISO)
End Sub

Private Sub Cancelar()
  txtNro.Text = ""
  txtNro.Enabled = False
  txtNombre.Text = ""
  txtNombre.Enabled = False
  txtDescripcion.Text = ""
  txtDescripcion.Enabled = False
  tbSave.Enabled = False
End Sub

Private Sub Guardar_Datos()
  drPERMISO("perNombre") = Trim(txtNombre.Text)
  drPERMISO("perDescripcion") = Trim(txtDescripcion.Text)
End Sub

Private Sub frmPermisos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
Dim x As Integer
  Formulario = "8"
  For x = 1 To 3
    Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, x)
    If x = 1 And Autorizacion = "NO" Then
      tbPermisos.Buttons(0).Visible = False
    ElseIf x = 2 And Autorizacion = "NO" Then
      tbPermisos.Buttons(1).Visible = False
    ElseIf x = 3 And Autorizacion = "NO" Then
      tbPermisos.Buttons(2).Visible = False
    End If
  Next
  OPT = 0
  oPERMISO = New CONTROLADORA.PERMISO
  dtPERMISO = oPERMISO.OBTENER_PERMISOS
  Cancelar()
  LlenarListView()
  txtNombre.Enabled = False
  txtDescripcion.Enabled = False
  oAPERMISO = New CONTROLADORA.APERMISO
  dtAPERMISO = oAPERMISO.OBTENER_PERMISOS
End Sub

Private Sub LlenarListView()
  Dim itmGrupo As ListViewItem

  lvwPermisos.Items.Clear()
  For Each rwGrupos As DataRow In dtPERMISO.Rows
    itmGrupo = New ListViewItem(rwGrupos(0).ToString())
    itmGrupo.SubItems.Add(rwGrupos(1).ToString)
    lvwPermisos.Items.Add(itmGrupo)
  Next
End Sub

Private Sub lvwPermisos_ItemActivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwPermisos.ItemActivate
  drPERMISO = oPERMISO.OBTENER_PERMISO(lvwPermisos.FocusedItem.Text)
  txtNro.Text = drPERMISO("Id_Permiso")
  txtNombre.Text = drPERMISO("perNombre")
  txtDescripcion.Text = drPERMISO("perDescripcion")
  tbSave.Enabled = False
End Sub

End Class