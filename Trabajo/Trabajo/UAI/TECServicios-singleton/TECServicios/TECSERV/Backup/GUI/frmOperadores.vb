Imports System.Data.OleDb
Imports System.Security.Cryptography
Imports System.Text

Public Class frmOperadores
  Private drOPERADOR As DataRow
  Private oOPERADOR As CONTROLADORA.OPERADOR
  Private dtOPERADOR As DataTable
  Private dvOPERADOR As DataView
  Private oGRUPO As CONTROLADORA.GRUPO
  Private dtGRUPO As DataTable
  Private oPERFIL As CONTROLADORA.PERFIL
  Private dtPERFIL As DataTable
  Private drPERFIL As DataRow

  Private drAOPERADOR As DataRow
  Private oAOPERADOR As CONTROLADORA.AOPERADOR
  Private dtAOPERADOR As DataTable

  Dim BAN, x, Rta As Integer
  Dim ClaveHASH As String
  Dim Autorizacion, Grupo, Formulario, Operacion As String

Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
  BAN = 1
  btnGuardar.Enabled = True
  Limpiar()
  dtGRUPO = oGRUPO.OBTENER_GRUPOS
  LlenarComboGrupos()
  GroupBox2.Enabled = True
End Sub

Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
  BAN = 2
  btnGuardar.Enabled = True
  dtGRUPO = oGRUPO.OBTENER_GRUPOS
  LlenarComboGrupos()
  GroupBox2.Enabled = True
End Sub

Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
Dim md5 As New MD5CryptoServiceProvider
ClaveHASH = (getMd5Hash(txtOpContrase�a.Text))
  If txtOpApellido.Text <> "" And txtOpNombre.Text <> "" And txtOpNombreUsuario.Text <> "" And cmbOpGrupos.Text <> "" And txtOpTelefono.Text <> "" Then
    If BAN = 1 Then
      drOPERADOR = oOPERADOR.CREAR_OPERADOR
      Guardar_Datos()
      oOPERADOR.AGREGAR_OPERADOR(drOPERADOR)
      Generar_Auditoria("ALTA")
      Limpiar()
      oOPERADOR = New CONTROLADORA.OPERADOR
      dtOPERADOR = oOPERADOR.OBTENER_OPERADORES
      LlenarListView()
      MsgBox("El nuevo Operador ha sido dado de Alta con �xito", MsgBoxStyle.Information, "TECSERV")
    ElseIf BAN = 2 Then
      Guardar_Datos()
      oOPERADOR.MODIFICAR_OPERADOR(drOPERADOR)
      Generar_Auditoria("MODIFICACION")
      Limpiar()
      oOPERADOR = New CONTROLADORA.OPERADOR
      dtOPERADOR = oOPERADOR.OBTENER_OPERADORES
      LlenarListView()
      MsgBox("El nuevo Operador ha sido Modificado con �xito", MsgBoxStyle.Information, "TECSERV")
    End If
    GroupBox2.Enabled = False
    btnGuardar.Enabled = False
  Else
    MsgBox("Faltan Campos a Completar!!", MsgBoxStyle.Exclamation, "TECSERV")
  End If
End Sub

Private Sub Guardar_Datos()
  drOPERADOR("logApellido") = UCase(Trim(txtOpApellido.Text))
  drOPERADOR("logNombre") = UCase(Trim(txtOpNombre.Text))
  drOPERADOR("logTelefono") = UCase(Trim(txtOpTelefono.Text))
  drOPERADOR("logNomUsuario") = UCase(Trim(txtOpNombreUsuario.Text))
  drOPERADOR("logClave") = ClaveHASH
  drOPERADOR("Id_Grupo") = Val(cmbOpGrupos.SelectedIndex + 1)
End Sub

Private Sub Generar_Auditoria(ByVal TipoEvento)

  drAOPERADOR = oAOPERADOR.CREAR_OPERADOR
  drAOPERADOR("logApellido") = UCase(Trim(txtOpApellido.Text))
  drAOPERADOR("logNombre") = UCase(Trim(txtOpNombre.Text))
  drAOPERADOR("logTelefono") = UCase(Trim(txtOpTelefono.Text))
  drAOPERADOR("logNomUsuario") = UCase(Trim(txtOpNombreUsuario.Text))
  drAOPERADOR("logClave") = ClaveHASH
  drAOPERADOR("logGrupo") = UCase(Trim(cmbOpGrupos.Text))
  drAOPERADOR("audTipo") = TipoEvento
  drAOPERADOR("audFecha") = Microsoft.VisualBasic.DateString
  drAOPERADOR("audHora") = Microsoft.VisualBasic.Left(Microsoft.VisualBasic.TimeString, 5)
  drAOPERADOR("audUsuario") = UsuarioLogeado

  oAOPERADOR.AGREGAR_OPERADOR(drAOPERADOR)
End Sub

Private Sub Limpiar()
  txtOpCodigo.Text = ""
  txtOpApellido.Text = ""
  txtOpNombre.Text = ""
  txtOpTelefono.Text = ""
  txtOpNombreUsuario.Text = ""
  txtOpContrase�a.Text = ""
  cmbOpGrupos.Text = ""
End Sub

Private Sub LlenarListView()
  Dim lvOPERADORES As ListViewItem

  lvwOperadores.Items.Clear()
  For Each rwclientes As DataRow In dtOPERADOR.Rows
    lvOPERADORES = New ListViewItem(rwclientes(0).ToString())
    lvOPERADORES.SubItems.Add(rwclientes(1).ToString)
    lvOPERADORES.SubItems.Add(rwclientes(2).ToString)
    'lvOPERADORES.SubItems.Add(rwclientes(3).ToString)
    lvwOperadores.Items.Add(lvOPERADORES)
  Next
End Sub

Private Sub frmOperadores_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
Dim x As Integer
  Formulario = "4"
  For x = 1 To 3
    Autorizacion = Verificar_Perfil(GrupoUsuario, Formulario, x)
    If x = 1 And Autorizacion = "NO" Then
      btnAgregar.Visible = False
    ElseIf x = 2 And Autorizacion = "NO" Then
      btnEliminar.Visible = False
    ElseIf x = 3 And Autorizacion = "NO" Then
      btnModificar.Visible = False
    End If
  Next
  GroupBox2.Enabled = False
  oOPERADOR = New CONTROLADORA.OPERADOR
  dtOPERADOR = oOPERADOR.OBTENER_OPERADORES
  LlenarListView()
  LlenarComboGrupos()
  oAOPERADOR = New CONTROLADORA.AOPERADOR
  dtAOPERADOR = oAOPERADOR.OBTENER_OPERADORES
End Sub

Private Sub LlenarComboGrupos()
  oGRUPO = New CONTROLADORA.GRUPO
  dtGRUPO = oGRUPO.OBTENER_GRUPOS
  cmbOpGrupos.Items.Clear()
  For Each rwGrupos As DataRow In dtGRUPO.Rows
    cmbOpGrupos.Items.Add(rwGrupos(1).ToString)
  Next
End Sub

Private Sub cmbOpGrupos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOpGrupos.SelectedIndexChanged
  oPERFIL = New CONTROLADORA.PERFIL
  dtPERFIL = oPERFIL.OBTENER_PERFILES
End Sub

Private Sub lvwOperadores_ItemActivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwOperadores.ItemActivate
  GroupBox2.Enabled = False
  drOPERADOR = oOPERADOR.OBTENER_OPERADOR(lvwOperadores.FocusedItem.Text)
  btnGuardar.Enabled = False
  LlenarComboGrupos()
  Cargar_Datos_Operador()
End Sub

Private Sub Cargar_Datos_Operador()

  txtOpCodigo.Text = drOPERADOR("Id_Operador")
  txtOpApellido.Text = drOPERADOR("logApellido")
  txtOpNombre.Text = drOPERADOR("logNombre")
  txtOpTelefono.Text = drOPERADOR("logTelefono")
  txtOpNombreUsuario.Text = drOPERADOR("logNomUsuario")
  txtOpContrase�a.Text = drOPERADOR("logClave")
  cmbOpGrupos.SelectedIndex = Val(drOPERADOR("Id_Grupo")) - 1
End Sub

Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
  If txtOpCodigo.Text <> "" And txtOpApellido.Text <> "" And txtOpNombre.Text <> "" Then
  Rta = MsgBox("�Desea Eliminar Permanentemente el Operador seleccionado?", MsgBoxStyle.YesNo, "TECSERV")
  If Rta = 7 Then Exit Sub

    If MsgBox("Est� seguro que desea Eliminar el T�cnico " & txtOpApellido.Text, MsgBoxStyle.YesNo, "TECSERV") = MsgBoxResult.Yes Then
      GroupBox2.Enabled = False
      oOPERADOR.ELIMINAR_OPERADOR(txtOpCodigo.Text)
      Generar_Auditoria("BAJA")
      Limpiar()
      dtOPERADOR = oOPERADOR.OBTENER_OPERADORES
      LlenarListView()
      MsgBox("El Operador ha sido Eliminado con �xito", MsgBoxStyle.Information, "TECSERV")
    End If
  Else
    MsgBox("Seleccione un Operador por favor", MsgBoxStyle.Exclamation, "TECSERV")
  End If
End Sub

Public Function getMd5Hash(ByVal input As String) As String
Dim md5Hasher As New MD5CryptoServiceProvider
Dim data As Byte() = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input))
Dim sBuilder As New StringBuilder()
Dim i As Integer
For i = 0 To data.Length - 1
sBuilder.Append(data(i).ToString("x2"))
Next i
Return sBuilder.ToString()
End Function

Private Sub lvwOperadores_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvwOperadores.SelectedIndexChanged

End Sub
End Class