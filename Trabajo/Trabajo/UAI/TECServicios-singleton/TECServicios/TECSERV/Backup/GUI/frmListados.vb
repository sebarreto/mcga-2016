Imports System.Data.OleDb

Public Class frmListados
  Private con As OleDbConnection
  Private cmd As OleDbCommand
  Private Reader As OleDb.OleDbDataReader

  Private oTECNICO As CONTROLADORA.TECNICO  ' OBJETO CLIENTE
  Private dtTECNICO As DataTable

  Private oOPERADOR As CONTROLADORA.OPERADOR  ' OBJETO CLIENTE
  Private dtOPERADOR As DataTable

  Private oCLIENTE As CONTROLADORA.CLIENTES   ' OBJETO CLIENTE
  Private dtCLIENTE As DataTable

  Dim x As Integer
  Dim OptList As Integer

Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
'LISTADOS
  If OptList = 0 Then
    Reporte_Tecnicos()
  ElseIf OptList = 1 Then
    Reporte_Operadores()
  ElseIf OptList = 2 Then
    Reporte_Clientes()
  ElseIf OptList = 3 Then
    Reporte_Cobranza()
  ElseIf OptList = 4 Then
    Reporte_Tickets_Pendientes()
  ElseIf OptList = 5 Then
    Reporte_Tickets_Resueltos()
'REPORTES
  ElseIf OptList = 6 Then
    nTickets_Menor_Monto()
  ElseIf OptList = 7 Then
    nTickets_Mayor_Monto()
  ElseIf OptList = 8 Then
    Total_Facturado()
  ElseIf OptList = 9 Then
    Total_Facturado_Tecnico()
  ElseIf OptList = 10 Then
    Total_Facturado_Empresa()
  ElseIf OptList = 11 Then
    Total_Facturado_Empresa_Mes()
  ElseIf OptList = 12 Then
    Cantidad_Tickets_Atendidos()
  ElseIf OptList = 13 Then
    total_tickets_atendidos_operador()
  ElseIf OptList = 14 Then
    total_tickets_resueltos_tecnico()
  ElseIf OptList = 15 Then
    ticket_monto_promedio()
  End If
End Sub

Private Sub Reporte_Tecnicos()
Dim objExcel As Excel.Application
  x = 3
  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\tecnico.xls")

  oTECNICO = New CONTROLADORA.TECNICO
  dtTECNICO = oTECNICO.OBTENER_TECNICOS

  For Each rwTecnicos As DataRow In dtTECNICO.Rows
    objExcel.Cells(x, 1).Formula = rwTecnicos(0).ToString
    objExcel.Cells(x, 1).WrapText = True

    objExcel.Cells(x, 2).Formula = rwTecnicos(1).ToString
    objExcel.Cells(x, 2).WrapText = True

    objExcel.Cells(x, 3).Formula = rwTecnicos(2).ToString
    objExcel.Cells(x, 3).WrapText = True

    objExcel.Cells(x, 4).Formula = rwTecnicos(3).ToString
    objExcel.Cells(x, 4).WrapText = True

    objExcel.Cells(x, 5).Formula = rwTecnicos(4).ToString
    objExcel.Cells(x, 5).WrapText = True

    objExcel.Cells(x, 6).Formula = rwTecnicos(5).ToString
    objExcel.Cells(x, 6).WrapText = True

    x = x + 1
  Next
End Sub

Private Sub Reporte_Operadores()
Dim objExcel As Excel.Application
  x = 3
  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\operador.xls")

  oOPERADOR = New CONTROLADORA.OPERADOR
  dtOPERADOR = oOPERADOR.OBTENER_OPERADORES

  For Each rwOperadores As DataRow In dtOPERADOR.Rows
    objExcel.Cells(x, 1).Formula = rwOperadores(0).ToString
    objExcel.Cells(x, 1).WrapText = True

    objExcel.Cells(x, 2).Formula = rwOperadores(1).ToString
    objExcel.Cells(x, 2).WrapText = True

    objExcel.Cells(x, 3).Formula = rwOperadores(2).ToString
    objExcel.Cells(x, 3).WrapText = True

    objExcel.Cells(x, 4).Formula = rwOperadores(4).ToString
    objExcel.Cells(x, 4).WrapText = True

    objExcel.Cells(x, 5).Formula = rwOperadores(3).ToString
    objExcel.Cells(x, 5).WrapText = True

    objExcel.Cells(x, 6).Formula = rwOperadores(6).ToString
    objExcel.Cells(x, 6).WrapText = True

    objExcel.Cells(x, 7).Formula = rwOperadores(7).ToString
    objExcel.Cells(x, 7).WrapText = True
    x = x + 1
  Next
End Sub

Private Sub Reporte_Clientes()
Dim objExcel As Excel.Application
  x = 3
  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\cliente.xls")

  oCLIENTE = New CONTROLADORA.CLIENTES
  dtCLIENTE = oCLIENTE.OBTENER_CLIENTES

  For Each rwClientes As DataRow In dtCLIENTE.Rows
    objExcel.Cells(x, 1).Formula = rwClientes(0).ToString
    objExcel.Cells(x, 1).WrapText = True

    objExcel.Cells(x, 2).Formula = rwClientes(1).ToString
    objExcel.Cells(x, 2).WrapText = True

    objExcel.Cells(x, 3).Formula = rwClientes(2).ToString
    objExcel.Cells(x, 3).WrapText = True

    objExcel.Cells(x, 4).Formula = rwClientes(3).ToString
    objExcel.Cells(x, 4).WrapText = True

    objExcel.Cells(x, 5).Formula = rwClientes(4).ToString
    objExcel.Cells(x, 5).WrapText = True

    objExcel.Cells(x, 6).Formula = rwClientes(5).ToString
    objExcel.Cells(x, 6).WrapText = True

    objExcel.Cells(x, 7).Formula = rwClientes(6).ToString
    objExcel.Cells(x, 7).WrapText = True

    x = x + 1
  Next
End Sub

Private Sub Reporte_Cobranza()
  Dim objExcel As Excel.Application
  x = 3

  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\cobranza.xls")

  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con
  cmd.CommandText = "SELECT * FROM T_Tickets  WHERE Cobranza = '" & "S" & "' "
  con.Open()
  Reader = cmd.ExecuteReader

  While (Reader.Read())
    objExcel.Cells(x, 1).Formula = Reader.GetInt32(0)
    objExcel.Cells(x, 2).Formula = Reader.GetString(2)
    objExcel.Cells(x, 3).Formula = Reader.GetString(3)
    objExcel.Cells(x, 4).Formula = Reader.GetInt32(6)
    objExcel.Cells(x, 5).Formula = Reader.GetInt32(8)
    objExcel.Cells(x, 6).Formula = Reader.GetDateTime(9)
    objExcel.Cells(x, 7).Formula = Reader.GetString(10)

    x = x + 1
  End While
End Sub

Private Sub Reporte_Tickets_Pendientes()
  Dim objExcel As Excel.Application
  x = 3

  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\ticket_pendiente.xls")

  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con
  cmd.CommandText = "SELECT * FROM T_Tickets  WHERE Nconforme = " & 0 & " "
  con.Open()
  Reader = cmd.ExecuteReader

  While (Reader.Read())
    objExcel.Cells(x, 1).Formula = Reader.GetInt32(0)
    objExcel.Cells(x, 2).Formula = Reader.GetString(2)
    objExcel.Cells(x, 3).Formula = Reader.GetString(3)
    objExcel.Cells(x, 4).Formula = Reader.GetDateTime(9)
    objExcel.Cells(x, 5).Formula = Reader.GetString(10)

    x = x + 1
  End While
End Sub

Private Sub Reporte_Tickets_Resueltos()
  Dim objExcel As Excel.Application
  x = 3

  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\ticket_resuelto.xls")

  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con
  cmd.CommandText = "SELECT * FROM T_Tickets  WHERE Nconforme <> " & 0 & " "
  con.Open()
  Reader = cmd.ExecuteReader

  While (Reader.Read())
    objExcel.Cells(x, 1).Formula = Reader.GetInt32(0)
    objExcel.Cells(x, 2).Formula = Reader.GetString(2)
    objExcel.Cells(x, 3).Formula = Reader.GetString(3)
    objExcel.Cells(x, 4).Formula = Reader.GetInt32(6)
    objExcel.Cells(x, 5).Formula = Reader.GetDateTime(9)
    objExcel.Cells(x, 6).Formula = Reader.GetString(10)
    x = x + 1
  End While
End Sub

Private Sub nTickets_Menor_Monto()
  Dim objExcel As Excel.Application
  x = 3
  If txtParametro1.Text = "" Then
    MsgBox("Ingrese un valor en el parametro, por favor !!", MsgBoxStyle.Exclamation, "TECSERV")
    Exit Sub
  End If
  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\ticket_monto_menor.xls")
  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con

  cmd.CommandText = "SELECT TOP " & txtParametro1.Text & " * FROM T_Tickets WHERE Cobranza = '" & "S" & "' ORDER BY Monto ASC "
  con.Open()
  Reader = cmd.ExecuteReader

  While (Reader.Read())
    objExcel.Cells(x, 1).Formula = Reader.GetInt32(0)
    objExcel.Cells(x, 2).Formula = Reader.GetString(2)
    objExcel.Cells(x, 3).Formula = Reader.GetString(3)
    objExcel.Cells(x, 4).Formula = Reader.GetInt32(6)
    objExcel.Cells(x, 5).Formula = Reader.GetInt32(8)
    x = x + 1
  End While
End Sub

Private Sub nTickets_Mayor_Monto()
  Dim objExcel As Excel.Application
  x = 3
  If txtParametro1.Text = "" Then
    MsgBox("Ingrese un valor en el parametro, por favor !!", MsgBoxStyle.Exclamation, "TECSERV")
    Exit Sub
  End If
  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\ticket_monto_mayor.xls")
  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con

  cmd.CommandText = "SELECT TOP " & txtParametro1.Text & " * FROM T_Tickets WHERE Cobranza = '" & "S" & "' ORDER BY Monto DESC"
  con.Open()
  Reader = cmd.ExecuteReader

  While (Reader.Read())
    objExcel.Cells(x, 1).Formula = Reader.GetInt32(0)
    objExcel.Cells(x, 2).Formula = Reader.GetString(2)
    objExcel.Cells(x, 3).Formula = Reader.GetString(3)
    objExcel.Cells(x, 4).Formula = Reader.GetInt32(6)
    objExcel.Cells(x, 5).Formula = Reader.GetInt32(8)
    x = x + 1
  End While
End Sub

Private Sub Total_Facturado()
  Dim objExcel As Excel.Application
  x = 3

  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\total_facturado.xls")
  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con

  cmd.CommandText = "SELECT SUM (Monto)as SumaTotal FROM T_Tickets"
  con.Open()
  Reader = cmd.ExecuteReader

  While (Reader.Read())
    objExcel.Cells(x, 5).Formula = Reader.Item("SumaTotal").ToString & "= Pesos"
  End While
End Sub

Private Sub Total_Facturado_Tecnico()
Dim objExcel As Excel.Application
  x = 3
  If txtParametro1.Text = "" Then
    MsgBox("Ingrese un valor en el parametro, por favor !!", MsgBoxStyle.Exclamation, "TECSERV")
    Exit Sub
  End If
  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\total_facturado_tecnico.xls")
  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con

  cmd.CommandText = "SELECT SUM (Monto)as SumaTotal FROM T_Tickets INNER JOIN T_Tecnicos ON T_Tecnicos.Id_Tecnico = T_Tickets.Id_tecnico WHERE T_Tecnicos.TecApellido = '" & UCase(Trim(txtParametro1.Text)) & "' "
  con.Open()
  Reader = cmd.ExecuteReader

  While (Reader.Read())
    objExcel.Cells(x, 5).Formula = Reader.Item("SumaTotal").ToString & "= Pesos"
  End While
  Reader.Close()
  cmd.CommandText = "SELECT * FROM T_Tecnicos WHERE TecApellido = '" & UCase(Trim(txtParametro1.Text)) & "' "
  Reader = cmd.ExecuteReader
  While (Reader.Read())
    objExcel.Cells(7, 1).formula = Reader.GetInt32(0)
    objExcel.Cells(7, 2).formula = Reader.GetString(2) & " " & Reader.GetString(1)
    objExcel.Cells(7, 3).formula = Reader.GetString(3)
    objExcel.Cells(7, 4).formula = Reader.GetString(4)
    objExcel.Cells(7, 5).formula = Reader.GetString(5)
  End While
End Sub

Private Sub Total_Facturado_Empresa()
Dim objExcel As Excel.Application
 x = 3
  If txtParametro1.Text = "" Then
    MsgBox("Ingrese un valor en el parametro, por favor !!", MsgBoxStyle.Exclamation, "TECSERV")
    Exit Sub
  End If
  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\total_facturado_empresa.xls")
  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con

  cmd.CommandText = "SELECT SUM (Monto)as SumaTotal FROM T_Tickets WHERE NomCliente = '" & UCase(Trim(txtParametro1.Text)) & "' "
  con.Open()
  Reader = cmd.ExecuteReader

  While (Reader.Read())
    objExcel.Cells(x, 5).Formula = Reader.Item("SumaTotal").ToString & "= Pesos"
  End While
  Reader.Close()
  cmd.CommandText = "SELECT * FROM T_Clientes WHERE cliNombre = '" & UCase(Trim(txtParametro1.Text)) & "' "
  Reader = cmd.ExecuteReader
  While (Reader.Read())
    objExcel.Cells(7, 1).formula = Reader.GetInt32(0)
    objExcel.Cells(7, 2).formula = Reader.GetString(2)
    objExcel.Cells(7, 3).formula = Reader.GetString(3)
    objExcel.Cells(7, 4).formula = Reader.GetString(5)
    objExcel.Cells(7, 5).formula = Reader.GetString(4)
  End While
End Sub

Private Sub Cantidad_Tickets_Atendidos()
Dim objExcel As Excel.Application
 x = 3

  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\total_tickets_atendidos.xls")
  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con

  cmd.CommandText = "SELECT COUNT (Atendio)as SumaTotal FROM T_Tickets "
  con.Open()
  Reader = cmd.ExecuteReader

  While (Reader.Read())
    objExcel.Cells(x, 5).Formula = Reader.Item("SumaTotal").ToString
  End While
  Reader.Close()
End Sub

Private Sub total_tickets_atendidos_operador()
Dim objExcel As Excel.Application
 x = 3
  If txtParametro1.Text = "" Then
    MsgBox("Ingrese un valor en el parametro, por favor !!", MsgBoxStyle.Exclamation, "TECSERV")
    Exit Sub
  End If
  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\total_tickets_atendidos_operador.xls")
  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con

  cmd.CommandText = "SELECT COUNT (Atendio)as SumaTotal FROM T_Tickets WHERE Atendio = '" & UCase(Trim(txtParametro1.Text)) & "' "
  con.Open()
  Reader = cmd.ExecuteReader

  While (Reader.Read())
    objExcel.Cells(x, 5).Formula = Reader.Item("SumaTotal").ToString
  End While
  Reader.Close()

  cmd.CommandText = "SELECT * FROM T_Operadores WHERE logNomUsuario = '" & UCase(Trim(txtParametro1.Text)) & "' "
  Reader = cmd.ExecuteReader
  While (Reader.Read())
    objExcel.Cells(7, 1).formula = Reader.GetInt32(0)
    objExcel.Cells(7, 2).formula = Reader.GetString(2) & "  " & Reader.GetString(1)
    objExcel.Cells(7, 3).formula = Reader.GetString(6)
    objExcel.Cells(7, 4).formula = Reader.GetString(7)
    objExcel.Cells(7, 5).formula = Reader.GetString(3)
  End While

End Sub

Private Sub total_tickets_resueltos_tecnico()
Dim objExcel As Excel.Application
  x = 3
  If txtParametro1.Text = "" Then
    MsgBox("Ingrese un valor en el parametro, por favor !!", MsgBoxStyle.Exclamation, "TECSERV")
    Exit Sub
  End If
  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\total_tickets_resueltos_tecnico.xls")
  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con

  cmd.CommandText = "SELECT COUNT (Atendio)as SumaTotal FROM T_Tickets INNER JOIN T_Tecnicos ON T_Tecnicos.Id_Tecnico = T_Tickets.Id_tecnico WHERE T_Tecnicos.TecApellido = '" & UCase(Trim(txtParametro1.Text)) & "' AND T_Tickets.Cobranza = '" & "S" & "' "
  con.Open()
  Reader = cmd.ExecuteReader

  While (Reader.Read())
    objExcel.Cells(x, 5).Formula = Reader.Item("SumaTotal").ToString
  End While
  Reader.Close()
  cmd.CommandText = "SELECT * FROM T_Tecnicos WHERE TecApellido = '" & UCase(Trim(txtParametro1.Text)) & "' "
  Reader = cmd.ExecuteReader
  While (Reader.Read())
    objExcel.Cells(7, 1).formula = Reader.GetInt32(0)
    objExcel.Cells(7, 2).formula = Reader.GetString(2) & " " & Reader.GetString(1)
    objExcel.Cells(7, 3).formula = Reader.GetString(3)
    objExcel.Cells(7, 4).formula = Reader.GetString(4)
    objExcel.Cells(7, 5).formula = Reader.GetString(5)
  End While
End Sub

Private Sub ticket_monto_promedio()
Dim objExcel As Excel.Application
  x = 3

  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\ticket_monto_promedio.xls")
  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con

  cmd.CommandText = "SELECT AVG (Monto)as SumaTotal FROM T_Tickets"
  con.Open()
  Reader = cmd.ExecuteReader

  While (Reader.Read())
    objExcel.Cells(x, 5).Formula = Reader.Item("SumaTotal").ToString
  End While
  Reader.Close() 
End Sub

Private Sub Total_Facturado_Empresa_Mes()
Dim objExcel As Excel.Application
Dim TotalMes As Integer
Dim a As String
Dim fecha, fecha1, fecha2 As String
  x = 3
  TotalMes = 0
  If txtParametro1.Text = "" Then
    MsgBox("Ingrese un valor en el parametro, por favor !!", MsgBoxStyle.Exclamation, "TECSERV")
    Exit Sub
  End If
  objExcel = New Excel.Application
  objExcel.Visible = True
  objExcel.Workbooks.Open("E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\Total_Facturado_Empresa_Mes.xls")
  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con

fecha1 = Microsoft.VisualBasic.Left(DateTimePicker1.Value, 11)
fecha2 = Microsoft.VisualBasic.Left(DateTimePicker2.Value, 11)

'  cmd.CommandText = "SELECT * FROM T_Tickets WHERE NomCliente = '" & UCase(Trim(txtParametro1.Text)) & "' AND Cobranza = '" & "S" & "' "
cmd.CommandText = "SELECT * FROM T_Tickets WHERE NomCliente = '" & UCase(Trim(txtParametro1.Text)) & "' "
  con.Open()
  Reader = cmd.ExecuteReader

  While (Reader.Read())
  'If Reader.GetString(9) Then
    fecha = Reader.GetDateTime(9)
    If Mid(fecha, 4, 2) = Mid(fecha1, 4, 2) Then
      If Mid(fecha, 1, 2) >= Mid(fecha1, 1, 2) Then
        If Mid(fecha, 1, 2) <= Mid(fecha2, 1, 2) Then
          TotalMes = TotalMes + Reader.GetInt32(8)
        End If
      End If
    End If
  End While
  objExcel.Cells(x, 5).Formula = TotalMes & "= pesos "

  Reader.Close()

  cmd.CommandText = "SELECT * FROM T_Clientes WHERE cliNombre = '" & UCase(Trim(txtParametro1.Text)) & "' "
  Reader = cmd.ExecuteReader
  While (Reader.Read())
    objExcel.Cells(7, 1).formula = Reader.GetInt32(0)
    objExcel.Cells(7, 2).formula = Reader.GetString(2)
    objExcel.Cells(7, 3).formula = Reader.GetString(3)
    objExcel.Cells(7, 4).formula = Reader.GetString(5)
    objExcel.Cells(7, 5).formula = Reader.GetString(4)
  End While
End Sub

Private Sub lbListados_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbListados.SelectedIndexChanged
  OptList = lbListados.SelectedIndex
  GroupBox2.Enabled = False
End Sub

Private Sub lbReportes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbReportes.SelectedIndexChanged
  OptList = lbReportes.SelectedIndex + 6
  GroupBox2.Enabled = True

  Select Case OptList
    Case 6
      Label1.Text = "Cantidad"
      txtParametro1.Visible = True
      DateTimePicker1.Enabled = False
      DateTimePicker2.Enabled = False
    Case 7
      Label1.Text = "Cantidad"
      txtParametro1.Visible = True
      DateTimePicker1.Enabled = False
      DateTimePicker2.Enabled = False
    Case 8
      Label1.Text = "Sin Parametros"
      txtParametro1.Visible = False
      DateTimePicker1.Enabled = False
      DateTimePicker2.Enabled = False
    Case 9
      Label1.Text = "Apellido"
      txtParametro1.Visible = True
      DateTimePicker1.Enabled = False
      DateTimePicker2.Enabled = False
    Case 10
      Label1.Text = "Razon Social"
      txtParametro1.Visible = True
      DateTimePicker1.Enabled = False
      DateTimePicker2.Enabled = False
    Case 11
      Label1.Text = "Razon Social"
      txtParametro1.Visible = True
      DateTimePicker1.Enabled = True
      DateTimePicker2.Enabled = True
    Case 12
      Label1.Text = "Sin Parametros"
      txtParametro1.Visible = False
      DateTimePicker1.Enabled = False
      DateTimePicker2.Enabled = False
    Case 13
      Label1.Text = "Nombre Usr"
      txtParametro1.Visible = True
      DateTimePicker1.Enabled = False
      DateTimePicker2.Enabled = False
    Case 14
      Label1.Text = "Apellido"
      txtParametro1.Visible = True
      DateTimePicker1.Enabled = False
      DateTimePicker2.Enabled = False
    Case 15
      Label1.Text = "Sin Parametros"
      txtParametro1.Visible = False
      DateTimePicker1.Enabled = False
      DateTimePicker2.Enabled = False
  End Select
End Sub

Private Sub frmListados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

End Sub
End Class