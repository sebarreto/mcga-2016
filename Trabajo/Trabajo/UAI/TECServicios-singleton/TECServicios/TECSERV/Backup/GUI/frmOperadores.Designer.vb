<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOperadores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOperadores))
Me.GroupBox2 = New System.Windows.Forms.GroupBox
Me.Label7 = New System.Windows.Forms.Label
Me.txtOpContraseña = New System.Windows.Forms.TextBox
Me.Label9 = New System.Windows.Forms.Label
Me.txtOpNombreUsuario = New System.Windows.Forms.TextBox
Me.Label5 = New System.Windows.Forms.Label
Me.cmbOpGrupos = New System.Windows.Forms.ComboBox
Me.Label4 = New System.Windows.Forms.Label
Me.txtOpTelefono = New System.Windows.Forms.TextBox
Me.Label1 = New System.Windows.Forms.Label
Me.txtOpNombre = New System.Windows.Forms.TextBox
Me.Label8 = New System.Windows.Forms.Label
Me.txtOpApellido = New System.Windows.Forms.TextBox
Me.txtOpCodigo = New System.Windows.Forms.TextBox
Me.Label6 = New System.Windows.Forms.Label
Me.lblVARSEG = New System.Windows.Forms.Label
Me.gbPrin = New System.Windows.Forms.GroupBox
Me.lvwOperadores = New System.Windows.Forms.ListView
Me.Nro = New System.Windows.Forms.ColumnHeader
Me.Nombre = New System.Windows.Forms.ColumnHeader
Me.Domicilio = New System.Windows.Forms.ColumnHeader
Me.btnGuardar = New System.Windows.Forms.Button
Me.btnEliminar = New System.Windows.Forms.Button
Me.btnModificar = New System.Windows.Forms.Button
Me.btnAgregar = New System.Windows.Forms.Button
Me.GroupBox2.SuspendLayout()
Me.gbPrin.SuspendLayout()
Me.SuspendLayout()
'
'GroupBox2
'
Me.GroupBox2.Controls.Add(Me.Label7)
Me.GroupBox2.Controls.Add(Me.txtOpContraseña)
Me.GroupBox2.Controls.Add(Me.Label9)
Me.GroupBox2.Controls.Add(Me.txtOpNombreUsuario)
Me.GroupBox2.Controls.Add(Me.Label5)
Me.GroupBox2.Controls.Add(Me.cmbOpGrupos)
Me.GroupBox2.Controls.Add(Me.Label4)
Me.GroupBox2.Controls.Add(Me.txtOpTelefono)
Me.GroupBox2.Controls.Add(Me.Label1)
Me.GroupBox2.Controls.Add(Me.txtOpNombre)
Me.GroupBox2.Controls.Add(Me.Label8)
Me.GroupBox2.Controls.Add(Me.txtOpApellido)
Me.GroupBox2.Controls.Add(Me.txtOpCodigo)
Me.GroupBox2.Controls.Add(Me.Label6)
Me.GroupBox2.Controls.Add(Me.lblVARSEG)
Me.GroupBox2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.GroupBox2.ForeColor = System.Drawing.Color.Black
Me.GroupBox2.Location = New System.Drawing.Point(12, 212)
Me.GroupBox2.Name = "GroupBox2"
Me.GroupBox2.Size = New System.Drawing.Size(374, 211)
Me.GroupBox2.TabIndex = 20
Me.GroupBox2.TabStop = False
Me.GroupBox2.Text = "Datos del Operadores"
'
'Label7
'
Me.Label7.AutoSize = True
Me.Label7.ForeColor = System.Drawing.Color.Black
Me.Label7.Location = New System.Drawing.Point(31, 154)
Me.Label7.Name = "Label7"
Me.Label7.Size = New System.Drawing.Size(71, 14)
Me.Label7.TabIndex = 55
Me.Label7.Text = "Contraseña"
'
'txtOpContraseña
'
Me.txtOpContraseña.Location = New System.Drawing.Point(108, 151)
Me.txtOpContraseña.Name = "txtOpContraseña"
Me.txtOpContraseña.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
Me.txtOpContraseña.Size = New System.Drawing.Size(253, 20)
Me.txtOpContraseña.TabIndex = 4
'
'Label9
'
Me.Label9.AutoSize = True
Me.Label9.ForeColor = System.Drawing.Color.Black
Me.Label9.Location = New System.Drawing.Point(6, 128)
Me.Label9.Name = "Label9"
Me.Label9.Size = New System.Drawing.Size(96, 14)
Me.Label9.TabIndex = 53
Me.Label9.Text = "Nombre usuario"
'
'txtOpNombreUsuario
'
Me.txtOpNombreUsuario.Location = New System.Drawing.Point(108, 125)
Me.txtOpNombreUsuario.Name = "txtOpNombreUsuario"
Me.txtOpNombreUsuario.Size = New System.Drawing.Size(253, 20)
Me.txtOpNombreUsuario.TabIndex = 3
'
'Label5
'
Me.Label5.AutoSize = True
Me.Label5.ForeColor = System.Drawing.Color.Black
Me.Label5.Location = New System.Drawing.Point(61, 180)
Me.Label5.Name = "Label5"
Me.Label5.Size = New System.Drawing.Size(41, 14)
Me.Label5.TabIndex = 51
Me.Label5.Text = "Grupo"
'
'cmbOpGrupos
'
Me.cmbOpGrupos.FormattingEnabled = True
Me.cmbOpGrupos.Location = New System.Drawing.Point(108, 177)
Me.cmbOpGrupos.Name = "cmbOpGrupos"
Me.cmbOpGrupos.Size = New System.Drawing.Size(253, 22)
Me.cmbOpGrupos.TabIndex = 5
'
'Label4
'
Me.Label4.AutoSize = True
Me.Label4.ForeColor = System.Drawing.Color.Black
Me.Label4.Location = New System.Drawing.Point(46, 102)
Me.Label4.Name = "Label4"
Me.Label4.Size = New System.Drawing.Size(56, 14)
Me.Label4.TabIndex = 49
Me.Label4.Text = "Telefono"
'
'txtOpTelefono
'
Me.txtOpTelefono.Location = New System.Drawing.Point(108, 99)
Me.txtOpTelefono.Name = "txtOpTelefono"
Me.txtOpTelefono.Size = New System.Drawing.Size(253, 20)
Me.txtOpTelefono.TabIndex = 2
'
'Label1
'
Me.Label1.AutoSize = True
Me.Label1.ForeColor = System.Drawing.Color.Black
Me.Label1.Location = New System.Drawing.Point(51, 77)
Me.Label1.Name = "Label1"
Me.Label1.Size = New System.Drawing.Size(51, 14)
Me.Label1.TabIndex = 43
Me.Label1.Text = "Nombre"
'
'txtOpNombre
'
Me.txtOpNombre.Location = New System.Drawing.Point(108, 74)
Me.txtOpNombre.Name = "txtOpNombre"
Me.txtOpNombre.Size = New System.Drawing.Size(253, 20)
Me.txtOpNombre.TabIndex = 1
'
'Label8
'
Me.Label8.AutoSize = True
Me.Label8.ForeColor = System.Drawing.Color.Black
Me.Label8.Location = New System.Drawing.Point(50, 51)
Me.Label8.Name = "Label8"
Me.Label8.Size = New System.Drawing.Size(52, 14)
Me.Label8.TabIndex = 40
Me.Label8.Text = "Apellido"
'
'txtOpApellido
'
Me.txtOpApellido.Location = New System.Drawing.Point(108, 48)
Me.txtOpApellido.Name = "txtOpApellido"
Me.txtOpApellido.Size = New System.Drawing.Size(253, 20)
Me.txtOpApellido.TabIndex = 0
'
'txtOpCodigo
'
Me.txtOpCodigo.Enabled = False
Me.txtOpCodigo.Location = New System.Drawing.Point(108, 22)
Me.txtOpCodigo.Name = "txtOpCodigo"
Me.txtOpCodigo.Size = New System.Drawing.Size(117, 20)
Me.txtOpCodigo.TabIndex = 35
'
'Label6
'
Me.Label6.AutoSize = True
Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
Me.Label6.Location = New System.Drawing.Point(56, 25)
Me.Label6.Name = "Label6"
Me.Label6.Size = New System.Drawing.Size(46, 14)
Me.Label6.TabIndex = 36
Me.Label6.Text = "Codigo"
'
'lblVARSEG
'
Me.lblVARSEG.AutoSize = True
Me.lblVARSEG.ForeColor = System.Drawing.Color.Red
Me.lblVARSEG.Location = New System.Drawing.Point(176, 22)
Me.lblVARSEG.Name = "lblVARSEG"
Me.lblVARSEG.Size = New System.Drawing.Size(49, 14)
Me.lblVARSEG.TabIndex = 58
Me.lblVARSEG.Text = "CODIGO"
Me.lblVARSEG.Visible = False
'
'gbPrin
'
Me.gbPrin.Controls.Add(Me.lvwOperadores)
Me.gbPrin.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.gbPrin.Location = New System.Drawing.Point(12, 12)
Me.gbPrin.Name = "gbPrin"
Me.gbPrin.Size = New System.Drawing.Size(374, 194)
Me.gbPrin.TabIndex = 19
Me.gbPrin.TabStop = False
Me.gbPrin.Text = "Listado de Operadores"
'
'lvwOperadores
'
Me.lvwOperadores.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Nro, Me.Nombre, Me.Domicilio})
Me.lvwOperadores.FullRowSelect = True
Me.lvwOperadores.GridLines = True
Me.lvwOperadores.Location = New System.Drawing.Point(6, 19)
Me.lvwOperadores.MultiSelect = False
Me.lvwOperadores.Name = "lvwOperadores"
Me.lvwOperadores.Size = New System.Drawing.Size(355, 169)
Me.lvwOperadores.TabIndex = 0
Me.lvwOperadores.UseCompatibleStateImageBehavior = False
Me.lvwOperadores.View = System.Windows.Forms.View.Details
'
'Nro
'
Me.Nro.Text = "Nro"
Me.Nro.Width = 50
'
'Nombre
'
Me.Nombre.Text = "Apellido"
Me.Nombre.Width = 160
'
'Domicilio
'
Me.Domicilio.Text = "Nivel"
Me.Domicilio.Width = 120
'
'btnGuardar
'
Me.btnGuardar.Enabled = False
Me.btnGuardar.Image = CType(resources.GetObject("btnGuardar.Image"), System.Drawing.Image)
Me.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
Me.btnGuardar.Location = New System.Drawing.Point(203, 438)
Me.btnGuardar.Name = "btnGuardar"
Me.btnGuardar.Size = New System.Drawing.Size(57, 53)
Me.btnGuardar.TabIndex = 3
Me.btnGuardar.Text = "Guardar"
Me.btnGuardar.UseVisualStyleBackColor = True
'
'btnEliminar
'
Me.btnEliminar.Image = CType(resources.GetObject("btnEliminar.Image"), System.Drawing.Image)
Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
Me.btnEliminar.Location = New System.Drawing.Point(140, 438)
Me.btnEliminar.Name = "btnEliminar"
Me.btnEliminar.Size = New System.Drawing.Size(57, 53)
Me.btnEliminar.TabIndex = 2
Me.btnEliminar.Text = "Eliminar"
Me.btnEliminar.UseVisualStyleBackColor = True
'
'btnModificar
'
Me.btnModificar.Image = CType(resources.GetObject("btnModificar.Image"), System.Drawing.Image)
Me.btnModificar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
Me.btnModificar.Location = New System.Drawing.Point(77, 438)
Me.btnModificar.Name = "btnModificar"
Me.btnModificar.Size = New System.Drawing.Size(57, 53)
Me.btnModificar.TabIndex = 1
Me.btnModificar.Text = "Modifica"
Me.btnModificar.UseVisualStyleBackColor = True
'
'btnAgregar
'
Me.btnAgregar.Image = CType(resources.GetObject("btnAgregar.Image"), System.Drawing.Image)
Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
Me.btnAgregar.Location = New System.Drawing.Point(14, 438)
Me.btnAgregar.Name = "btnAgregar"
Me.btnAgregar.Size = New System.Drawing.Size(57, 53)
Me.btnAgregar.TabIndex = 0
Me.btnAgregar.Text = "Agregar"
Me.btnAgregar.UseVisualStyleBackColor = True
'
'frmOperadores
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.ClientSize = New System.Drawing.Size(403, 504)
Me.Controls.Add(Me.btnGuardar)
Me.Controls.Add(Me.btnEliminar)
Me.Controls.Add(Me.btnModificar)
Me.Controls.Add(Me.btnAgregar)
Me.Controls.Add(Me.GroupBox2)
Me.Controls.Add(Me.gbPrin)
Me.Name = "frmOperadores"
Me.Text = "Operadores del sistema"
Me.GroupBox2.ResumeLayout(False)
Me.GroupBox2.PerformLayout()
Me.gbPrin.ResumeLayout(False)
Me.ResumeLayout(False)

End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtOpApellido As System.Windows.Forms.TextBox
    Friend WithEvents txtOpCodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents gbPrin As System.Windows.Forms.GroupBox
    Friend WithEvents lvwOperadores As System.Windows.Forms.ListView
    Friend WithEvents Nro As System.Windows.Forms.ColumnHeader
    Friend WithEvents Nombre As System.Windows.Forms.ColumnHeader
    Friend WithEvents Domicilio As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtOpContraseña As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtOpNombreUsuario As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbOpGrupos As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtOpTelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtOpNombre As System.Windows.Forms.TextBox
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents lblVARSEG As System.Windows.Forms.Label
End Class
