Imports System.Data.OleDb

Public Class AOPERADOR
    Private Conexion As New OleDbConnection()
    Private oDataAdapter As OleDbDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.ConectarAud
        oDataAdapter = New OleDbDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As OleDbCommand
        oSComando = New OleDbCommand("SELECT * FROM T_AuditoriaOperadores", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As OleDbCommand
        oIComando = New OleDbCommand("INSERT INTO T_AuditoriaOperadores (logApellido,logNombre,logTelefono,logNomUsuario,logClave,logGrupo,logPerfil,audFecha,audHora,audTipo,audUsuario) VALUES (@logApellido,@logNombre,@logTelefono,@logNomUsuario,@logClave,@logGrupo,@logPerfil,@audFecha,@audHora,@audTipo,@audUsuario)", Conexion)
        oIComando.Parameters.Add("@logApellido", OleDbType.VarChar, 20, "logApellido")
        oIComando.Parameters.Add("@logNombre", OleDbType.VarChar, 20, "logNombre")
        oIComando.Parameters.Add("@logTelefono", OleDbType.VarChar, 20, "logTelefono")
        oIComando.Parameters.Add("@logNomUsuario", OleDbType.VarChar, 20, "logNomUsuario")
        oIComando.Parameters.Add("@logClave", OleDbType.VarChar, 50, "logClave")
        oIComando.Parameters.Add("@logGrupo", OleDbType.VarChar, 20, "logGrupo")
        oIComando.Parameters.Add("@logPerfil", OleDbType.VarChar, 20, "logPerfil")
        oIComando.Parameters.Add("@audFecha", OleDbType.VarChar, 20, "audFecha")
        oIComando.Parameters.Add("@audHora", OleDbType.VarChar, 20, "audHora")
        oIComando.Parameters.Add("@audTipo", OleDbType.VarChar, 20, "audTipo")
        oIComando.Parameters.Add("@audUsuario", OleDbType.VarChar, 20, "audUsuario")

        oIComando.Parameters.Add("@Id_Login", OleDbType.Integer, 10, "Id_Login")

        oDataAdapter.InsertCommand = oIComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtTECNICOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtTECNICOS)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub

End Class
