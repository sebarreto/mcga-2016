Imports System.Data.OleDb

Public Class FORMULARIO

    Private Conexion As New OleDbConnection()
    Private oDataAdapter As OleDbDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New OleDbDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As OleDbCommand
        oSComando = New OleDbCommand("SELECT * FROM T_Formularios ORDER BY Id_Formulario", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As OleDbCommand
        oIComando = New OleDbCommand("INSERT INTO T_Formularios(frmNombre,frmDescripcion) VALUES (@frmNombre,frmDescripcion)", Conexion)
        oIComando.Parameters.Add("@frmNombre", OleDbType.VarChar, 20, "frmNombre")
        oIComando.Parameters.Add("@frmDescripcion", OleDbType.VarChar, 200, "frmDescripcion")

        oIComando.Parameters.Add("@Id_Formulario", OleDbType.Integer, 10, "Id_Formulario")
        oDataAdapter.InsertCommand = oIComando

        'UPDATE COMMAND
        Dim oUComando As OleDbCommand
        oUComando = New OleDbCommand("UPDATE T_Formularios SET  frmNombre = @frmNombre, frmDescripcion = @frmDescripcion  WHERE Id_Formulario = @Id_Formulario ", Conexion)
        oUComando.Parameters.Add("@frmNombre", OleDbType.VarChar, 20, "frmNombre")
        oUComando.Parameters.Add("@frmDescripcion", OleDbType.VarChar, 200, "frmDescripcion")

        oUComando.Parameters.Add("@Id_Formulario", OleDbType.Integer, 10, "Id_Formulario")
        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As OleDbCommand
        oDComando = New OleDbCommand("DELETE * FROM T_Formularios  WHERE Id_Formulario = @Id_Formulario", Conexion)
        oDComando.Parameters.Add("@Id_Formulario", OleDbType.Integer, 10, "Id_Formulario")
        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtFORMULARIOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtFORMULARIOS)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class

