Imports System.Data.OleDb

Public Class APERFIL
  Private Conexion As New OleDbConnection()
    Private oDataAdapter As OleDbDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.ConectarAud
        oDataAdapter = New OleDbDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As OleDbCommand
        oSComando = New OleDbCommand("SELECT * FROM T_AuditoriaPerfiles", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As OleDbCommand
        oIComando = New OleDbCommand("INSERT INTO T_AuditoriaPerfiles (audpeNombre,audpePermisos,audpeGrupo,audFecha,audHora,audTipo,audUsuario) VALUES (@audpeNombre,@audpePermisos,@audpeGrupo,@audFecha,@audHora,@audTipo,@audUsuario)", Conexion)
        oIComando.Parameters.Add("@audpeNombre", OleDbType.VarChar, 20, "audpeNombre")
        oIComando.Parameters.Add("@audpePermisos", OleDbType.VarChar, 50, "audpePermisos")
        oIComando.Parameters.Add("@audpeGrupo", OleDbType.VarChar, 20, "audpeGrupo")
        oIComando.Parameters.Add("@audFecha", OleDbType.VarChar, 20, "audFecha")
        oIComando.Parameters.Add("@audHora", OleDbType.VarChar, 20, "audHora")
        oIComando.Parameters.Add("@audTipo", OleDbType.VarChar, 20, "audTipo")
        oIComando.Parameters.Add("@audUsuario", OleDbType.VarChar, 20, "audUsuario")

        oIComando.Parameters.Add("@Id_AudPerfil", OleDbType.Integer, 10, "Id_AudPerfil")

        oDataAdapter.InsertCommand = oIComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtTECNICOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtTECNICOS)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class

