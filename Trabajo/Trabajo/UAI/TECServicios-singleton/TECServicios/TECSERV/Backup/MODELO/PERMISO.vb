Imports System.Data.OleDb

Public Class PERMISO

    Private Conexion As New OleDbConnection()
    Private oDataAdapter As OleDbDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New OleDbDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As OleDbCommand
        oSComando = New OleDbCommand("SELECT * FROM T_Permisos", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As OleDbCommand
        oIComando = New OleDbCommand("INSERT INTO T_Permisos(perNombre,perDescripcion) VALUES (@perNombre,@perDescripcion)", Conexion)
        oIComando.Parameters.Add("@perNombre", OleDbType.VarChar, 20, "perNombre")
        oIComando.Parameters.Add("@perDescripcion", OleDbType.VarChar, 200, "perDescripcion")

        oIComando.Parameters.Add("@Id_Permiso", OleDbType.Integer, 10, "Id_Permiso")
        oDataAdapter.InsertCommand = oIComando

        'UPDATE COMMAND
        Dim oUComando As OleDbCommand
        oUComando = New OleDbCommand("UPDATE T_Permisos SET  perNombre = @perNombre,perDescripcion = @perDescripcion   WHERE Id_Permiso = @Id_Permiso ", Conexion)
        oUComando.Parameters.Add("@perNombre", OleDbType.VarChar, 20, "perNombre")
        oUComando.Parameters.Add("@perDescripcion", OleDbType.VarChar, 20, "perDescripcion")

        oUComando.Parameters.Add("@Id_Permiso", OleDbType.Integer, 10, "Id_Permiso")
        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As OleDbCommand
        oDComando = New OleDbCommand("DELETE * FROM T_Permisos WHERE Id_Permiso = @Id_Permiso", Conexion)
        oDComando.Parameters.Add("@Id_Permiso", OleDbType.Integer, 10, "Id_Permiso")
        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtPERMISOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtPERMISOS)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class

