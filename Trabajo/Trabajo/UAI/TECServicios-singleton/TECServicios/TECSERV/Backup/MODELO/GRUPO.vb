Imports System.Data.OleDb

Public Class GRUPO

    Private Conexion As New OleDbConnection()
    Private oDataAdapter As OleDbDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New OleDbDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As OleDbCommand
        oSComando = New OleDbCommand("SELECT * FROM T_Grupos", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As OleDbCommand
        oIComando = New OleDbCommand("INSERT INTO T_Grupos(GrNombre,GrDescripcion) VALUES (@GrNombre,@GrDescripcion)", Conexion)
        oIComando.Parameters.Add("@GrNombre", OleDbType.VarChar, 20, "GrNombre")
        oIComando.Parameters.Add("@GrDescripcion", OleDbType.VarChar, 200, "GrDescripcion")

        oIComando.Parameters.Add("@Id_Grupo", OleDbType.Integer, 10, "Id_Grupo")
        oDataAdapter.InsertCommand = oIComando

        'UPDATE COMMAND
        Dim oUComando As OleDbCommand
        oUComando = New OleDbCommand("UPDATE T_Grupos SET  GrNombre = @GrNombre ,GrDescripcion = @GrDescripcion WHERE Id_Grupo = @Id_Grupo ", Conexion)
        oUComando.Parameters.Add("@GrNombre", OleDbType.VarChar, 20, "GrNombre")
        oUComando.Parameters.Add("@GrDescripcion", OleDbType.VarChar, 200, "GrDescripcion")

        oUComando.Parameters.Add("@Id_Grupo", OleDbType.Integer, 10, "Id_Grupo")
        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As OleDbCommand
        oDComando = New OleDbCommand("DELETE * FROM T_Grupos  WHERE Id_Grupo = @Id_Grupo", Conexion)
        oDComando.Parameters.Add("@Id_Grupo", OleDbType.Integer, 10, "Id_Grupo")
        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtCLIENTES As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtCLIENTES)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class

