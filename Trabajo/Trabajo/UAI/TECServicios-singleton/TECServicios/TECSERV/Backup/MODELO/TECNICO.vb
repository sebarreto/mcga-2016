Imports System.Data.OleDb

Public Class TECNICO
  Private Conexion As New OleDbConnection()
    Private oDataAdapter As OleDbDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New OleDbDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As OleDbCommand
        oSComando = New OleDbCommand("SELECT * FROM T_Tecnicos", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As OleDbCommand
        oIComando = New OleDbCommand("INSERT INTO T_Tecnicos (tecNombre,tecApellido,tecDomicilio,tecLocalidad,tecTelefono) VALUES (@tecNombre,@tecApellido,@tecDomicilio,@tecLocalidad,@tecTelefono)", Conexion)
        oIComando.Parameters.Add("@tecNombre", OleDbType.VarChar, 20, "tecNombre")
        oIComando.Parameters.Add("@tecApellido", OleDbType.VarChar, 20, "tecApellido")
        oIComando.Parameters.Add("@tecDomicilio", OleDbType.VarChar, 20, "tecDomicilio")
        oIComando.Parameters.Add("@tecLocalidad", OleDbType.VarChar, 20, "tecLocalidad")
        oIComando.Parameters.Add("@tecTelefono", OleDbType.VarChar, 20, "tecTelefono")


        oIComando.Parameters.Add("@Id_Tecnico", OleDbType.Integer, 10, "Id_Tecnico")

        oDataAdapter.InsertCommand = oIComando

        'UPDATE COMMAND
        Dim oUComando As OleDbCommand
        oUComando = New OleDbCommand("UPDATE T_Tecnicos SET  tecNombre = @tecNombre, tecApellido = @tecApellido, tecDomicilio=@tecDomicilio, tecLocalidad=@tecLocalidad, tecTelefono=@tecTelefono WHERE Id_Tecnico = @Id_Tecnico ", Conexion)
        oUComando.Parameters.Add("@tecNombre", OleDbType.VarChar, 20, "tecNombre")
        oUComando.Parameters.Add("@tecApellido", OleDbType.VarChar, 20, "tecApellido")
        oUComando.Parameters.Add("@tecDomicilio", OleDbType.VarChar, 20, "tecDomicilio")
        oUComando.Parameters.Add("@tecLocalidad", OleDbType.VarChar, 20, "tecLocalidad")
        oUComando.Parameters.Add("@tecTelefono", OleDbType.VarChar, 20, "tecTelefono")

        oUComando.Parameters.Add("@Id_Tecnico", OleDbType.Integer, 10, "Id_Tecnico")

        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As OleDbCommand
        oDComando = New OleDbCommand("DELETE * FROM T_Tecnicos  WHERE Id_Tecnico = @Id_Tecnico", Conexion)
        oDComando.Parameters.Add("@Id_Tecnico", OleDbType.Integer, 10, "Id_Tecnico")

        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtTECNICOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtTECNICOS)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class

