Imports System.Data.OleDb

Public Class ACLIENTES
    Private Conexion As New OleDbConnection()
    Private oDataAdapter As OleDbDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.ConectarAud
        oDataAdapter = New OleDbDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As OleDbCommand
        oSComando = New OleDbCommand("SELECT * FROM T_AuditoriaClientes", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As OleDbCommand

        oIComando = New OleDbCommand("INSERT INTO T_AuditoriaClientes (cliCodigo, cliNombre, cliDomicilio,cliTelefono,cliLocalidad,cliHorario,cliFecha,cliHora,cliTipo,Usuario) VALUES (@cliCodigo,@cliNombre,@cliDomicilio,@cliTelefono,@cliLocalidad,@cliHorario,@cliFecha,@cliHora,@cliTipo,@Usuario)", Conexion)
        oIComando.Parameters.Add("@cliCodigo", OleDbType.VarChar, 20, "cliCodigo")
        oIComando.Parameters.Add("@cliNombre", OleDbType.VarChar, 20, "cliNombre")
        oIComando.Parameters.Add("@cliDomicilio", OleDbType.VarChar, 20, "cliDomicilio")
        oIComando.Parameters.Add("@cliTelefono", OleDbType.VarChar, 20, "cliTelefono")
        oIComando.Parameters.Add("@cliLocalidad", OleDbType.VarChar, 20, "cliLocalidad")
        oIComando.Parameters.Add("@cliHorario", OleDbType.VarChar, 20, "cliHorario")
        oIComando.Parameters.Add("@cliFecha", OleDbType.VarChar, 20, "cliFecha")
        oIComando.Parameters.Add("@cliHora", OleDbType.VarChar, 20, "cliHora")
        oIComando.Parameters.Add("@cliTipo", OleDbType.VarChar, 20, "cliTipo")
        oIComando.Parameters.Add("@Usuario", OleDbType.VarChar, 20, "Usuario")


        oIComando.Parameters.Add("@Id_Cliente", OleDbType.Integer, 10, "Id_Cliente")
        oDataAdapter.InsertCommand = oIComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtACLIENTES As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.ConectarAud
        Try
            oDataAdapter.Update(dtACLIENTES)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class