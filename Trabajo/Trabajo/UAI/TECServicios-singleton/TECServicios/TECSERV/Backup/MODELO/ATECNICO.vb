Imports System.Data.OleDb

Public Class ATECNICO
  Private Conexion As New OleDbConnection()
    Private oDataAdapter As OleDbDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.ConectarAud
        oDataAdapter = New OleDbDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As OleDbCommand
        oSComando = New OleDbCommand("SELECT * FROM T_AuditoriaTecnicos", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As OleDbCommand
        oIComando = New OleDbCommand("INSERT INTO T_AuditoriaTecnicos (tecNombre,tecApellido,tecDomicilio,tecLocalidad,tecTelefono,audTipo,audFecha,audHora,audUsuario) VALUES (@tecNombre,@tecApellido,@tecDomicilio,@tecLocalidad,@tecTelefono,@audTipo,@audFecha,@audHora,@audUsuario)", Conexion)
        oIComando.Parameters.Add("@tecNombre", OleDbType.VarChar, 20, "tecNombre")
        oIComando.Parameters.Add("@tecApellido", OleDbType.VarChar, 20, "tecApellido")
        oIComando.Parameters.Add("@tecDomicilio", OleDbType.VarChar, 20, "tecDomicilio")
        oIComando.Parameters.Add("@tecLocalidad", OleDbType.VarChar, 20, "tecLocalidad")
        oIComando.Parameters.Add("@tecTelefono", OleDbType.VarChar, 20, "tecTelefono")
        oIComando.Parameters.Add("@audTipo", OleDbType.VarChar, 20, "audTipo")
        oIComando.Parameters.Add("@audFecha", OleDbType.VarChar, 20, "audFecha")
        oIComando.Parameters.Add("@audHora", OleDbType.VarChar, 20, "audHora")
        oIComando.Parameters.Add("@audUsuario", OleDbType.VarChar, 20, "audUsuario")

        oIComando.Parameters.Add("@Id_Tecnico", OleDbType.Integer, 10, "Id_Tecnico")

        oDataAdapter.InsertCommand = oIComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtTECNICOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtTECNICOS)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class

