Imports System.Data.OleDb

Public Class CLIENTES
    Private Conexion As New OleDbConnection()
    Private oDataAdapter As OleDbDataAdapter
    Private oDataTable As DataTable

    Private con As OleDbConnection
    Private cmd As OleDbCommand
    Private Reader As OleDb.OleDbDataReader

    Public Shared Crear_Cliente As CLIENTES

    Public Shared Function Crear() As CLIENTES
        If Crear_Cliente Is Nothing Then
            Crear_Cliente = New CLIENTES
        Else
         '   MsgBox("El objeto ya existe")
        End If

        Return Crear_Cliente
    End Function

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New OleDbDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As OleDbCommand
        oSComando = New OleDbCommand("SELECT * FROM T_Clientes ORDER BY cliNombre", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As OleDbCommand
        oIComando = New OleDbCommand("INSERT INTO T_Clientes(cliCodigo, cliNombre, cliDomicilio,cliTelefono,cliLocalidad,cliHorario) VALUES (@cliCodigo,@cliNombre,@cliDomicilio,@cliTelefono,@cliLocalidad,@cliHorario)", Conexion)
        oIComando.Parameters.Add("@cliCodigo", OleDbType.VarChar, 20, "cliCodigo")
        oIComando.Parameters.Add("@cliNombre", OleDbType.VarChar, 20, "cliNombre")
        oIComando.Parameters.Add("@cliDomicilio", OleDbType.VarChar, 20, "cliDomicilio")
        oIComando.Parameters.Add("@cliTelefono", OleDbType.VarChar, 20, "cliTelefono")
        oIComando.Parameters.Add("@cliLocalidad", OleDbType.VarChar, 20, "cliLocalidad")
        oIComando.Parameters.Add("@cliHorario", OleDbType.VarChar, 20, "cliHorario")

        oIComando.Parameters.Add("@Id_Cliente", OleDbType.Integer, 10, "Id_Cliente")
        oDataAdapter.InsertCommand = oIComando

        'UPDATE COMMAND
        Dim oUComando As OleDbCommand
        oUComando = New OleDbCommand("UPDATE T_Clientes SET  cliCodigo=@cliCodigo ,cliNombre = @cliNombre, cliDomicilio = @cliDomicilio, cliTelefono = @cliTelefono, cliLocalidad =@cliLocalidad,cliHorario=@cliHorario WHERE Id_Cliente = @Id_Cliente ", Conexion)
        oUComando.Parameters.Add("@cliCodigo", OleDbType.VarChar, 20, "cliCodigo")
        oUComando.Parameters.Add("@cliNombre", OleDbType.VarChar, 20, "cliNombre")
        oUComando.Parameters.Add("@cliDomicilio", OleDbType.VarChar, 20, "cliDomicilio")
        oUComando.Parameters.Add("@cliTelefono", OleDbType.VarChar, 20, "cliTelefono")
        oUComando.Parameters.Add("@cliLocalidad", OleDbType.VarChar, 20, "cliLocalidad")
        oUComando.Parameters.Add("@cliHorario", OleDbType.VarChar, 20, "cliHorario")

        oUComando.Parameters.Add("@Id_Cliente", OleDbType.Integer, 10, "Id_Cliente")

        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As OleDbCommand
        oDComando = New OleDbCommand("DELETE * FROM T_Clientes  WHERE Id_Cliente = @Id_Cliente", Conexion)
        oDComando.Parameters.Add("@Id_Cliente", OleDbType.Integer, 10, "Id_Cliente")
        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtCLIENTES As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtCLIENTES)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub

Public Function BuscarId_Cliente(ByVal IdCliente)
  Dim NombreCliente As String
  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con
  cmd.CommandText = "SELECT * FROM T_Clientes WHERE Id_Cliente = " & IdCliente & " "
  con.Open()
  Reader = cmd.ExecuteReader
  While (Reader.Read())
    NombreCliente = Reader.GetString(2)
  End While
Return NombreCliente
End Function

End Class