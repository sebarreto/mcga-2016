Imports System.Data.OleDb

Public Class TICKET
    Private Conexion As New OleDbConnection()
    Private oDataAdapter As OleDbDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New OleDbDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As OleDbCommand
        oSComando = New OleDbCommand("SELECT * FROM T_Tickets ORDER BY Id_Ticket ", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As OleDbCommand
        oIComando = New OleDbCommand("INSERT INTO T_Tickets (Id_Cliente,Id_Operador,Id_Tecnico,Falla,Nconforme,Cobranza,Monto,Fecha,Hora) VALUES (@Id_Cliente,@Id_Operador,@Id_Tecnico,@Falla,@Nconforme,@Cobranza,@Monto,@Fecha,@Hora)", Conexion)
        oIComando.Parameters.Add("@Id_Cliente", OleDbType.Integer, 200, "Id_Cliente")
        'oIComando.Parameters.Add("@NomCliente", OleDbType.VarChar, 50, "NomCliente")
        oIComando.Parameters.Add("@Id_Operador", OleDbType.Integer, 20, "Id_Operador")
        oIComando.Parameters.Add("@Id_Tecnico", OleDbType.Integer, 20, "Id_Tecnico")
        oIComando.Parameters.Add("@Falla", OleDbType.VarChar, 200, "Falla")
        oIComando.Parameters.Add("@Nconforme", OleDbType.Integer, 10, "Nconforme")
        oIComando.Parameters.Add("@Cobranza", OleDbType.VarChar, 1, "Cobranza")
        oIComando.Parameters.Add("@Monto", OleDbType.Integer, 10, "Monto")
        oIComando.Parameters.Add("@Fecha", OleDbType.Date, 20, "Fecha")
        oIComando.Parameters.Add("@Hora", OleDbType.VarChar, 20, "Hora")

        oIComando.Parameters.Add("@Id_Ticket", OleDbType.Integer, 10, "Id_Ticket")
        oDataAdapter.InsertCommand = oIComando

        'DELETECOMMAND
        Dim oDComando As OleDbCommand
        oDComando = New OleDbCommand("DELETE * FROM T_Tickets  WHERE Id_Ticket = @Id_Ticket", Conexion)
        oDComando.Parameters.Add("@Id_Ticket", OleDbType.Integer, 1, "Id_Ticket")
        oDataAdapter.DeleteCommand = oDComando

        'UPDATE COMMAND
        Dim oUComando As OleDbCommand
        oUComando = New OleDbCommand("UPDATE T_Tickets SET Id_Cliente=@Id_Cliente, Id_Operador=@Id_Operador, Id_Tecnico=@Id_Tecnico, Falla=@Falla, Nconforme=@Nconforme, Cobranza=@Cobranza, Monto=@Monto,Fecha= @Fecha , Hora = @Hora  WHERE Id_Ticket = @Id_Ticket ", Conexion)
        oUComando.Parameters.Add("@Id_Cliente", OleDbType.Integer, 200, "Id_Cliente")
        'oUComando.Parameters.Add("@NomCliente", OleDbType.VarChar, 50, "NomCliente")
        oUComando.Parameters.Add("@Id_Operador", OleDbType.Integer, 20, "Id_Operador")
        oUComando.Parameters.Add("@Id_Tecnico", OleDbType.Integer, 20, "Id_Tecnico")
        oUComando.Parameters.Add("@Falla", OleDbType.VarChar, 200, "Falla")
        oUComando.Parameters.Add("@Nconforme", OleDbType.Integer, 6, "Nconforme")
        oUComando.Parameters.Add("@Cobranza", OleDbType.VarChar, 1, "Cobranza")
        oUComando.Parameters.Add("@Monto", OleDbType.Integer, 6, "Monto")
        oUComando.Parameters.Add("@Fecha", OleDbType.VarChar, 20, "Fecha")
        oUComando.Parameters.Add("@Hora", OleDbType.VarChar, 20, "Hora")

        oUComando.Parameters.Add("@Id_Ticket", OleDbType.Integer, 10, "Id_Ticket")

        oDataAdapter.UpdateCommand = oUComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtTICKETS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtTICKETS)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub

    Public Function BuscarID_OPERADOR(ByVal IdOperador)
    Dim con As OleDbConnection
    Dim cmd As OleDbCommand
    Dim Reader As OleDb.OleDbDataReader
    Dim OPERADOR As String

      con = New OleDbConnection
      cmd = New OleDbCommand
      con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
      cmd.Connection = con
      cmd.CommandText = "SELECT * FROM T_Operadores WHERE Id_Operador = " & IdOperador & " "
      con.Open()
      Reader = cmd.ExecuteReader
      While (Reader.Read())
         OPERADOR = Reader.GetString(1)
      End While
      Reader.Close()
      Return OPERADOR
    End Function
End Class