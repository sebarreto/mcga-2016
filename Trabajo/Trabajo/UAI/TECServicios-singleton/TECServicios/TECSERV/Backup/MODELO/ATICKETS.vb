Imports System.Data.OleDb

Public Class ATICKETS
    Private Conexion As New OleDbConnection()
    Private oDataAdapter As OleDbDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.ConectarAud
        oDataAdapter = New OleDbDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As OleDbCommand
        oSComando = New OleDbCommand("SELECT * FROM T_AuditoriaTickets", Conexion)
        oDataAdapter.SelectCommand = oSComando

          ' INSERT COMMAND
        Dim oIComando As OleDbCommand
        oIComando = New OleDbCommand("INSERT INTO T_AuditoriaTickets (Id_Cliente,Id_Operador,Id_Tecnico,Falla,Nconforme,Cobranza,Monto,Fecha,Hora,audTipo,audFecha,audHora,audUsuario) VALUES (@Id_Cliente,@Id_Operador,@Id_Tecnico,@Falla,@Nconforme,@Cobranza,@Monto,@Fecha,@Hora,@audTipo,@audFecha,@audHora,@audUsuario)", Conexion)
        oIComando.Parameters.Add("@Id_Cliente", OleDbType.Integer, 200, "Id_Cliente")
       ' oIComando.Parameters.Add("@NomCliente", OleDbType.VarChar, 50, "NomCliente")
        oIComando.Parameters.Add("@Id_Operador", OleDbType.Integer, 10, "Id_Operador")
        oIComando.Parameters.Add("@Id_Tecnico", OleDbType.Integer, 20, "Id_Tecnico")
        oIComando.Parameters.Add("@Falla", OleDbType.VarChar, 200, "Falla")
        oIComando.Parameters.Add("@Nconforme", OleDbType.Integer, 10, "Nconforme")
        oIComando.Parameters.Add("@Cobranza", OleDbType.VarChar, 1, "Cobranza")
        oIComando.Parameters.Add("@Monto", OleDbType.Integer, 10, "Monto")
        oIComando.Parameters.Add("@Fecha", OleDbType.VarChar, 20, "Fecha")
        oIComando.Parameters.Add("@Hora", OleDbType.VarChar, 20, "Hora")
        oIComando.Parameters.Add("@audTipo", OleDbType.VarChar, 20, "audTipo")
        oIComando.Parameters.Add("@audFecha", OleDbType.VarChar, 20, "audFecha")
        oIComando.Parameters.Add("@audHora", OleDbType.VarChar, 20, "audHora")
        oIComando.Parameters.Add("@audUsuario", OleDbType.VarChar, 20, "audUsuario")

        oIComando.Parameters.Add("@Id_Ticket", OleDbType.Integer, 10, "Id_Ticket")
        oDataAdapter.InsertCommand = oIComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtATICKETS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.ConectarAud
        Try
            oDataAdapter.Update(dtATICKETS)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class