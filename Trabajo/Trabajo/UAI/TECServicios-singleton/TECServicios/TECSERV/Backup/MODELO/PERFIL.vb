Imports System.Data.OleDb

Public Class PERFIL

    Private Conexion As New OleDbConnection()
    Private oDataAdapter As OleDbDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New OleDbDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As OleDbCommand
        oSComando = New OleDbCommand("SELECT * FROM T_Perfiles", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As OleDbCommand
        oIComando = New OleDbCommand("INSERT INTO T_Perfiles (Id_Grupo, Id_Formulario, Id_Permiso) VALUES (@Id_Grupo, @Id_Formulario, @Id_Permiso)", Conexion)
        oIComando.Parameters.Add("@Id_Grupo", OleDbType.Integer, 20, "Id_Grupo")
        oIComando.Parameters.Add("@Id_Formulario", OleDbType.Integer, 20, "Id_Formulario")
        oIComando.Parameters.Add("@Id_Permiso", OleDbType.Integer, 20, "Id_Permiso")

       ' oIComando.Parameters.Add("@Id_Perfil", OleDbType.Integer, 10, "Id_Perfil")
        oDataAdapter.InsertCommand = oIComando

        'UPDATE COMMAND
        Dim oUComando As OleDbCommand
        oUComando = New OleDbCommand("UPDATE T_Perfiles SET  perPermiso = @perPermiso WHERE perFormulario = @perFormulario AND perGrupo=@perGrupo ", Conexion)
        oUComando.Parameters.Add("@pePermisos", OleDbType.VarChar, 50, "pePermisos")

        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As OleDbCommand
        oDComando = New OleDbCommand("DELETE * FROM T_Perfiles  WHERE Id_Perfil = @Id_Perfil", Conexion)
        oDComando.Parameters.Add("@Id_Perfil", OleDbType.Integer, 10, "Id_Perfil")

        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
       ' oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Function ELIMINAR_PERFIL(ByVal Grupo, ByVal Formulario, ByVal Perm)
    Dim con As OleDbConnection
    Dim cmd As OleDbCommand
    Dim Reader As OleDb.OleDbDataReader

      con = New OleDbConnection
      cmd = New OleDbCommand
      con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
      cmd.Connection = con
      cmd.CommandText = "DELETE FROM T_Perfiles WHERE Id_Grupo = " & Grupo & " AND Id_Formulario = " & Formulario & " AND Id_Permiso = " & Perm & "  "
      con.Open()
      Reader = cmd.ExecuteReader
    End Function

    Public Function MODIFICAR_PERFIL(ByVal Grupo, ByVal Formulario, ByVal Perm)
    Dim con As OleDbConnection
    Dim cmd As OleDbCommand
    Dim Reader As OleDb.OleDbDataReader

      con = New OleDbConnection
      cmd = New OleDbCommand
      con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
      cmd.Connection = con
      cmd.CommandText = "DELETE FROM T_Perfiles WHERE Id_Grupo = " & Grupo & " AND Id_Formulario = " & Formulario & " AND Id_Permiso = " & Perm & "  "
      con.Open()
      Reader = cmd.ExecuteReader

    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtCLIENTES As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtCLIENTES)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class