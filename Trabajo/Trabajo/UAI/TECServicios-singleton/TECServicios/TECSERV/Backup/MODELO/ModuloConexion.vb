Imports System.Data.OleDb

Module ModuloConexion
    Private oConnection As OleDbConnection
    Private oConnectionAud As OleDbConnection

    Public Function Conectar() As OleDbConnection
        Dim rConString As String
        rConString = "Provider = Microsoft.Jet.OLEDB.4.0;Data Source=C:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
        oConnection = New OleDbConnection()
        If oConnection.State = ConnectionState.Closed Then
            oConnection.ConnectionString = rConString
            oConnection.Open()
            Return oConnection
        End If
    End Function

    Public Function ConectarAud() As OleDbConnection
        Dim rConString As String
        rConString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\UAI\TECServicios-singleton\TECServicios\Auditoria.mdb"
        oConnectionAud = New OleDbConnection()
        If oConnectionAud.State = ConnectionState.Closed Then
            oConnectionAud.ConnectionString = rConString
            oConnectionAud.Open()
            Return oConnectionAud
        End If
    End Function

End Module