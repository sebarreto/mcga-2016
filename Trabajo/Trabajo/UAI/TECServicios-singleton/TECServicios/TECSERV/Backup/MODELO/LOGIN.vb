Imports System.Data.OleDb

Public Class LOGIN
    Private Conexion As New OleDbConnection()
    Private oDataAdapter As OleDbDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New OleDbDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As OleDbCommand
        oSComando = New OleDbCommand("SELECT * FROM T_Operadores", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As OleDbCommand
        oIComando = New OleDbCommand("INSERT INTO T_Operadores(logNombre, logClave,logNivel) VALUES (@logNombre,@logClave,@logNivel)", Conexion)
        oIComando.Parameters.Add("@logNombre", OleDbType.VarChar, 20, "logNombre")
        oIComando.Parameters.Add("@logClave", OleDbType.VarChar, 50, "logClave")
        oIComando.Parameters.Add("@logNivel", OleDbType.Integer, 4, "logNivel")

        oIComando.Parameters.Add("@Id_Login", OleDbType.Integer, 10, "Id_Login")
        oDataAdapter.InsertCommand = oIComando

        ' DELETECOMMAND
        Dim oDComando As OleDbCommand
        oDComando = New OleDbCommand("DELETE * FROM T_Operadores  WHERE Id_Login = @Id_Login", Conexion)
        oDComando.Parameters.Add("@Id_Login", OleDbType.Integer, 1, "Id_Login")
        oDataAdapter.DeleteCommand = oDComando

        'UPDATE COMMAND
        Dim oUComando As OleDbCommand
        oUComando = New OleDbCommand("UPDATE USUARIOS SET logNombre = @logNombre, logClave = @logClave, logNivel = @logNivel WHERE Id_Login = @Id_Login ", Conexion)
        oIComando.Parameters.Add("@logNombre", OleDbType.VarChar, 20, "logNombre")
        oIComando.Parameters.Add("@logClave", OleDbType.VarChar, 50, "logClave")
        oIComando.Parameters.Add("@logNivel", OleDbType.Integer, 4, "logNivel")

        oUComando.Parameters.Add("@Id_Login", OleDbType.Integer, 2, "Id_Login")
        oDataAdapter.UpdateCommand = oUComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtUSUARIOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtUSUARIOS)
        Catch oEX As Exception
            MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class
