Imports System.Data.OleDb

Public Class OPERADOR
    Private Conexion As New OleDbConnection()
    Private oDataAdapter As OleDbDataAdapter
    Private oDataTable As DataTable

    Private con As OleDbConnection
    Private cmd As OleDbCommand
    Private Reader As OleDb.OleDbDataReader

    Public Sub New()
        Conexion = ModuloConexion.Conectar
        oDataAdapter = New OleDbDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As OleDbCommand
        oSComando = New OleDbCommand("SELECT * FROM T_Operadores", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As OleDbCommand
        oIComando = New OleDbCommand("INSERT INTO T_Operadores (logApellido,logNombre,logTelefono,logNomUsuario,logClave,Id_Grupo) VALUES (@logApellido,@logNombre,@logTelefono,@logNomUsuario,@logClave,@Id_Grupo)", Conexion)
        oIComando.Parameters.Add("@logApellido", OleDbType.VarChar, 20, "logApellido")
        oIComando.Parameters.Add("@logNombre", OleDbType.VarChar, 20, "logNombre")
        oIComando.Parameters.Add("@logTelefono", OleDbType.VarChar, 20, "logTelefono")
        oIComando.Parameters.Add("@logNomUsuario", OleDbType.VarChar, 20, "logNomUsuario")
        oIComando.Parameters.Add("@logClave", OleDbType.VarChar, 50, "logClave")
        oIComando.Parameters.Add("@Id_Grupo", OleDbType.VarChar, 20, "Id_Grupo")

        oIComando.Parameters.Add("@Id_Operador", OleDbType.Integer, 10, "Id_Operador")

        oDataAdapter.InsertCommand = oIComando

         'UPDATE COMMAND
        Dim oUComando As OleDbCommand
        oUComando = New OleDbCommand("UPDATE T_Operadores SET  logApellido = @logApellido, logNombre = @logNombre, logTelefono = @logTelefono, logNomUsuario = @logNomUsuario, logClave = @logClave, Id_Grupo = @Id_Grupo WHERE Id_Operador = @Id_Operador ", Conexion)
        oUComando.Parameters.Add("@logApellido", OleDbType.VarChar, 20, "logApellido")
        oUComando.Parameters.Add("@logNombre", OleDbType.VarChar, 20, "logNombre")
        oUComando.Parameters.Add("@logTelefono", OleDbType.VarChar, 20, "logTelefono")
        oUComando.Parameters.Add("@logNomUsuario", OleDbType.VarChar, 20, "logNomUsuario")
        oUComando.Parameters.Add("@logClave", OleDbType.VarChar, 50, "logClave")
        oUComando.Parameters.Add("@Id_Grupo", OleDbType.VarChar, 20, "Id_Grupo")

        oUComando.Parameters.Add("@Id_Operador", OleDbType.Integer, 10, "Id_Operador")

        oDataAdapter.UpdateCommand = oUComando

        ' DELETECOMMAND
        Dim oDComando As OleDbCommand
        oDComando = New OleDbCommand("DELETE * FROM T_Operadores  WHERE Id_Operador = @Id_Operador", Conexion)
        oDComando.Parameters.Add("@Id_Operador", OleDbType.Integer, 10, "Id_Operador")

        oDataAdapter.DeleteCommand = oDComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtTECNICOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtTECNICOS)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub

Public Function BuscarApellido_Operador(ByVal Id_Operador)
  Dim IdOperador As String
  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con
  cmd.CommandText = "SELECT * FROM T_Operadores WHERE Id_Operador = " & Id_Operador & " "
  con.Open()
  Reader = cmd.ExecuteReader
  While (Reader.Read())
    IdOperador = Reader.GetString(4)
  End While
Return IdOperador
End Function

Public Function BuscarID_Operador(ByVal Apellido)
  Dim IdOperador As Integer
  con = New OleDbConnection
  cmd = New OleDbCommand
  con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\UAI\TECServicios-singleton\TECServicios\TECSERV\BASE\dataserv.mdb"
  cmd.Connection = con
  cmd.CommandText = "SELECT * FROM T_Operadores WHERE logNomUsuario = '" & Apellido & "' "
  con.Open()
  Reader = cmd.ExecuteReader
  While (Reader.Read())
    IdOperador = Reader.GetInt32(0)
  End While
Return IdOperador
End Function
End Class
