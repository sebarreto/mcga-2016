Imports System.Data.OleDb

Public Class AFORMULARIO
  Private Conexion As New OleDbConnection()
    Private oDataAdapter As OleDbDataAdapter
    Private oDataTable As DataTable

    Public Sub New()
        Conexion = ModuloConexion.ConectarAud
        oDataAdapter = New OleDbDataAdapter()

        ' SELECT COMMAND
        Dim oSComando As OleDbCommand
        oSComando = New OleDbCommand("SELECT * FROM T_AuditoriaFormularios", Conexion)
        oDataAdapter.SelectCommand = oSComando

        ' INSERT COMMAND
        Dim oIComando As OleDbCommand
        oIComando = New OleDbCommand("INSERT INTO T_AuditoriaFormularios (frmNombre,audFecha,audHora,audTipo,audUsuario) VALUES (@frmNombre,@audFecha,@audHora,@audTipo,@audUsuario)", Conexion)
        oIComando.Parameters.Add("@frmNombre", OleDbType.VarChar, 20, "frmNombre")
        oIComando.Parameters.Add("@audFecha", OleDbType.VarChar, 20, "audFecha")
        oIComando.Parameters.Add("@audHora", OleDbType.VarChar, 20, "audHora")
        oIComando.Parameters.Add("@audTipo", OleDbType.VarChar, 20, "audTipo")
        oIComando.Parameters.Add("@audUsuario", OleDbType.VarChar, 20, "audUsuario")

        oIComando.Parameters.Add("@Id_AudForm", OleDbType.Integer, 10, "Id_AudForm")

        oDataAdapter.InsertCommand = oIComando

        oDataTable = New DataTable()
        oDataAdapter.Fill(oDataTable)
        oDataAdapter.FillSchema(oDataTable, SchemaType.Source)

        Conexion.Close()
    End Sub

    Public Function OBTENER_TABLA() As DataTable
        Return oDataTable
    End Function

    Public Sub ACTUALIZAR_TABLA(ByVal dtFORMULARIOS As DataTable)
        If Conexion.State = ConnectionState.Closed Then Conexion = ModuloConexion.Conectar
        Try
            oDataAdapter.Update(dtFORMULARIOS)
        Catch oEX As Exception
           MsgBox(oEX.Message)
        Finally
            Conexion.Close()
        End Try
    End Sub
End Class

