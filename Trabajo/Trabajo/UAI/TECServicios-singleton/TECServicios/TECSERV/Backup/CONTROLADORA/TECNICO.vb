Imports System.Data.OleDb

Public Class TECNICO

    Private modTECNICOS As MODELO.TECNICO  ' OBJETO
    Private dtTECNICOS As DataTable
    Private dvTECNICOS As DataView

    Public Sub New()
        modTECNICOS = New MODELO.TECNICO
        dtTECNICOS = modTECNICOS.OBTENER_TABLA()
    End Sub

   Public Function CREAR_TECNICO() As DataRow
        Return dtTECNICOS.NewRow
    End Function

    Public Sub AGREGAR_CLIENTE(ByVal drCLIENTE As DataRow)
        Try
            dtTECNICOS.Rows.Add(drCLIENTE)
            ACTUALIZA_TABLA()
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub ACTUALIZA_TABLA()
        modTECNICOS.ACTUALIZAR_TABLA(dtTECNICOS)
    End Sub

    Public Function OBTENER_CLIENTE(ByVal NRO) As DataRow
        Return dtTECNICOS.Rows.Find(NRO)
    End Function

    Public Function OBTENER_TECNICOS() As DataTable
        Return dtTECNICOS
    End Function

    Public Sub ELIMINAR_TECNICO(ByVal NRO As Integer)
        Dim rowAUTO As DataRow
        rowAUTO = dtTECNICOS.Rows.Find(NRO)
        rowAUTO.Delete()

        ACTUALIZA_TABLA()
    End Sub

    Public Sub MODIFICAR_TECNICO(ByVal drTECNICO As DataRow)
        Dim rowCLIENTE As DataRow
        rowCLIENTE = dtTECNICOS.Rows.Find(drTECNICO("Id_Tecnico"))

        rowCLIENTE("tecNombre") = drTECNICO("tecNombre")
        rowCLIENTE("tecApellido") = drTECNICO("tecApellido")

        ACTUALIZA_TABLA()
    End Sub

Public Function BUSCAR_TECNICO(ByVal Nombre As String) As DataRow
      Try
        Return dtTECNICOS.Select("tecApellido LIKE '%" & Nombre & "%'")(0)
    Catch oEX As Exception
     ' MsgBox(oEX.Message)
    Finally
    End Try
End Function

End Class
