Imports System.Data.OleDb

Public Class CLIENTES
    Private modCLIENTES As MODELO.CLIENTES ' OBJETO
    Private dtCLIENTES As DataTable
    Private dvCLIENTES As DataView
    Private cliCodigo As String
    Private cliNombre As String

    Public Sub New()
        modCLIENTES = MODELO.CLIENTES.Crear()
        dtCLIENTES = modCLIENTES.OBTENER_TABLA()
    End Sub

    Public Function CREAR_CLIENTE() As DataRow
        Return dtCLIENTES.NewRow
    End Function

    Public Sub AGREGAR_CLIENTE(ByVal drCLIENTE As DataRow)
        Try
            dtCLIENTES.Rows.Add(drCLIENTE)
            ACTUALIZA_TABLA()
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub ACTUALIZA_TABLA()
        modCLIENTES.ACTUALIZAR_TABLA(dtCLIENTES)
    End Sub

    Public Sub MODIFICAR_CLIENTE(ByVal drCLIENTE As DataRow)
        Dim rowCLIENTE As DataRow

        rowCLIENTE = dtCLIENTES.Rows.Find(drCLIENTE("Id_Cliente"))

        rowCLIENTE("cliCodigo") = drCLIENTE("cliCodigo")
        rowCLIENTE("cliNombre") = drCLIENTE("cliNombre")
        rowCLIENTE("cliDomicilio") = drCLIENTE("cliDomicilio")
        rowCLIENTE("cliTelefono") = drCLIENTE("cliTelefono")
        rowCLIENTE("cliLocalidad") = drCLIENTE("cliLocalidad")
        rowCLIENTE("cliHorario") = drCLIENTE("cliHorario")
        ACTUALIZA_TABLA()
    End Sub

    Public Function OBTENER_CLIENTE(ByVal NRO) As DataRow
        Return dtCLIENTES.Rows.Find(NRO)
    End Function

    Public Function OBTENER_CLIENTES() As DataTable
        Return dtCLIENTES
    End Function

Public Function BUSCAR_CLIENTE(ByVal Nombre As String) As DataRow
      Try
        Return dtCLIENTES.Select("cliNombre LIKE '%" & Nombre & "%'")(0)
    Catch oEX As Exception
'      MsgBox(oEX.Message)
    Finally
    End Try
End Function

    Public Sub ELIMINAR_CLIENTE(ByVal NRO As Integer)
        Dim rowCLIENTE As DataRow
        rowCLIENTE = dtCLIENTES.Rows.Find(NRO)
        rowCLIENTE.Delete()

        ACTUALIZA_TABLA()
    End Sub

  Public Function BuscarID_Cliente(ByVal ID_Cliente)
    Dim NombreCli As String
     NombreCli = modCLIENTES.BuscarId_Cliente(ID_Cliente)
     Return NombreCli
  End Function
End Class