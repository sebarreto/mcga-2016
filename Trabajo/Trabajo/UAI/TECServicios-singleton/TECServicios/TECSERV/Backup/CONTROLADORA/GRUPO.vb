Imports System.Data.OleDb

Public Class GRUPO

    Private modGRUPOS As MODELO.GRUPO
        Private dtGRUPOS As DataTable
    Private dVGRUPOS As DataView

    Public Sub New()
        modGRUPOS = New MODELO.GRUPO
        dtGRUPOS = modGRUPOS.OBTENER_TABLA()
    End Sub

    Public Function CREAR_GRUPO() As DataRow
        Return dtGRUPOS.NewRow
    End Function

    Public Sub AGREGAR_GRUPO(ByVal drGRUPO As DataRow)
        Try
            dtGRUPOS.Rows.Add(drGRUPO)
            ACTUALIZA_TABLA()
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub ACTUALIZA_TABLA()
        modGRUPOS.ACTUALIZAR_TABLA(dtGRUPOS)
    End Sub

    Public Sub MODIFICAR_GRUPO(ByVal drGRUPO As DataRow)
        Dim rowGRUPO As DataRow

        rowGRUPO = dtGRUPOS.Rows.Find(drGRUPO("Id_Grupo"))
        rowGRUPO("GrNombre") = drGRUPO("GrNombre")
        rowGRUPO("GrDescripcion") = drGRUPO("GrDescripcion")
        ACTUALIZA_TABLA()
    End Sub

    Public Function OBTENER_ID_GRUPO(ByVal NRO) As String
    Dim IdGR() As DataRow
      IdGR = dtGRUPOS.Select("GrNombre = '" & NRO & "'")
      Return IdGR(0)(0)
    End Function

   Public Function OBTENER_GRUPO(ByVal nom) As DataRow
        Return dtGRUPOS.Rows.Find(nom)
    End Function

    Public Function OBTENER_GRUPOS() As DataTable
        Return dtGRUPOS
    End Function

    Public Sub ELIMINAR_GRUPO(ByVal NRO As Integer)
        Dim rowGRUPO As DataRow
        rowGRUPO = dtGRUPOS.Rows.Find(NRO)
        rowGRUPO.Delete()

        ACTUALIZA_TABLA()
    End Sub

End Class