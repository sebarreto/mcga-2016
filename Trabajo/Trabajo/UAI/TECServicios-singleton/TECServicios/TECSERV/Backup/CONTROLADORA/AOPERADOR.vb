Imports System.Data.OleDb

Public Class AOPERADOR

    Private modOPERADORES As MODELO.AOPERADOR
    Private dtOPERADORES As DataTable
    Private dvOPERADORES As DataView

    Public Sub New()
        modOPERADORES = New MODELO.AOPERADOR
        dtOPERADORES = modOPERADORES.OBTENER_TABLA()
    End Sub

   Public Function CREAR_OPERADOR() As DataRow
        Return dtOPERADORES.NewRow
    End Function

    Public Sub AGREGAR_OPERADOR(ByVal drOperador As DataRow)
        Try
            dtOPERADORES.Rows.Add(drOperador)
            ACTUALIZA_TABLA()
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub ACTUALIZA_TABLA()
        modOPERADORES.ACTUALIZAR_TABLA(dtOPERADORES)
    End Sub

    Public Function OBTENER_OPERADOR(ByVal NRO) As DataRow
        Return dtOPERADORES.Rows.Find(NRO)
    End Function

    Public Function OBTENER_OPERADORES() As DataTable
        Return dtOPERADORES
    End Function

End Class
