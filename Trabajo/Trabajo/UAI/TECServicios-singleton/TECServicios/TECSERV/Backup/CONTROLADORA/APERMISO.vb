Imports System.Data.OleDb

Public Class APERMISO

    Private modPERMISOS As MODELO.APERMISO
    Private dtPERMISOS As DataTable
    Private dvPERMISOS As DataView

    Public Sub New()
        modPERMISOS = New MODELO.APERMISO
        dtPERMISOS = modPERMISOS.OBTENER_TABLA()
    End Sub

   Public Function CREAR_PERMISO() As DataRow
        Return dtPERMISOS.NewRow
    End Function

    Public Sub AGREGAR_PERMISO(ByVal drPermiso As DataRow)
        Try
            dtPERMISOS.Rows.Add(drPermiso)
            ACTUALIZA_TABLA()
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub ACTUALIZA_TABLA()
        modPERMISOS.ACTUALIZAR_TABLA(dtPERMISOS)
    End Sub

    Public Function OBTENER_PERMISOS() As DataTable
        Return dtPERMISOS
    End Function

End Class
