Imports System.Data.OleDb

Public Class ACLIENTES
 Private modACLIENTES As MODELO.ACLIENTES
    Private dtACLIENTES As DataTable
    Private dvACLIENTES As DataView

 Public Sub New()
        modACLIENTES = New MODELO.ACLIENTES
        dtACLIENTES = modACLIENTES.OBTENER_TABLA()
    End Sub

    Public Function CREAR_ACLIENTE() As DataRow
        Return dtACLIENTES.NewRow
    End Function


    Public Sub AGREGAR_ACLIENTE(ByVal drACLIENTE As DataRow)
        Try
            dtACLIENTES.Rows.Add(drACLIENTE)
            ACTUALIZA_TABLA()

        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

Public Function OBTENER_CLIENTES() As DataTable
       Return dtACLIENTES
End Function

   Private Sub ACTUALIZA_TABLA()
        modACLIENTES.ACTUALIZAR_TABLA(dtACLIENTES)
    End Sub

End Class