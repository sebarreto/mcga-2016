Imports System.Data.OleDb

Public Class ATICKETS
  Private modATICKETS As MODELO.ATICKETS
  Private dtATICKETS As DataTable
  Private dvATICKETS As DataView

Public Sub New()
  modATICKETS = New MODELO.ATICKETS
  dtATICKETS = modATICKETS.OBTENER_TABLA()
End Sub

Public Function CREAR_ATICKET() As DataRow
  Return dtATICKETS.NewRow
End Function

Public Sub AGREGAR_ATICKET(ByVal drATICKETS As DataRow)
  Try
    dtATICKETS.Rows.Add(drATICKETS)
    ACTUALIZA_TABLA()
  Catch ex As Exception
    Throw (ex)
  End Try
End Sub

Public Function OBTENER_TICKETS() As DataTable
  Return dtATICKETS
End Function

Private Sub ACTUALIZA_TABLA()
  modATICKETS.ACTUALIZAR_TABLA(dtATICKETS)
End Sub
End Class