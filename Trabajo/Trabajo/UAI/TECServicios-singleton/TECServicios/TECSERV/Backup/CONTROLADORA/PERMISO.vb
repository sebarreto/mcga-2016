Imports System.Data.OleDb

Public Class PERMISO

    Private modPERMISOS As MODELO.PERMISO
    Private dtPERMISOS As DataTable
    Private dvPERMISOS As DataView

    Public Sub New()
        modPERMISOS = New MODELO.PERMISO
        dtPERMISOS = modPERMISOS.OBTENER_TABLA()
    End Sub

    Public Function CREAR_PERMISO() As DataRow
        Return dtPERMISOS.NewRow
    End Function

    Public Sub AGREGAR_PERMISO(ByVal drPERMISO As DataRow)
        Try
            dtPERMISOS.Rows.Add(drPERMISO)
            ACTUALIZA_TABLA()
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub ACTUALIZA_TABLA()
        modPERMISOS.ACTUALIZAR_TABLA(dtPERMISOS)
    End Sub

    Public Sub MODIFICAR_PERMISO(ByVal drPERMISO As DataRow)
        Dim rowGRUPO As DataRow

        rowGRUPO = dtPERMISOS.Rows.Find(drPERMISO("Id_Permiso"))
        rowGRUPO("perNombre") = drPERMISO("perNombre")
        rowGRUPO("perDescripcion") = drPERMISO("perDescripcion")
        ACTUALIZA_TABLA()
    End Sub

    Public Function OBTENER_PERMISO(ByVal NRO) As DataRow
        Return dtPERMISOS.Rows.Find(NRO)
    End Function

    Public Function OBTENER_PERMISOS() As DataTable
        Return dtPERMISOS
    End Function

    Public Function OBTENER_ID_PERMISO(ByVal NRO) As String
    Dim IdPERM() As DataRow
      IdPERM = dtPERMISOS.Select("perNombre = '" & NRO & "'")
      Return IdPERM(0)(0)
    End Function

    Public Sub ELIMINAR_PERMISO(ByVal NRO As Integer)
        Dim rowGRUPO As DataRow
        rowGRUPO = dtPERMISOS.Rows.Find(NRO)
        rowGRUPO.Delete()

        ACTUALIZA_TABLA()
    End Sub

End Class