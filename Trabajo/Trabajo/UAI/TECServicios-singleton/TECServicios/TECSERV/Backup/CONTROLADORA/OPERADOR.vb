Imports System.Data.OleDb

Public Class OPERADOR

    Private modOPERADORES As MODELO.OPERADOR    ' OBJETO
    Private dtOPERADORES As DataTable
    Private dvOPERADORES As DataView

    Public Sub New()
        modOPERADORES = New MODELO.OPERADOR
        dtOPERADORES = modOPERADORES.OBTENER_TABLA()
    End Sub

   Public Function CREAR_OPERADOR() As DataRow
        Return dtOPERADORES.NewRow
    End Function

    Public Sub AGREGAR_OPERADOR(ByVal drCLIENTE As DataRow)
        Try
            dtOPERADORES.Rows.Add(drCLIENTE)
            ACTUALIZA_TABLA()
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub ACTUALIZA_TABLA()
        modOPERADORES.ACTUALIZAR_TABLA(dtOPERADORES)
    End Sub

    Public Function OBTENER_OPERADOR(ByVal NRO) As DataRow
        Return dtOPERADORES.Rows.Find(NRO)
    End Function

    Public Function OBTENER_OPERADOR_LOGIN(ByVal Nombre As String) As DataRow
    Try
      'Return dtLOGIN.Select("Nombre LIKE '%" & Nombre & "%'")(0)
      Return dtOPERADORES.Select("logNomUsuario = '" & Nombre & "'")(0)
    Catch oEX As Exception
    'MsgBox(oEX.Message)
    Finally
    End Try
  End Function

Public Function BUSCAR_OPERADOR(ByVal Nombre As String) As DataRow
      Try
        Return dtOPERADORES.Select("logApellido LIKE '%" & Nombre & "%'")(0)
    Catch oEX As Exception
    '  MsgBox(oEX.Message)
    Finally
    End Try
End Function

    Public Function OBTENER_OPERADORES() As DataTable
        Return dtOPERADORES
    End Function

    Public Sub ELIMINAR_OPERADOR(ByVal NRO As Integer)
        Dim rowAUTO As DataRow
        rowAUTO = dtOPERADORES.Rows.Find(NRO)
        rowAUTO.Delete()

        ACTUALIZA_TABLA()
    End Sub

    Public Sub MODIFICAR_OPERADOR(ByVal drOPERADOR As DataRow)
        Dim rowOPERADOR As DataRow
        rowOPERADOR = dtOPERADORES.Rows.Find(drOPERADOR("Id_Operador"))

        rowOPERADOR("logApellido") = drOPERADOR("logApellido")
        rowOPERADOR("logNombre") = drOPERADOR("logNombre")
        rowOPERADOR("logTelefono") = drOPERADOR("logTelefono")
        rowOPERADOR("logNomUsuario") = drOPERADOR("logNomUsuario")
        rowOPERADOR("logClave") = drOPERADOR("logClave")
        rowOPERADOR("Id_Grupo") = drOPERADOR("Id_Grupo")

        ACTUALIZA_TABLA()
    End Sub

  Public Function BuscarApellido_Operador(ByVal ID_Operador)
  Dim NombreOp As String
    NombreOp = modOPERADORES.BuscarApellido_Operador(ID_Operador)
    Return NombreOp
  End Function

  Public Function BuscarID_Operador(ByVal Apellido)
  Dim IdOp As Integer
    IdOp = modOPERADORES.BuscarID_Operador(Apellido)
    Return IdOp
  End Function
End Class
