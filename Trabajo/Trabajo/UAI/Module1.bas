Attribute VB_Name = "Module1"
Global rutaIni  As String

'Lectura de archivos INI
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
       "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
                                   ByVal lpKeyName As Any, ByVal lpDefault As String, _
                                   ByVal lpReturnedString As String, ByVal nSize As Long, _
                                   ByVal lpFileName As String) As Long
'Escritura de Archivos INI
Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
        "WritePrivateProfileStringA" (ByVal lpApplicationName As String, _
                                      ByVal lpKeyName As Any, ByVal lpString As Any, _
                                      ByVal lpFileName As String) As Long

Public Function LeeINI(ByVal Encabezado As String, ByVal ClaveIni As String, _
       ByVal RutaArchivo As String) As String
Dim Char As Long, Result As String
    Result = String(300, 0)
    Char = GetPrivateProfileString(Encabezado, ClaveIni, "", Result, 300, RutaArchivo)
        If Char <> 0 Then Result = Left(Result, Char)
    Result = Trim(Replace(Result, Chr(0), vbNullString))
    LeeINI = Result
End Function

Public Function EscribeIni(ByVal Encabezado As String, ByVal ClaveIni As String, ByVal Valor As String, ByVal RutaArchivo As String)
    WritePrivateProfileString Encabezado, ClaveIni, Valor, RutaArchivo
End Function

Public Function fileExists(cArchivo As String) As Boolean
On Error GoTo 100
    fileExists = IIf(Dir(cArchivo) = "", False, True)
Exit Function
100
MENSAJE = "NO SE ENCONTRO " + cArchivo
121
MsgBox MENSAJE
End
End Function


