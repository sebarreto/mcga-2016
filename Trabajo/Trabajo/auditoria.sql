USE [master]
GO
/****** Object:  Database [Auditoria]    Script Date: 10/10/2015 11:44:40 ******/
CREATE DATABASE [Auditoria] ON  PRIMARY 
( NAME = N'Auditoria', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\Auditoria.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Auditoria_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\Auditoria_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Auditoria] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Auditoria].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Auditoria] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Auditoria] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Auditoria] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Auditoria] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Auditoria] SET ARITHABORT OFF 
GO
ALTER DATABASE [Auditoria] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Auditoria] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Auditoria] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Auditoria] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Auditoria] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Auditoria] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Auditoria] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Auditoria] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Auditoria] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Auditoria] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Auditoria] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Auditoria] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Auditoria] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Auditoria] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Auditoria] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Auditoria] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Auditoria] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Auditoria] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Auditoria] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Auditoria] SET  MULTI_USER 
GO
ALTER DATABASE [Auditoria] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Auditoria] SET DB_CHAINING OFF 
GO
USE [Auditoria]
GO
/****** Object:  Table [dbo].[T_AuditoriaClientes]    Script Date: 10/10/2015 11:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_AuditoriaClientes](
	[Id_Cliente] [int] NOT NULL identity,
	[cliCodigo] [nvarchar](50) NULL,
	[cliNombre] [nvarchar](50) NULL,
	[cliDomicilio] [nvarchar](50) NULL,
	[cliTelefono] [nvarchar](50) NULL,
	[cliLocalidad] [nvarchar](50) NULL,
	[cliHorario] [nvarchar](50) NULL,
	[cliFecha] [nvarchar](50) NULL,
	[cliHora] [nvarchar](50) NULL,
	[cliTipo] [nvarchar](50) NULL,
	[Usuario] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_AuditoriaFormularios]    Script Date: 10/10/2015 11:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_AuditoriaFormularios](
	[Id_AudForm] [int] NOT NULL identity,
	[frmNombre] [nvarchar](50) NULL,
	[audFecha] [nvarchar](50) NULL,
	[audHora] [nvarchar](50) NULL,
	[audTipo] [nvarchar](50) NULL,
	[audUsuario] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_AuditoriaGrupos]    Script Date: 10/10/2015 11:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_AuditoriaGrupos](
	[Id_AudGrupo] [int] NOT NULL identity,
	[nomGrupo] [nvarchar](50) NULL,
	[audFecha] [nvarchar](50) NULL,
	[audHora] [nvarchar](50) NULL,
	[audTipo] [nvarchar](50) NULL,
	[audUsuario] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_AuditoriaOperadores]    Script Date: 10/10/2015 11:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_AuditoriaOperadores](
	[Id_Login] [int] NOT NULL identity,
	[logApellido] [nvarchar](50) NULL,
	[logNombre] [nvarchar](50) NULL,
	[logTelefono] [nvarchar](50) NULL,
	[logNomUsuario] [nvarchar](50) NULL,
	[logClave] [nvarchar](50) NULL,
	[logGrupo] [nvarchar](50) NULL,
	[logPerfil] [nvarchar](50) NULL,
	[audFecha] [nvarchar](50) NULL,
	[audHora] [nvarchar](50) NULL,
	[audTipo] [nvarchar](50) NULL,
	[audUsuario] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_AuditoriaPerfiles]    Script Date: 10/10/2015 11:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_AuditoriaPerfiles](
	[Id_AudPerfil] [int] NOT NULL identity,
	[audpeNombre] [nvarchar](50) NULL,
	[audpePermisos] [nvarchar](50) NULL,
	[audpeGrupo] [nvarchar](50) NULL,
	[audFecha] [nvarchar](50) NULL,
	[audHora] [nvarchar](50) NULL,
	[audTipo] [nvarchar](50) NULL,
	[audUsuario] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_AuditoriaPermisos]    Script Date: 10/10/2015 11:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_AuditoriaPermisos](
	[Id_AudPerm] [int] NOT NULL identity,
	[perNombre] [nvarchar](50) NULL,
	[audFecha] [nvarchar](50) NULL,
	[audHora] [nvarchar](50) NULL,
	[audTipo] [nvarchar](50) NULL,
	[audUsuario] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_AuditoriaTecnicos]    Script Date: 10/10/2015 11:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_AuditoriaTecnicos](
	[Id_Tecnico] [int] NOT NULL identity,
	[tecNombre] [nvarchar](50) NULL,
	[tecApellido] [nvarchar](50) NULL,
	[tecDomicilio] [nvarchar](50) NULL,
	[tecLocalidad] [nvarchar](50) NULL,
	[tecTelefono] [nvarchar](50) NULL,
	[audTipo] [nvarchar](50) NULL,
	[audFecha] [nvarchar](50) NULL,
	[audHora] [nvarchar](50) NULL,
	[audUsuario] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_AuditoriaTickets]    Script Date: 10/10/2015 11:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_AuditoriaTickets](
	[Id_Ticket] [int] NOT NULL identity,
	[Id_Cliente] [int] NULL,
	[Id_Operador] [int] NULL,
	[Id_Tecnico] [int] NULL,
	[Falla] [nvarchar](255) NULL,
	[Nconforme] [int] NULL,
	[Cobranza] [nvarchar](50) NULL,
	[Monto] [int] NULL,
	[Fecha] [nvarchar](255) NULL,
	[Hora] [nvarchar](255) NULL,
	[audTipo] [nvarchar](50) NULL,
	[audFecha] [nvarchar](50) NULL,
	[audHora] [nvarchar](50) NULL,
	[audUsuario] [nvarchar](50) NULL
) ON [PRIMARY]

GO
USE [master]
GO
ALTER DATABASE [Auditoria] SET  READ_WRITE 
GO
